import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';

const MasterCardIcon = (props) => {
  return (
    <SvgIcon viewBox="0 0 256 158.1" {...props}>
      <rect
        x="93.3"
        y="16.9"
        style={{ fill: '#FF5F00' }}
        width="69.2"
        height="124.3"
      />
      <path
        style={{ fill: '#EB001B' }}
        d="M97.7,79c0-25.2,11.9-47.6,30.1-62.1C114.4,6.4,97.5,0,79,0C35.3,0,0,35.3,0,79s35.3,79,79,79   c18.4,0,35.3-6.4,48.7-16.9C109.5,126.9,97.7,104.3,97.7,79z"
      />
      <path
        style={{ fill: '#F79E1B' }}
        d="M255.7,79c0,43.7-35.3,79-79,79c-18.4,0-35.3-6.4-48.7-16.9c18.4-14.5,30.1-36.9,30.1-62.1   S146.2,31.4,128,16.9C141.4,6.4,158.3,0,176.7,0C220.4,0,255.7,35.6,255.7,79z"
      />
    </SvgIcon>
  );
};

export default MasterCardIcon;
