import React from 'react';
import { render } from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { store } from './redux/store';
import { Provider } from 'react-redux';
import { I18nextProvider } from 'react-i18next';

// import i18n
import i18n from './i18n';
import "slick-carousel/slick/slick.css";

const app = (
  <Provider store={store}>
    <I18nextProvider i18n={i18n}>
      <App />
    </I18nextProvider>
  </Provider>
);

render(app, document.getElementById('root'));

serviceWorker.unregister();
