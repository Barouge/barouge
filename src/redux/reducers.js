import { combineReducers } from 'redux';
import { userReducer } from './reducers/userReducer';
import { basketReducer } from './reducers/basketReducer';
import { appReducer } from './reducers/appReducer';
import { newsReducer } from './reducers/newsReducer';
import { catalogReducer } from './reducers/catalogReducer';
import { searchReducer } from './reducers/searchReducer';

export const rootReducer = combineReducers({
  user: userReducer,
  basket: basketReducer,
  app: appReducer,
  news: newsReducer,
  catalog: catalogReducer,
  search: searchReducer,
});
