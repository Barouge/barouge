export const BASKET_ADD_PRODUCT = 'BASKET/ADD_PRODUCT';
export const BASKET_REMOVE_PRODUCT = 'BASKET/REMOVE_PRODUCT';
export const BASKET_FETCH_INFO = 'BASKET/FETCH_INFO';

export const SEARCH_QUERY = 'PRODUCTS/SEARCH_QUERY';
export const SEARCH_CLEAR = 'PRODUCTS/SEARCH_CLEAR';

export const SHOW_LOADER = 'APP/SHOW_LOADER';
export const HIDE_LOADER = 'APP/HIDE_LOADER';
export const CHANGE_LOCALE = 'APP/CHANGE_LOCALE';
export const SET_LOCALE_FLAG = 'APP/SET_LOCALE_FLAG';
export const FETCH_SECTIONS = 'APP/FETCH_SECTIONS';
export const UPDATE_SUBSCRIBE = 'APP/UPDATE_SUBSCRIBE';
export const FETCH_PAYMENT_METHODS = 'APP/FETCH_PAYMENT_METHODS';
export const FETCH_FAQ = 'APP/FETCH_FAQ';
export const FETCH_HOME_SLIDER = 'APP/FETCH_HOME_SLIDER';
export const FETCH_TIZERS = 'APP/FETCH_TIZERS';
export const FETCH_SEO = 'APP/FETCH_SEO';

export const FETCH_TOP_MENU = 'CONTENT/FETCH_TOP_MENU';
export const FETCH_FOOTER_MENU = 'CONTENT/FETCH_FOOTER_MENU';

export const FETCH_NEWS = 'NEWS/FETCH_NEWS';
export const FETCH_ACTIONS = 'NEWS/FETCH_ACTIONS';
export const SET_CURRENT_NEWS = 'NEWS/SET_CURRENT_NEWS';
export const CLEAR_CUR_NEWS = 'NEWS/CLEAR_CUR_NEWS';
export const SHOW_MORE_NEWS = 'NEWS/SHOW_MORE_NEWS';

export const REQUEST_PHONE_CODE = 'USER/REQUEST_PHONE_CODE';
export const CHECK_PHONE_CODE = 'USER/CHECK_PHONE_CODE';
export const FETCH_USER_INFO = 'USER/FETCH_USER_INFO';
export const FETCH_USER_ORDERS = 'USER/FETCH_USER_ORDERS';
export const CODE_TIMER_ACTIVATE = 'USER/CODE_TIMER_ACTIVATE';
export const CHANGE_USER_DATA = 'USER/CHANGE_USER_DATA';
export const FETCH_AUTH_ENTITY = 'USER/FETCH_AUTH_ENTITY';

export const FETCH_CATALOG_FILTERS = 'CATALOG/FETCH_CATALOG_FILTERS';
export const CATALOG_CURRENT_FILTERS_UPDATE =
  'CATALOG/CATALOG_CURRENT_FILTERS_UPDATE';
export const SHOW_CATALOG_LOADER = 'CATALOG/SHOW_CATALOG_LOADER';
export const HIDE_CATALOG_LOADER = 'CATALOG/HIDE_CATALOG_LOADER';
export const FETCH_CATALOG_MENU = 'CATALOG/FETCH_CATALOG_MENU';
export const FETCH_CATALOG_PRODUCTS = 'CATALOG/FETCH_CATALOG_PRODUCTS';
export const FETCH_CATALOG_PRODUCTS_MORE =
  'CATALOG/FETCH_CATALOG_PRODUCTS_MORE';
export const CHANGE_CATALOG_VIEW = 'CATALOG/CHANGE_CATALOG_VIEW';
export const CATALOG_PRICE_SORT = 'CATALOG/CATALOG_PRICE_SORT';
export const FETCH_RECOMMENDED_PRODUCTS = 'CATALOG/FETCH_RECOMMENDED_PRODUCTS';
export const FETCH_BUYWITH_PRODUCTS = 'CATALOG/FETCH_BUYWITH_PRODUCTS';
export const FETCH_CATEGORIES = 'CATALOG/FETCH_CATEGORIES';
export const FETCH_PRODUCT_PAGE = 'CATALOG/FETCH_PRODUCT_PAGE';
export const CHANGE_VISITED_PRODUCTS = 'CATALOG/CHANGE_VISITED_PRODUCTS';
export const CLEAR_CURRENT_PRODUCT = 'CATALOG/CLEAR_CURRENT_PRODUCT';
export const CATALOG_CURRENT_FILTERS_RESET =
  'CATALOG/CATALOG_CURRENT_FILTERS_RESET';
export const UPDATE_CURRENT_PRODUCT_DATA =
  'CATALOG/UPDATE_CURRENT_PRODUCT_DATA';
export const CATALOG_CURRENT_FILTERS_ADD =
  'CATALOG/CATALOG_CURRENT_FILTERS_ADD';
