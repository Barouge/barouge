import {
  FETCH_NEWS,
  FETCH_ACTIONS,
  SET_CURRENT_NEWS,
  CLEAR_CUR_NEWS,
  SHOW_MORE_NEWS,
} from '../types';

const initialState = {
  news: null,
  allNews: null,
  actions: null,
  onPage: 8,
  curPage: 1,
  totalNews: null,
  curNews: null,
};

export const newsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_NEWS:
      return {
        ...state,
        allNews: action.payload,
        totalNews: action.payload.length,
        news: action.payload.slice(
          (state.curPage - 1) * state.onPage,
          state.onPage,
        ),
      };

    case SHOW_MORE_NEWS:
      return {
        ...state,
        news: [
          ...state.news,
          ...state.allNews.slice(
            state.curPage * state.onPage,
            state.curPage * state.onPage + state.onPage,
          ),
        ],
        curPage: state.curPage + 1,
      };

    case CLEAR_CUR_NEWS:
      return { ...state, curNews: null };

    case SET_CURRENT_NEWS:
      return {
        ...state,
        curNews: state.allNews
          ? state.allNews.find((item) => item.id === action.payload)
          : null,
      };

    case FETCH_ACTIONS:
      return { ...state, actions: action.payload };

    default:
      return state;
  }
};
