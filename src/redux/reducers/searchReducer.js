import { SEARCH_QUERY, SEARCH_CLEAR } from '../types';

const initialState = {
  query: '',
  search: null,
};

export const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_QUERY:
      return {
        ...state,
        query: action.payload.query,
        search: action.payload.search,
      };

    case SEARCH_CLEAR:
      return initialState;

    default:
      return state;
  }
};
