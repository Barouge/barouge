import {
  BASKET_ADD_PRODUCT,
  BASKET_REMOVE_PRODUCT,
  BASKET_FETCH_INFO,
} from '../types';

const initialState = {
  data: null,
  products: null,
};

export const basketReducer = (state = initialState, action) => {
  switch (action.type) {
    case BASKET_ADD_PRODUCT:
      return {
        ...state,
        data: {
          ...state.data,
          count: state.data.count + 1,
        },
        products: state.products.find((o) => o.id === action.payload.id)
          ? state.products
          : [...state.products, action.payload],
      };

    case BASKET_REMOVE_PRODUCT:
      return {
        ...state,
        data: {
          ...state.data,
          count: state.data.count - 1,
        },
        products:
          action.payload.count < 1
            ? state.products.filter((o) => o.id !== action.payload.id)
            : state.products,
      };

    case BASKET_FETCH_INFO:
      return {
        ...state,
        products: action.payload.products,
        data: action.payload.data,
      };

    default:
      return state;
  }
};
