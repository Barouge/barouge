import {
  SHOW_LOADER,
  HIDE_LOADER,
  CHANGE_LOCALE,
  SET_LOCALE_FLAG,
  FETCH_SECTIONS,
  UPDATE_SUBSCRIBE,
  FETCH_PAYMENT_METHODS,
  FETCH_FAQ,
  FETCH_HOME_SLIDER,
  FETCH_TIZERS,
  FETCH_SEO,
} from '../types';

import FlagRus from '../../assets/icons/flags/flag-russia-ico.svg';

const initialState = {
  loading: false,
  defaultLocale: 'ru',
  locale: '',
  flag: FlagRus,
  languages: ['ru', 'en'],
  sections: null,
  subscribe: {
    status: false,
    error: null,
  },
  payMethods: null,
  faq: null,
  homeSlider: null,
  tizers: null,
  seo: null,
};

export const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SEO:
      return { ...state, seo: action.payload };

    case FETCH_TIZERS:
      return { ...state, tizers: action.payload };

    case FETCH_HOME_SLIDER:
      return { ...state, homeSlider: action.payload };

    case FETCH_FAQ:
      return { ...state, faq: action.payload };

    case FETCH_PAYMENT_METHODS:
      return { ...state, payMethods: action.payload };

    case UPDATE_SUBSCRIBE:
      return { ...state, subscribe: action.payload };

    case FETCH_SECTIONS:
      return { ...state, sections: action.payload };

    case SHOW_LOADER:
      return { ...state, loading: true };

    case HIDE_LOADER:
      return { ...state, loading: false };

    case CHANGE_LOCALE:
      return { ...state, locale: action.payload };

    case SET_LOCALE_FLAG:
      return { ...state, flag: action.payload };

    default:
      return state;
  }
};
