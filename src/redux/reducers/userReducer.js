import {
  FETCH_USER_INFO,
  FETCH_USER_ORDERS,
  REQUEST_PHONE_CODE,
  CHECK_PHONE_CODE,
  CHANGE_USER_DATA,
  FETCH_AUTH_ENTITY
} from '../types';

const initialState = {
  isAuth: null,
  user: null,
  orders: null,
  timer: null,
  codeStatus: null,
  authEntity: null
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_INFO:
      return {
        ...state,
        user: action.payload.data,
        isAuth: action.payload.status,
      };

    case FETCH_AUTH_ENTITY:
      return {
        ...state,
        authEntity: action.payload
      };

    case CHANGE_USER_DATA:
      return {
        ...state,
        user: {
          ...state.user,
          [action.payload.type]: action.payload.data,
        },
      };

    case FETCH_USER_ORDERS:
      return { ...state, orders: action.payload };

    case REQUEST_PHONE_CODE:
      return { ...state, timerStatus: action.payload };

    case CHECK_PHONE_CODE:
      return {
        ...state,
        codeStatus: action.payload.status,
      };

    default:
      return state;
  }
};
