import { addOrRemove } from '../../libs';
import {
  FETCH_CATALOG_FILTERS,
  FETCH_CATALOG_PRODUCTS,
  FETCH_CATALOG_MENU,
  CHANGE_CATALOG_VIEW,
  FETCH_CATALOG_PRODUCTS_MORE,
  CATALOG_PRICE_SORT,
  FETCH_RECOMMENDED_PRODUCTS,
  FETCH_BUYWITH_PRODUCTS,
  FETCH_CATEGORIES,
  FETCH_PRODUCT_PAGE,
  CHANGE_VISITED_PRODUCTS,
  UPDATE_CURRENT_PRODUCT_DATA,
  CLEAR_CURRENT_PRODUCT,
  SHOW_CATALOG_LOADER,
  HIDE_CATALOG_LOADER,
  CATALOG_CURRENT_FILTERS_UPDATE,
  CATALOG_CURRENT_FILTERS_ADD,
  CATALOG_CURRENT_FILTERS_RESET,
} from '../types';

const initialState = {
  filters: null,
  initialProducts: null,
  products: null,
  menu: null,
  view: 'block',
  onPage: 8,
  total: 0,
  recommended: null,
  categories: null,
  curProduct: null,
  buyWith: null,
  visited: [],
  loading: false,
  curFilters: {
    colors: [],
    series: [],
    material: [],
    price: '',
    new: false,
    tourism: false,
    children: false,
    auto: false,
    sale: false,
    page: 1,
  },
  initialFilters: {
    colors: [],
    series: [],
    material: [],
    price: '',
    new: false,
    tourism: false,
    children: false,
    auto: false,
    sale: false,
    page: 1,
  },
};

export const catalogReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCT_PAGE:
      return {
        ...state,
        curProduct: {
          data: {
            id: action.payload.id && action.payload.id,
            inCart: action.payload.inCart && action.payload.inCart,
            count: action.payload.count && action.payload.count,
            volume: action.payload.volume && action.payload.volume,
            color: action.payload.color && action.payload.color,
            vendor: action.payload.vendor && action.payload.vendor,
            title: action.payload.title && action.payload.title,
            price: action.payload.price && action.payload.price,
            colors:
              action.payload.volumes.length > 0 &&
              action.payload.volumes[0].colors,
          },
          product: action.payload,
        },
      };

    case UPDATE_CURRENT_PRODUCT_DATA:
      return {
        ...state,
        curProduct: {
          ...state.curProduct,
          data: action.payload,
        },
      };

    case CLEAR_CURRENT_PRODUCT:
      return { ...state, curProduct: null };

    case FETCH_CATEGORIES:
      return { ...state, categories: action.payload };

    case CATALOG_PRICE_SORT:
      return { ...state, products: action.payload };

    case FETCH_CATALOG_PRODUCTS_MORE:
      return {
        ...state,
        curFilters: {
          ...state.curFilters,
          page: Number(state.curFilters.page) + 1,
        },
      };

    case FETCH_CATALOG_FILTERS:
      return { ...state, filters: action.payload };

    case FETCH_BUYWITH_PRODUCTS:
      return { ...state, buyWith: action.payload };

    case FETCH_RECOMMENDED_PRODUCTS:
      return { ...state, recommended: action.payload };

    case FETCH_CATALOG_PRODUCTS:
      return {
        ...state,
        products: action.payload.products,
        initialProducts: action.payload.products,
        total: action.payload.total,
      };

    case FETCH_CATALOG_MENU:
      return { ...state, menu: action.payload };

    case CHANGE_CATALOG_VIEW:
      return { ...state, view: action.payload };

    case CHANGE_VISITED_PRODUCTS:
      return { ...state, visited: [...state.visited, action.payload] };

    case SHOW_CATALOG_LOADER:
      return { ...state, loading: true };

    case HIDE_CATALOG_LOADER:
      return { ...state, loading: false };

    case CATALOG_CURRENT_FILTERS_ADD:
      return {
        ...state,
        curFilters: { ...state.curFilters, ...action.payload },
      };

    case CATALOG_CURRENT_FILTERS_RESET:
      return {
        ...state,
        curFilters: state.initialFilters,
      };

    case CATALOG_CURRENT_FILTERS_UPDATE:
      return {
        ...state,
        curFilters: {
          ...state.curFilters,
          [action.payload.name]:
            typeof action.payload.value === 'boolean'
              ? action.payload.value
              : addOrRemove(
                  state.curFilters[action.payload.name],
                  action.payload.value,
                ),
        },
      };

    default:
      return state;
  }
};
