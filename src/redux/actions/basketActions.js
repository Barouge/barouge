import axios from 'axios';
import {
  BASKET_FETCH_INFO,
  BASKET_ADD_PRODUCT,
  BASKET_REMOVE_PRODUCT,
} from '../types';

export function fetchBasket() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/basket/basket.json');

      dispatch({ type: BASKET_FETCH_INFO, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function addBasketProduct(id) {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/basket/basket-add.json', {
        params: {
          id: id,
        },
      });

      dispatch({ type: BASKET_ADD_PRODUCT, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function removeBasketProduct(id) {
  return async (dispatch) => {
    try {
      const response = await axios.get(
        '/assets/api/basket/basket-remove.json',
        {
          params: {
            id: id,
          },
        },
      );

      dispatch({
        type: BASKET_REMOVE_PRODUCT,
        payload: response.data,
      });
    } catch (e) {
      console.log(e);
    }
  };
}
