import axios from 'axios';
import {
  SHOW_LOADER,
  HIDE_LOADER,
  FETCH_TOP_MENU,
  CHANGE_LOCALE,
  SET_LOCALE_FLAG,
  FETCH_SECTIONS,
  UPDATE_SUBSCRIBE,
  FETCH_PAYMENT_METHODS,
  FETCH_FAQ,
  FETCH_HOME_SLIDER,
  FETCH_TIZERS,
  FETCH_SEO,
} from '../types';

import FlagRus from '../../assets/icons/flags/flag-russia-ico.svg';
import FlagEng from '../../assets/icons/flags/flag-england-ico.svg';
import FlagBel from '../../assets/icons/flags/flag-belarus-ico.svg';
import FlagKaz from '../../assets/icons/flags/flag-kaz-ico.svg';

export function changeLocale(locale) {
  return async (dispatch) => {
    dispatch(showLoader());

    setTimeout(() => {
      dispatch({ type: CHANGE_LOCALE, payload: locale });
      dispatch(hideLoader());
    }, 300);
  };
}

export function fetchSeo(path) {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/app/seo.json', {
        params: {
          path: path,
        },
      });

      dispatch({ type: FETCH_SEO, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchTizers() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/app/tizers.json');

      dispatch({ type: FETCH_TIZERS, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchHomeSlider() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/app/homeSlider.json');

      dispatch({ type: FETCH_HOME_SLIDER, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchFaq() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/app/faq.json');

      dispatch({ type: FETCH_FAQ, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchPaymentMethods() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/app/paymentMethods.json');

      dispatch({ type: FETCH_PAYMENT_METHODS, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function updateSubscribe(email) {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/app/subscribe.json');

      dispatch({ type: UPDATE_SUBSCRIBE, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function setLocaleFlag(locale) {
  let flag;
  switch (locale) {
    case 'ru':
      flag = FlagRus;
      break;
    case 'en':
      flag = FlagEng;
      break;
    case 'bel':
      flag = FlagBel;
      break;
    case 'kz':
      flag = FlagKaz;
      break;

    default:
      flag = FlagRus;
  }

  return {
    type: SET_LOCALE_FLAG,
    payload: flag,
  };
}

export function showLoader() {
  return {
    type: SHOW_LOADER,
  };
}

export function hideLoader() {
  return {
    type: HIDE_LOADER,
  };
}

export function fetchTopMenu() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/app/topMenu.json');

      dispatch({ type: FETCH_TOP_MENU, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchSections() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/app/sections.json');

      dispatch({ type: FETCH_SECTIONS, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}
