import axios from 'axios';
import {
  CHECK_PHONE_CODE,
  REQUEST_PHONE_CODE,
  FETCH_USER_INFO,
  FETCH_USER_ORDERS,
  CHANGE_USER_DATA,
  FETCH_AUTH_ENTITY,
} from '../types';

export function changeUserData(data, type) {
  return async (dispatch) => {
    try {
      const response = await axios.get(
        '/assets/api/user/change-user-data.json',
      );

      dispatch({
        type: CHANGE_USER_DATA,
        payload: response.data,
      });
    } catch (e) {
      console.log(e);
    }
  };
}

export function requestPhoneCode(phone) {
  return async (dispatch) => {
    try {
      const response = await axios.get(
        '/assets/api/user/requestPhoneCode.json',
      );

      dispatch({ type: REQUEST_PHONE_CODE, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function checkPhoneCode(code) {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/user/checkPhoneCode.json');
      
      dispatch({ type: CHECK_PHONE_CODE, payload: response.data });

    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchUserInfo() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/user/userInfo.json');

      dispatch({ type: FETCH_USER_INFO, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchCheckAuthEntity(authData) {
  
  return async (dispatch) => {
    try {
      //пока некуда отправлять запрос
      // const response = await axios.post('/assets/api/user/checkAuthEntity.json',authData);


      //подставим типа готовый ответ
      const answer = {
        status: true,
        error: null
      }
      dispatch({ type: FETCH_AUTH_ENTITY, payload: answer });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchUserOrders(id) {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/user/userOrders.json');

      dispatch({ type: FETCH_USER_ORDERS, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}
