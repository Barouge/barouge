import axios from 'axios';
import { SEARCH_QUERY, SEARCH_CLEAR } from '../types';

export function searchQuery(query) {
  let url =
    query === ''
      ? '/assets/api/search/search-empty.json'
      : '/assets/api/search/search.json';
  return async (dispatch) => {
    try {
      const response = await axios.get(url, {
        params: {
          query: query,
        },
      });

      dispatch({
        type: SEARCH_QUERY,
        payload: response.data,
      });
    } catch (e) {
      console.log(e);
    }
  };
}

export function searchClear() {
  return {
    type: SEARCH_CLEAR,
  };
}
