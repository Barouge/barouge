import axios from 'axios';
import {
  FETCH_CATALOG_FILTERS,
  FETCH_CATALOG_PRODUCTS,
  FETCH_CATALOG_PRODUCTS_MORE,
  FETCH_CATALOG_MENU,
  CHANGE_CATALOG_VIEW,
  CATALOG_PRICE_SORT,
  FETCH_RECOMMENDED_PRODUCTS,
  FETCH_BUYWITH_PRODUCTS,
  FETCH_CATEGORIES,
  FETCH_PRODUCT_PAGE,
  CHANGE_VISITED_PRODUCTS,
  UPDATE_CURRENT_PRODUCT_DATA,
  CLEAR_CURRENT_PRODUCT,
  SHOW_CATALOG_LOADER,
  HIDE_CATALOG_LOADER,
  CATALOG_CURRENT_FILTERS_UPDATE,
  CATALOG_CURRENT_FILTERS_ADD,
  CATALOG_CURRENT_FILTERS_RESET,
} from '../types';

export function catalogCurrentFiltersUpdate(name, value, type) {
  return {
    type: CATALOG_CURRENT_FILTERS_UPDATE,
    payload: {
      name,
      value,
      type,
    },
  };
}

export function catalogCurrentFiltersReset() {
  return {
    type: CATALOG_CURRENT_FILTERS_RESET,
  };
}

export function catalogCurrentFiltersAdd(params) {
  return {
    type: CATALOG_CURRENT_FILTERS_ADD,
    payload: params,
  };
}

export function showCatalogLoader() {
  return {
    type: SHOW_CATALOG_LOADER,
  };
}

export function hideCatalogLoader() {
  return {
    type: HIDE_CATALOG_LOADER,
  };
}

export function changeCatalogView(view) {
  return {
    type: CHANGE_CATALOG_VIEW,
    payload: view,
  };
}

export function clearCurrentProduct() {
  return {
    type: CLEAR_CURRENT_PRODUCT,
  };
}

export function updateCurrentProductData(data) {
  return async (dispatch) => {
    try {
      const response = await axios.get(
        '/assets/api/catalog/update-product-data.json',
      );

      dispatch({ type: UPDATE_CURRENT_PRODUCT_DATA, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchCategories() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/catalog/categories.json');

      dispatch({ type: FETCH_CATEGORIES, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function catalogPriceSort(products) {
  return {
    type: CATALOG_PRICE_SORT,
    payload: products,
  };
}

export function fetchRecommendedProducts() {
  return async (dispatch) => {
    try {
      const response = await axios.get(
        '/assets/api/catalog/catalog-recommended.json',
      );

      dispatch({ type: FETCH_RECOMMENDED_PRODUCTS, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchBuyWithProducts() {
  return async (dispatch) => {
    try {
      const response = await axios.get(
        '/assets/api/catalog/catalog-buy-with.json',
      );

      dispatch({ type: FETCH_BUYWITH_PRODUCTS, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchCatalogFilters() {
  return async (dispatch) => {
    try {
      const response = await axios.get(
        '/assets/api/catalog/catalog-filters.json',
      );

      dispatch({ type: FETCH_CATALOG_FILTERS, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchCatalogProducts(location) {
  return async (dispatch) => {
    dispatch(showCatalogLoader());

    try {
      let url;
      switch (location.pathname) {
        case '/catalog/termos':
          url = '/assets/api/catalog/catalog/termos.json';

          break;
        case '/catalog/french-press':
          url = '/assets/api/catalog/catalog/french-press.json';

          break;
        default:
          url = '/assets/api/catalog/catalog/empty.json';

          break;
      }
      const response = await axios.get(
        'http://barouge2.ck31528.tmweb.ru/ajax/?action=getcatalog',
      );

      console.log(response);

      dispatch({
        type: FETCH_CATALOG_PRODUCTS,
        payload: {
          products: response.data.products,
          total: response.data.total,
        },
      });
      dispatch(hideCatalogLoader());
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchCatalogProductsMore() {
  return {
    type: FETCH_CATALOG_PRODUCTS_MORE,
  };
}

export function fetchCatalogMenu() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/catalog/catalog-menu.json');

      dispatch({ type: FETCH_CATALOG_MENU, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchProduct(id) {
  let url;

  switch (id) {
    case 'termos-adrenalin-black':
      url = '/assets/api/catalog/products/termos-adrenalin-black.json';

      break;
    case 'termos-adrenalin-blue':
      url = '/assets/api/catalog/products/termos-adrenalin-blue.json';

      break;
    case 'termos-adrenalin-red':
      url = '/assets/api/catalog/products/termos-adrenalin-red.json';

      break;
    case 'french-press-market-place-black-300':
      url = '/assets/api/catalog/products/french-press-black-300.json';

      break;
    default:
  }

  return async (dispatch) => {
    try {
      const response = await axios.get(url);

      dispatch({ type: FETCH_PRODUCT_PAGE, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function changeVisitedProducts(product) {
  return async (dispatch) => {
    try {
      const response = await axios.get(
        '/assets/api/catalog/change-visited.json',
      );

      if (response.data.status) {
        dispatch({ type: CHANGE_VISITED_PRODUCTS, payload: response.data });
      }
    } catch (e) {
      console.log(e);
    }
  };
}
