import axios from 'axios';
import {
  FETCH_NEWS,
  FETCH_ACTIONS,
  SET_CURRENT_NEWS,
  CLEAR_CUR_NEWS,
  SHOW_MORE_NEWS,
} from '../types';

export function fetchNews(data) {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/news/news.json', {
        params: {
          data: data,
        },
      });

      dispatch({ type: FETCH_NEWS, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchActions() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/assets/api/actions/actions.json');

      dispatch({ type: FETCH_ACTIONS, payload: response.data });
    } catch (e) {
      console.log(e);
    }
  };
}

export function setCurrentNews(id) {
  return {
    type: SET_CURRENT_NEWS,
    payload: id,
  };
}

export function clearCurNews() {
  return {
    type: CLEAR_CUR_NEWS,
  };
}

export function showMoreNews() {
  return {
    type: SHOW_MORE_NEWS,
  };
}
