import i18n from 'i18next';

import translationRU from './assets/locales/ru/translations.json';
import translationEN from './assets/locales/en/translations.json';

const resources = {
  ru: {
    translation: translationRU,
  },
  en: {
    translation: translationEN,
  },
};

i18n.init({
  resources,
  lng: 'ru',
  fallbackLng: 'ru',
  languages: ['ru', 'en'],
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
