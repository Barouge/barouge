import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import deepPurple from '@material-ui/core/colors/deepPurple';
import blueGrey from '@material-ui/core/colors/blueGrey';
import cyan from '@material-ui/core/colors/cyan';
import lightBlue from '@material-ui/core/colors/lightBlue';
import grey from '@material-ui/core/colors/grey';
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';

let theme = createMuiTheme({
  spacing: 10,
  breakpoints: {
    values: {
      xs: 0,
      sm: 480,
      md: 768,
      lg: 1280,
      df: 1440,
      xl: 1920,
      tablet: 1280,
    },
  },
  palette: {
    primary: blue,
    secondary: deepPurple,
    blueGrey: blueGrey,
    cyan: cyan,
    lightBlue: lightBlue,
    grey: grey,
    green: green,
    background: {
      default: '#fff',
    },
    red: red,
    text: {
      primary: '#000',
    },
  },
  color: {
    black: '#000000',
    white: `#ffffff`,
    greyLight: `#e3e3e3`,
    greyDark: '#2b2b2b',
    red: '#b71c1c',
    redLight: '#ee1c25',
    pink: '#ef426f',
    border: '#b1b4bd',
  },
  header: {
    height: '140px',
    row: {
      height: '40px',
      heightLarge: '60px',
    },
  },
  overrides: {
    MuiLinearProgress: {
      colorPrimary: {
        backgroundColor: blue[50],
      },
    },
    MuiFormHelperText: {
      root: {
        position: 'absolute',
        right: 0,
        padding: '3px',
        top: '-12px',
        marginTop: '0',
        backgroundColor: '#fff',
        fontSize: '12px',
        fontWeight: '700',
      },
    },
    MuiIconButton: {
      colorPrimary: {},
    },

    MuiButton: {
      root: {
        boxShadow: 'none',
        '&:hover': {
          boxShadow: 'none',
        },
      },
      contained: {
        boxShadow: 'none',
        '&:hover': {
          boxShadow: 'none',
        },
      },
      outlined: {
        minWidth: '150px',
      },
    },
    MuiInput: {
      underline: {
        '&::before, &::after': {
          display: 'none',
        },
      },
    },
    MuiAccordion: {
      root: {
        boxShadow: 'none',
        border: `solid 1px ${blue[200]}`,
        margin: '0 0 5px 0',
        '&.Mui-expanded': {
          margin: '0 0 5px 0',
        },
      },
    },
    MuiAccordionSummary: {
      root: {
        fontSize: '20px',
        fontWeight: '600',
        '&.Mui-expanded': {
          minHeight: '48px',
        },
      },
      content: {
        '&.Mui-expanded': {
          margin: '12px 0',
        },
      },
    },
    MuiAvatar: {
      root: {
        fontSize: '9px',
        width: '25px',
        height: '25px',
        padding: '2px',
        textTransform: 'uppercase',
      },
    },
    MuiDialogTitle: {
      root: {
        fontSize: '22px',
      },
    },
  },
  typography: {
    fontFamily: `"Akrobat", sans-serif`,
    fontSize: 14,
    fontWeightRegular: 400,
    fontWeightBold: 700,
    h1: {
      fontSize: '36px',
      fontWeight: 'bold',
    },
    h2: {
      fontSize: '30px',
      fontWeight: 'normal',
      textTransform: 'uppercase',
    },
    h3: {
      fontSize: '22px',
    },
    h4: {
      fontSize: '22px',
    },
    h5: {
      fontSize: '16px',
    },
    h6: {
      fontSize: '14px',
    },
    body1: {
      fontSize: '20px',
    },
    body2: {
      fontSize: '16px',
    },
    a: {
      textDecoration: 'none',
    },
  },
});

theme = responsiveFontSizes(theme);

export default theme;
