import React, { Suspense } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { ThemeProvider } from '@material-ui/core/styles';
import { LinearProgress } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';


import Layout from './components/Layout';

import routes from './routes';
import theme from './theme';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <BrowserRouter>
        <Suspense fallback={<LinearProgress color="primary" />}>
          <Layout>{renderRoutes(routes)}</Layout>
        </Suspense>
      </BrowserRouter>
    </ThemeProvider>
  );
};

export default App;
