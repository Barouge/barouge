import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {},
}));

const Content = ({ content }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box dangerouslySetInnerHTML={{ __html: content }} />
    </Box>
  );
};

export default Content;
