import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  TextField,
  InputAdornment,
  IconButton,
  Tooltip,
  Snackbar,
} from '@material-ui/core';
import { Formik, Form, Field } from 'formik';
import EditIcon from '@material-ui/icons/Edit';
import DoneIcon from '@material-ui/icons/Done';
import * as yup from 'yup';
import { useDispatch } from 'react-redux';
import clsx from 'clsx';
import { changeUserData } from '../../redux/actions/userActions';

const useStyles = makeStyles((theme) => ({
  root: {},
  title: {
    marginBottom: '30px',
  },
  field: {
    width: '100%',
    marginBottom: '40px',
    '&._confirmed': {
      '& .MuiFormHelperText-root': {
        '&.Mui-disabled': {
          color: theme.palette.green[500],
        },
      },
    },
    '&._warning': {
      '& .MuiFormHelperText-root': {
        '&.Mui-disabled': {
          color: '#ff9800',
        },
      },
    },
  },
}));

const validationSchema = yup.object().shape({});

const initialEdit = {
  edit: false,
  updated: false,
  value: '',
};

const PersonalData = ({ user }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [message, setMessage] = useState('Ваши данные успешно обновлены');
  const [nameEdit, setNameEdit] = useState(initialEdit);
  const [adressEdit, setAdressEdit] = useState(initialEdit);

  useEffect(() => {
    if (nameEdit.updated) {
      setMessage('Имя успешно обновлено!');

      return;
    }

    if (adressEdit.updated) {
      setMessage('Адрес успешно обновлен!');

      return;
    }
  }, [nameEdit.updated, adressEdit.updated]);

  const handleChange = (e, type) => {
    const value = e.target.value;

    switch (type) {
      case 'name':
        dispatch(changeUserData(value, 'name'));
        setNameEdit({
          ...nameEdit,
          value: value,
        });

        break;
      case 'adress':
        dispatch(changeUserData(value, 'adress'));
        setAdressEdit({
          ...adressEdit,
          value: value,
        });

        break;
      default:
    }
  };

  const handleSave = (type) => {
    switch (type) {
      case 'name':
        setNameEdit({
          edit: false,
          updated: true,
        });

        setTimeout(() => {
          setNameEdit(initialEdit);
        }, 3000);

        break;
      case 'adress':
        setAdressEdit({
          edit: false,
          updated: true,
        });

        setTimeout(() => {
          setAdressEdit(initialEdit);
        }, 3000);

        break;
      default:
    }
  };

  console.log(nameEdit);
  console.log(adressEdit);

  const handleEdit = (type) => {
    switch (type) {
      case 'name':
        setNameEdit({
          ...nameEdit,
          edit: true,
        });

        break;
      case 'adress':
        setAdressEdit({
          edit: true,
        });

        break;
      default:
    }
  };

  const adornment = (name) => {
    let variant = '';

    switch (name) {
      case 'name':
        variant = nameEdit.edit;

        break;
      case 'adress':
        variant = adressEdit.edit;

        break;
      default:
    }

    return {
      endAdornment: (
        <InputAdornment position="end">
          {variant ? (
            <Tooltip title="Сохранить">
              <IconButton onClick={() => handleSave(name)} color="primary">
                <DoneIcon color="primary" />
              </IconButton>
            </Tooltip>
          ) : (
            <Tooltip title="Изменить">
              <IconButton onClick={() => handleEdit(name)} color="primary">
                <EditIcon color="primary" />
              </IconButton>
            </Tooltip>
          )}
        </InputAdornment>
      ),
    };
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.wrapper}>
        <Box className={classes.form}>
          <Formik validationSchema={validationSchema}>
            <Form noValidate autoComplete="off">
              {user.name && (
                <Field
                  component={TextField}
                  name="name"
                  label="ФИО"
                  variant="outlined"
                  disabled={!nameEdit.edit}
                  value={user.name}
                  className={classes.field}
                  onChange={(e) => handleChange(e, 'name')}
                  InputProps={adornment('name')}
                />
              )}
              {user.email && (
                <Field
                  component={TextField}
                  type="email"
                  name="email"
                  label="E-mail"
                  variant="outlined"
                  disabled
                  value={user.email.email}
                  className={clsx(
                    classes.field,
                    user.email.confirmed ? '_confirmed' : '_warning',
                  )}
                  helperText={
                    user.email.confirmed
                      ? 'Почта подтверждена'
                      : 'Почта не подтверждена'
                  }
                />
              )}
              {user.phone && (
                <Field
                  component={TextField}
                  name="phone"
                  label="Телефон"
                  variant="outlined"
                  disabled
                  value={user.phone.phone}
                  className={classes.field}
                />
              )}
              {user.adress && (
                <Field
                  component={TextField}
                  name="adress"
                  label="Адрес"
                  variant="outlined"
                  disabled={!adressEdit.edit}
                  value={user.adress}
                  className={classes.field}
                  onChange={(e) => handleChange(e, 'adress')}
                  InputProps={adornment('adress')}
                />
              )}
            </Form>
          </Formik>
        </Box>
      </Box>
      {(nameEdit.updated || adressEdit.updated) && (
        <Snackbar
          open={nameEdit.updated || adressEdit.updated}
          message={message}
          key={''}
        />
      )}
    </Box>
  );
};

export default PersonalData;
