import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {},
}));

const CabinetFavorite = () => {
  const classes = useStyles();

  return <Box className={classes.root}>Избранное</Box>;
};

export default CabinetFavorite;
