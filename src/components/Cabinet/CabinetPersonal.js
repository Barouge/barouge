import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Grid,
  FormControlLabel,
  Switch,
} from '@material-ui/core';

import PersonalData from '../PersonalData';

const useStyles = makeStyles(() => ({
  root: {},
  paper: {
    padding: '20px',
  },
  title: {
    marginBottom: '30px',
  },
  field: {
    width: '100%',
    marginBottom: '40px',
  },
}));

const CabinetPersonal = ({ user }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Grid container spacing={4} className={classes.wrapper}>
        <Grid item xs={12} lg={8}>
          <Box className={classes.form}>
            <Box className={classes.title}>Персональные данные</Box>
            <PersonalData user={user} />
          </Box>
        </Grid>
        <Grid item xs={12} lg={4}>
          <Box className={classes.aside}>
            <Box className={classes.row}>
              <FormControlLabel
                control={
                  <Switch checked={true} name="checkedB" color="primary" />
                }
              />
              Подписаться на рассылку
            </Box>
            {user.discount && (
              <Box className={classes.row}>
                Ваша персональная скидка {user.discount}
              </Box>
            )}
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default CabinetPersonal;
