import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

import CabinetPersonal from './CabinetPersonal';
import CabinetOrders from './CabinetOrders';
import CabinetFavorite from './CabinetFavorite';

const useStyles = makeStyles((theme) => ({
  root: {},
}));

const Cabinet = ({ user, type }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      {type === 'personal' && <CabinetPersonal user={user} />}
      {type === 'orders' && <CabinetOrders />}
      {type === 'favorites' && <CabinetFavorite />}
    </Box>
  );
};

export default Cabinet;
