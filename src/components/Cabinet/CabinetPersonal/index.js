import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Grid, Paper, Typography, TextField } from '@material-ui/core';
import { Formik, Form, Field } from 'formik';
import * as yup from 'yup';

const useStyles = makeStyles(() => ({
  root: {},
  paper: {
    padding: '20px',
  },
  title: {
    marginBottom: '30px',
  },
  field: {
    width: '100%',
  },
}));

const validationSchema = yup.object().shape({});

const CabinetPersonal = ({ user }) => {
  const classes = useStyles();

  const handleSubmit = () => {
    console.log('FormikValues');
  };

  return (
    <Box className={classes.root}>
      <Grid container spacing={5}>
        <Grid item xs={6}>
          <Paper className={classes.paper}>
            <Typography variant="h3" className={classes.title}>
              Личные данные
            </Typography>

            <Formik validationSchema={validationSchema} onSubmit={handleSubmit}>
              {({}) => {
                return (
                  <Form noValidate autoComplete="off">
                    <Grid container spacing={5}>
                      {user.officialName !== undefined && (
                        <Grid item xs={12}>
                          <Field
                            component={TextField}
                            name="name"
                            label="ФИО"
                            value={user.officialName}
                            className={classes.field}
                          />
                        </Grid>
                      )}
                      {user.email !== undefined && (
                        <Grid item xs={12}>
                          <Field
                            component={TextField}
                            name="email"
                            label="E-mail"
                            value={user.email}
                            className={classes.field}
                          />
                        </Grid>
                      )}
                      {user.phone !== undefined && (
                        <Grid item xs={12}>
                          <Field
                            component={TextField}
                            name="phone"
                            label="Телефон"
                            value={user.phone}
                            className={classes.field}
                          />
                        </Grid>
                      )}
                    </Grid>
                  </Form>
                );
              }}
            </Formik>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          Адрес доставки
        </Grid>
      </Grid>
    </Box>
  );
};

export default CabinetPersonal;
