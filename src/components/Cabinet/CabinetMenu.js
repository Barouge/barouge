import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import { Link } from 'react-router-dom';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  root: {},
  link: {
    transition: 'ease .3s all',
    position: 'relative',
    display: 'block',
    height: '60px',
    lineHeight: '60px',
    fontSize: '20px',
    fontWeight: '700',
    padding: `0 25px`,
    color: '#000',
    '&:hover': {
      color: theme.palette.primary[300],
    },
    '&::before': {
      transition: 'inherit',
      content: `''`,
      position: 'absolute',
      top: '0',
      left: '0',
      height: '100%',
      width: '3px',
      backgroundColor: theme.palette.primary[300],
      transform: 'scale(0)',
    },
    '&._active': {
      '&::before': {
        transform: 'scale(1)',
      },
    },
  },
}));

const CabinetMenu = ({ type, setType }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box
        component={Link}
        onClick={() => setType('personal')}
        to={'/cabinet/personal'}
        className={clsx(classes.link, type === 'personal' && '_active')}
      >
        Настройки
      </Box>
      <Box
        component={Link}
        onClick={() => setType('orders')}
        to={'/cabinet/orders'}
        className={clsx(classes.link, type === 'orders' && '_active')}
      >
        Мои заказы
      </Box>
      <Box
        component={Link}
        onClick={() => setType('favorites')}
        to={'/cabinet/favorites'}
        className={clsx(classes.link, type === 'favorites' && '_active')}
      >
        Мне понравилось
      </Box>
    </Box>
  );
};

export default CabinetMenu;
