import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';

import { fetchUserOrders } from '../../../redux/actions/userActions';

import CabinetOrdersItem from './CabinetOrdersItem';

const useStyles = makeStyles(() => ({
  root: {},
}));

const CabinetOrders = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const orders = useSelector((state) => state.user.orders);

  useEffect(() => {
    if (orders) return;

    dispatch(fetchUserOrders());
  }, [orders]);

  return (
    <Box className={classes.root}>
      {orders &&
        orders.map((order) => {
          return <CabinetOrdersItem key={order.id} order={order} />;
        })}
    </Box>
  );
};

export default CabinetOrders;
