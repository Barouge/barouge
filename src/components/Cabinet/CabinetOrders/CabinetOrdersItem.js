import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {
    marginBottom: '20px',
  },
}));

const CabinetOrdersItem = ({ order }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      Заказ №{order.vendor}
      <br />
      Статус: {order.status}
    </Box>
  );
};

export default CabinetOrdersItem;
