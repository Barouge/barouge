import React from 'react';
import { useTranslation } from 'react-i18next';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import SearchBar from 'material-ui-search-bar';
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import { searchQuery } from '../../redux/actions/searchActions';

import SearchList from './SearchList';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
  },
  search: {
    transition: 'ease .3s all',
    height: '45px',
    boxShadow: 'none',
    backgroundColor: '#fff',
    '&:hover, &._opened': {
      boxShadow: `0 2px 8px ${theme.palette.grey[700]}`,
      color: '#fff',
    },
    '&._opened': {
      position: 'relative',
      borderRadius: '5px 5px 0 0',
    },
  },
  result: {
    position: 'absolute',
    top: '65px',
    left: '0',
    width: '100%',
    padding: `${theme.spacing(2)} 0`,
    backgroundColor: '#fff',
    borderRadius: '0 0 5px 5px',
    boxShadow: `0 4px 12px ${theme.palette.grey[700]}`,
    '&::before': {
      content: `''`,
      position: 'absolute',
      bottom: '100%',
      left: '0',
      width: '100%',
      height: '20px',
      backgroundColor: '#fff',
    },
  },
}));

const Search = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const search = useSelector((state) => state.search);
  const { t } = useTranslation();

  const handleChange = (val) => {
    dispatch(searchQuery(val));
  };

  const handleSubmit = (val) => {};

  const handleClear = () => {
    dispatch(searchQuery(''));
  };

  return (
    <Box className={clsx(classes.root, search.query !== '' && '_opened')}>
      <SearchBar
        placeholder={t('Search')}
        value={search.query}
        onChange={handleChange}
        onRequestSearch={handleSubmit}
        onCancelSearch={handleClear}
        className={clsx(classes.search, search.query !== '' && '_opened')}
      />
      {search.search && search.search.total > 0 && (
        <Box className={classes.result}>
          <Box className={classes.list}>
            <SearchList search={search.search} />
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default Search;
