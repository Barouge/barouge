import React from 'react';
import { Box, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import SearchItem from './SearchItem';

const useStyles = makeStyles(() => ({
  root: {},
  inner: {
    maxHeight: '330px',
    overflowY: 'auto',
    overflowX: 'hidden',
  },
}));

const SearchList = ({ search: { products, term } }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.inner}>
        <Grid container spacing={2}>
          {products.map((item) => {
            return (
              <Grid item>
                <SearchItem key={item.id} term={term} product={item} />
              </Grid>
            );
          })}
        </Grid>
      </Box>
    </Box>
  );
};

export default SearchList;
