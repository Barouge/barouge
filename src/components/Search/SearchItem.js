import React from 'react';
import { Box, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { searchClear } from '../../redux/actions/searchActions';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'block',
    color: theme.color.black,
    height: '110px',
  },
  wrapper: {
    maxHeight: '100%',
  },
  image: {
    width: '100%',
    height: '110px',
    objectFit: 'contain',
  },
  cell: {
    maxHeight: '100%',
  },
}));

const SearchItem = ({ product }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <Box
      component={Link}
      to={product.href}
      className={classes.root}
      onClick={() => {
        dispatch(searchClear());
      }}
    >
      <Grid container spacing={2} className={classes.wrapper}>
        {product.previewImage && (
          <Grid className={classes.cell} item xs={3}>
            <img className={classes.image} src={product.previewImage} alt="" />
          </Grid>
        )}
        {product.shortName && (
          <Grid className={classes.cell} item xs={6}>
            {product.shortName}
          </Grid>
        )}
        {product.price && (
          <Grid className={classes.cell} item xs={3}>
            {product.price}
          </Grid>
        )}
      </Grid>
    </Box>
  );
};

export default SearchItem;
