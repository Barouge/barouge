import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Tooltip } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPaymentMethods } from '../../redux/actions/appActions';
import clsx from 'clsx';

import MasterCardIcon from '../../icons/MasterCardIcon';
import VisaIcon from '../../icons/VisaIcon';
import MirIcon from '../../icons/MirIcon';
import ApplePayIcon from '../../icons/ApplePayIcon';
import GooglePayIcon from '../../icons/GooglePayIcon';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    height: '100%',
  },
  icon: {
    width: '60px',
    height: '30px',
    marginRight: '15px',
    '&._master': {
      height: '25px',
      marginRight: '5px',
    },
    '&:last-child': {
      marginRight: '0',
    },
    '& svg': {
      transition: 'ease .3s all',
      width: '100%',
      height: '100%',
    },
  },
}));

const PaymentMethods = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const payMethods = useSelector((state) => state.app.payMethods);

  useEffect(() => {
    if (payMethods) return;

    dispatch(fetchPaymentMethods());
  }, []);

  if (!payMethods) return null;

  return (
    <Box className={classes.root}>
      {payMethods.map((item, i) => {
        let icon;

        switch (item.type) {
          case 'master':
            icon = <MasterCardIcon />;

            break;
          case 'visa':
            icon = <VisaIcon />;

            break;
          case 'mir':
            icon = <MirIcon />;

            break;
          case 'apple':
            icon = <ApplePayIcon />;

            break;
          case 'google':
            icon = <GooglePayIcon />;

            break;
          default:
            return;
        }
        return (
          <Tooltip key={i} title={item.name}>
            <Box
              className={clsx(
                classes.icon,
                item.type === 'master' && '_master',
              )}
            >
              {icon}
            </Box>
          </Tooltip>
        );
      })}
    </Box>
  );
};

export default PaymentMethods;
