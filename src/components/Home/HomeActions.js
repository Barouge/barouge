import React from 'react';
import {
  Box,
  Grid,
  Container,
  Typography,
  Button,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import Masonry from 'react-masonry-css';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ActionsItem from '../Actions/ActionsItem';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: `${theme.spacing(2)}px 0`,
  },
  masonry: {
    display: 'flex',
    marginLeft: `-${theme.spacing(2)}px`,
    width: 'auto',
  },
  col: {
    paddingLeft: `${theme.spacing(2)}px`,
    backgroundClip: 'padding-box',
  },
  lastCol: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    display: 'flex',
    minWidth: '140px',
    justifyContent: 'space-between',
    paddingRight: '10px',
    borderColor: theme.palette.primary[300],
    color: theme.palette.primary[600],
    '&:hover': {
      backgroundColor: theme.palette.primary[300],
      color: '#fff',
    },
  },
}));

const HomeActions = ({ actions }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.root}>
        <Container>
          <Grid container spacing={4}>
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={10}>
                  <Typography variant="h2">Акции</Typography>
                </Grid>
                <Grid item xs={2} className={classes.lastCol}>
                  <Button
                    className={classes.button}
                    component={Link}
                    to={`/actions`}
                    variant="outlined"
                  >
                    Все акции
                    <ArrowForwardIcon className={classes.iconForwd} />
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            {actions && (
              <Grid item xs={12}>
                <Masonry
                  breakpointCols={window.innerWidth > 1280 ? 2 : 1}
                  className={classes.masonry}
                  columnClassName={classes.col}
                >
                  {actions.map((item, i) => {
                    if (i > 2) return;

                    return <ActionsItem home={true} item={item} key={i} />;
                  })}
                </Masonry>
              </Grid>
            )}
          </Grid>
        </Container>
      </Box>
    </Box>
  );
};

export default HomeActions;
