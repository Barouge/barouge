import React from 'react';
import { Box, Grid, Container, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { Link } from 'react-router-dom';
import Masonry from 'react-masonry-css';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: `${theme.spacing(4)}px 0 ${theme.spacing(2)}px`,
  },
  masonry: {
    display: 'flex',
    marginLeft: `-${theme.spacing(2)}px`,
    width: 'auto',
  },
  col: {
    paddingLeft: `${theme.spacing(3)}px`,
    backgroundClip: 'padding-box',
  },
  wrapper: {
    display: 'block',
    position: 'relative',
    overflow: 'hidden',
    height: '200px',
    width: '600px',
    marginBottom: `${theme.spacing(2)}px`,
    '&._long': {
      height: '640px',
    },
    '&._pre-long': {
      height: '420px',
    },
    '&:hover': {
      '& ._item': {
        transform: 'scale(.7)',
      },
    },
    [theme.breakpoints.down('md')]: {
      marginBottom: '10px',
      height: '250px',
      width: '100%',
      '&._long, &._pre-long': {
        height: '250px',
      },
    },
  },
  item: {
    transition: 'ease .3s all',
    position: 'absolute',
    top: '-150px',
    bottom: '-150px',
    left: '-150px',
    right: '-150px',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '50% 50%',
  },
  name: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    color: '#fff',
    fontWeight: '900',
    fontSize: '40px',
    whiteSpace: 'nowrap',
  },
}));

const HomeCategories = ({ categories }) => {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      <Container>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Typography variant="h2">Категории</Typography>
          </Grid>
          <Grid xs={12} item>
            {categories && (
              <Grid item xs={12}>
                <Masonry
                  breakpointCols={window.innerWidth > 1280 ? 2 : 1}
                  className={classes.masonry}
                  columnClassName={classes.col}
                >
                  {categories.map((item, i) => {
                    const long = item.id === 1;
                    const prelong = item.id === 4;

                    return (
                      <Box
                        component={Link}
                        to={item.href}
                        key={i}
                        className={clsx(
                          classes.wrapper,
                          long && '_long',
                          prelong && '_pre-long',
                        )}
                      >
                        {item.image && (
                          <Box
                            style={{ backgroundImage: `url(${item.image})` }}
                            className={clsx(classes.item, '_item')}
                          ></Box>
                        )}
                        {item.name && (
                          <Box className={classes.name}>{item.name}</Box>
                        )}
                      </Box>
                    );
                  })}
                </Masonry>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default HomeCategories;
