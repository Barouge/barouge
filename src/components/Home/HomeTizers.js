import React from 'react';
import { Box, Grid, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import TizerDiscountIcon from '../../icons/TizerDiscountIcon';
import TizerDeliveryIcon from '../../icons/TizerDeliveryIcon';
import TizerQualityIcon from '../../icons/TizerQualityIcon';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100px',
    padding: '10px 0',
    backgroundColor: theme.palette.grey[200],
    [theme.breakpoints.down('md')]: {
      height: 'auto',
    },
  },
  tizer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '80px',
    height: '80px',
    backgroundColor: '#fff',
    borderRadius: '50%',
    color: theme.palette.grey[800],
    marginRight: '20px',
    '& svg': {
      width: '28px',
      height: '28px',
    },
    [theme.breakpoints.down('md')]: {
      marginRight: '10px',
      marginBottom: '0',
      width: '50px',
      height: '50px',
    },
  },
  title: {
    fontSize: '16px',
    [theme.breakpoints.down('md')]: {
      fontSize: '12px',
    },
  },
}));

const HomeTizers = ({ tizers }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Container>
        {tizers && (
          <Grid container spacing={2}>
            {tizers.map((tizer) => {
              let icon;

              switch (tizer.icon) {
                case 'money':
                  icon = <AttachMoneyIcon />;

                  break;
                case 'discount':
                  icon = <TizerDiscountIcon />;

                  break;
                case 'delivery':
                  icon = <TizerDeliveryIcon />;

                  break;
                case 'quality':
                  icon = <TizerQualityIcon />;

                  break;
                default:
              }
              return (
                <Grid key={tizer.id} item xs={6} lg={3}>
                  <Box className={classes.tizer}>
                    <Box className={classes.icon}>{icon}</Box>
                    <Box className={classes.title}>{tizer.title}</Box>
                  </Box>
                </Grid>
              );
            })}
          </Grid>
        )}
      </Container>
    </Box>
  );
};

export default HomeTizers;
