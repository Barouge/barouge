import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import Carousel from '../Carousel';

const useStyles = makeStyles(() => ({
  root: {},
}));

const HomeSlider = ({ slides }) => {
  const classes = useStyles();
  const settings = {
    dots: true,
    arrows: true,
    infinite: true,
    autoplay: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <Box className={classes.root}>
      <Carousel settings={settings} items={slides} />
    </Box>
  );
};

export default HomeSlider;
