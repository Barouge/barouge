import React from 'react';
import { Box, Grid, Container, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import FaqList from '../FaqList';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: `${theme.spacing(2)}px 0`,
  },
  lastCol: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    display: 'flex',
    minWidth: '140px',
    justifyContent: 'space-between',
    paddingRight: '10px',
    borderColor: theme.palette.primary[300],
    color: theme.palette.primary[600],
    '&:hover': {
      backgroundColor: theme.palette.primary[300],
      color: '#fff',
    },
  },
}));

const HomeFaq = ({ faq }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Container>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={10}>
                <Typography variant="h2">Вопрос-Ответ</Typography>
              </Grid>
              <Grid item xs={2} className={classes.lastCol}>
                <Button
                  className={classes.button}
                  component={Link}
                  to={`/faq`}
                  variant="outlined"
                >
                  FAQ
                  <ArrowForwardIcon className={classes.iconForwd} />
                </Button>
              </Grid>
            </Grid>
          </Grid>
          {faq && (
            <Grid xs={12} item>
              <FaqList faq={faq} />
            </Grid>
          )}
        </Grid>
      </Container>
    </Box>
  );
};

export default HomeFaq;
