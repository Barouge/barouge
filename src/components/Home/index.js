import React, { useEffect } from 'react';
import { Box, Grid, Hidden } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { useDispatch, useSelector } from 'react-redux';
import {
  fetchCategories,
} from '../../redux/actions/catalogActions';
import {
  fetchHomeSlider,
  fetchTizers,
  fetchFaq,
} from '../../redux/actions/appActions';
import { fetchNews, fetchActions } from '../../redux/actions/newsActions';
import clsx from 'clsx';

import HomeSlider from './HomeSlider';
import HomeTizers from './HomeTizers';
import HomeCategories from './HomeCategories';
import HomeNews from './HomeNews';
import HomeActions from './HomeActions';
import HomeFaq from './HomeFaq';
import Loader from '../Loader';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingBottom: theme.spacing(10),
  },
  row: {
    minHeight: '500px',
    position: 'relative',
    '&._normal': {
      minHeight: '0',
    },
  },
}));

const Home = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { categories } = useSelector((state) => state.catalog);
  const { homeSlider, tizers, faq } = useSelector((state) => state.app);
  const { actions, news } = useSelector((state) => state.news);



  useEffect(() => {
    dispatch(fetchFaq());
    dispatch(fetchActions());
    dispatch(fetchNews());
    dispatch(fetchHomeSlider());
    dispatch(fetchTizers());
    dispatch(fetchCategories());
  }, []);

  return (
    <Box className={classes.root}>
      <Grid container>
        <Hidden mdDown>
          <Grid className={classes.row} item xs={12}>
            {homeSlider ? <HomeSlider slides={homeSlider} /> : <Loader />}
          </Grid>
        </Hidden>
        <Grid className={clsx(classes.row, '_normal')} item xs={12}>
          {tizers ? <HomeTizers tizers={tizers} /> : <Loader />}
        </Grid>
        <Grid className={classes.row} item xs={12}>
          {categories ? <HomeCategories categories={categories} /> : <Loader />}
        </Grid>
        <Hidden mdDown>
          <Grid className={classes.row} item xs={12}>
            {actions ? <HomeActions actions={actions} /> : <Loader />}
          </Grid>
        </Hidden>
        <Grid className={classes.row} item xs={12}>
          {news ? <HomeNews news={news} /> : <Loader />}
        </Grid>
        <Grid className={classes.row} item xs={12}>
          {faq ? <HomeFaq faq={faq} /> : <Loader />}
        </Grid>
      </Grid>
    </Box>
  );
};

export default Home;
