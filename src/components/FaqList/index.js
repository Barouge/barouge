import React, { useState } from 'react';
import {
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
  root: {},
  item: {
    marginBottom: '20px',
  },
  question: {},
  icon: {
    color: theme.palette.primary[200],
  },
}));

const FaqList = ({ faq }) => {
  const classes = useStyles();
  const [expanded, setExpanded] = useState('');

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  return (
    <Box className={classes.root}>
      {faq.map((item, i) => {
        return (
          <Accordion
            square={true}
            expanded={expanded === `panel-${i}`}
            key={item.id}
            onChange={handleChange(`panel-${i}`)}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon className={classes.icon} />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              {item.question}
            </AccordionSummary>

            <AccordionDetails>
              <Box
                className={classes.answer}
                dangerouslySetInnerHTML={{ __html: item.answer }}
              />
            </AccordionDetails>
          </Accordion>
        );
      })}
    </Box>
  );
};

export default FaqList;
