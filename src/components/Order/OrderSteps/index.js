import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Button,
  Box,
} from '@material-ui/core';

import OrderAuthStep from './OrderAuthStep';
import OrderDeliveryStep from './OrderDeliveryStep';
import OrderPaymentStep from './OrderPaymentStep';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
}));

function getSteps() {
  return [
    'Данные покупателя',
    'Выберите способ доставки',
    'Выберите способ оплаты',
  ];
}

const Order = ({ activeStep, setActiveStep }) => {
  const classes = useStyles();

  const steps = getSteps();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const renderSwitch = (label) => {
    switch (label) {
      case 'Данные покупателя':
        return <OrderAuthStep />;

        break;
      case 'Выберите способ доставки':
        return <OrderDeliveryStep />;

        break;
      case 'Выберите способ оплаты':
        return <OrderPaymentStep />;

        break;
      default:
        return <Box className={classes.auth}>Непонятно</Box>;
    }
  };

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((label, index) => {
          return (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
              <StepContent>
                {renderSwitch(label)}

                <div className={classes.actionsContainer}>
                  <div>
                    <Button
                      disabled={activeStep === 0}
                      onClick={handleBack}
                      className={classes.button}
                    >
                      Back
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={handleNext}
                      className={classes.button}
                    >
                      {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                    </Button>
                  </div>
                </div>
              </StepContent>
            </Step>
          );
        })}
      </Stepper>
    </div>
  );
};

export default Order;
