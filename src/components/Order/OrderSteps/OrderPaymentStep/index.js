import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  root: {},
}));

const OrderPaymentStep = () => {
  const classes = useStyles();

  return <div className={classes.root}>OrderPaymentStep</div>;
};

export default OrderPaymentStep;
