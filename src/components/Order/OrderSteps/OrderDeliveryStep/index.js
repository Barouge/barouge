import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  root: {},
}));

const OrderDeliveryStep = () => {
  const classes = useStyles();

  return <div className={classes.root}>OrderDeliveryStep</div>;
};

export default OrderDeliveryStep;
