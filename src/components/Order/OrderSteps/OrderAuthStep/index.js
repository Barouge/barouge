import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import AuthEntity from '../../../Auth/AuthEntity';
import CabinetPersonal from '../../../Cabinet/CabinetPersonal';

const useStyles = makeStyles(() => ({
  root: {},
}));

const OrderAuthStep = () => {
  const classes = useStyles();
  const user = useSelector((state) => state.user);

  return (
    <div className={classes.root}>
      {user.isAuth ? <CabinetPersonal user={user.user} /> : <AuthEntity />}
    </div>
  );
};

export default OrderAuthStep;
