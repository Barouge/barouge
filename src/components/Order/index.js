import React, { useState } from 'react';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import OrderSteps from './OrderSteps';
import OrderAside from './OrderAside';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    position: 'relative',
  },
  steps: {
    width: 'calc(100% - 380px)',
    marginRight: '30px',
  },
}));

const Order = () => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);

  return (
    <Box className={classes.root}>
      <Box className={classes.steps}>
        <OrderSteps activeStep={activeStep} setActiveStep={setActiveStep} />
      </Box>
      <Box>
        <OrderAside activeStep={activeStep} />
      </Box>
    </Box>
  );
};

export default Order;
