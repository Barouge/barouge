import React from 'react';
import { Box, Paper, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  root: {
    width: '350px',
    position: 'sticky',
    top: '120px',
    height: '100px',
  },
}));

const OrderAside = ({ activeStep }) => {
  const classes = useStyles();

  const handleOrderSubmit = () => {
    console.log('поддтвердить заказ');
  };

  return (
    <Box className={classes.root}>
      {activeStep === 3 && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Button
            color="primary"
            variant="outlined"
            onClick={handleOrderSubmit}
            className={classes.button}
          >
            Подтвердить заказ
          </Button>
        </Paper>
      )}
    </Box>
  );
};

export default OrderAside;
