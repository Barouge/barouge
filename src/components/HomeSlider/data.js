export const items = [
  {
    title: 'Термос',
    subTitle: 'Вакуумный',
    text:
      'Подходит для путешествий, согреет горячим напитком при занятии зимними видами спорта.',
    link: {
      href: '/',
      name: 'Подробнее',
    },
    type: 'variant1',
    images: [
      '/assets/images/banners/banner1-2.png',
      '/assets/images/banners/banner1-1.png',
    ],
  },
  {
    title: 'Кувшин — Термос',
    subTitle: 'Вакуумный',
    text:
      'Практичный атрибут на вашей кухне в городе или загородом заключённый в элегантную форму',
    link: {
      href: '',
      name: 'Смотреть цветовую линейку',
    },
    type: 'variant2',
    images: [
      '/assets/images/banners/banner2-1.png',
      '/assets/images/banners/banner2-2.png',
    ],
  },
  {
    title: 'Термобутылка',
    text: 'Идеально вписывается во все стандартные подстаканники',
    link: {
      href: '',
      name: 'Подробнее',
    },
    type: 'variant3',
    images: [
      '/assets/images/banners/banner3-1.png',
      '/assets/images/banners/banner3-2.png',
      '/assets/images/banners/banner3-3.png',
    ],
  },
];