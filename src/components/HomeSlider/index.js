import React from 'react';
import { Box, CircularProgress } from '@material-ui/core';
import Carousel from '../Carousel';
import { items } from "./data";

const HomeSlider = () => {
  const settings = {
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return items.length ? (
    <Box mt={-2}>
      <Carousel settings={settings} items={items}></Carousel>
    </Box>
  ) : (
    <Box align="center" p={20}>
      <CircularProgress color="primary" />
    </Box>
  );
};

export default HomeSlider;
