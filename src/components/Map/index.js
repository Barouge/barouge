import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import { load } from '@2gis/mapgl';

const useStyles = makeStyles((theme) => ({
  root: {
    height: 'calc(100vh - 140px)',
    margin: `-${theme.spacing(2)}px 0 -${theme.spacing(2)}px`,
  },
}));

const Map = () => {
  const classes = useStyles();

  useEffect(() => {
    let map;
    load().then((mapglAPI) => {
      // container — id of the div element in your html
      map = new mapglAPI.Map('map', {
        center: [55.31878, 25.23584],
        zoom: 13,
      });
    });

    // Destroy the map on unmounted
    return () => map && map.destroy();
  }, []);

  return <Box id="map" className={classes.root} />;
};

export default Map;
