import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import clsx from 'clsx';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {},
  wrapper: {
    marginBottom: '10px',
    '&:last-child': {
      marginBottom: '0',
    },
  },
  title: {
    fontSize: '22px',
    padding: '20px 0 5px',
    marginBottom: '25px',
    borderBottom: `solid 1px ${theme.color.black}`,
    color: theme.color.black,
  },
  children: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  link: {
    position: 'relative',
    display: 'block',
    padding: '10px 15px',
    marginBottom: '5px',
    '&:last-child': {
      marginBottom: '0',
    },
    color: theme.color.black,
    '&::before, &::after': {
      content: `''`,
      position: 'absolute',
      left: '0',
      top: '0',
      width: '2px',
      height: '100%',
    },
    '&::before': {
      backgroundColor: theme.palette.primary[100],
    },
    '&::after': {
      transition: 'ease .3s all',
      backgroundColor: theme.palette.primary[700],
      transform: 'scaleX(0)',
    },
    '&:hover, &._active': {
      '&::after': {
        transform: 'scaleX(1)',
      },
    },
  },
}));

const SideMenu = ({ menu, setCurPage, curPage }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      {menu.map((item, i) => {
        return (
          <Box key={i} className={classes.wrapper}>
            <Box className={classes.title}>{item.name}</Box>

            <Box className={classes.children}>
              {item.children.map((el, j) => {
                return (
                  <Link
                    key={j}
                    to={el.href}
                    onClick={() => setCurPage(el.page)}
                    className={clsx(
                      classes.link,
                      el.page === curPage && '_active',
                    )}
                  >
                    {el.name}
                  </Link>
                );
              })}
            </Box>
          </Box>
        );
      })}
    </Box>
  );
};

export default SideMenu;
