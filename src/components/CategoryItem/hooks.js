import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'block',
    height: '300px',
    width: 'calc(100% - 40px)',
    margin: '0 auto 20px',
    position: 'relative',
    padding: '15px',
    boxShadow: `0 1px 5px ${theme.color.greyLight}`,
    backgroundColor: theme.color.white,
    overflow: 'hidden',
    borderRadius: '10px',
    '&:hover': {
      '& .MuiBox-root': {
        transform: 'scale(1.2)',
      },
      '& .MuiTypography-h4': {
        opacity: '1',
      },
    },
  },
  title: {
    transition: 'ease .3s all',
    opacity: '0',
    fontSize: '22px',
    lineHeight: '60px',
    position: 'absolute',
    bottom: '0',
    left: '0',
    width: '100%',
    height: '60px',
    backgroundColor: 'rgba(0, 0, 0, .5)',
    textAlign: 'center',
    color: theme.color.white,
  },
  image: {
    transition: 'ease .3s all',
    position: 'absolute',
    top: '15px',
    left: '15px',
    right: '15px',
    bottom: '20px',
    backgroundSize: 'contain',
    backgroundPosition: '50% 50%',
    backgroundRepeat: 'no-repeat',
  },
}));