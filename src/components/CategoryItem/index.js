import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';

import { useStyles } from './hooks';

const CategoryItem = ({ item }) => {
  const classes = useStyles();

  return (
    <Box component={Link} to={item.href} className={classes.root}>
      <Box
        className={classes.image}
        style={{ backgroundImage: `url(${item.image})` }}
      />
      <Typography variant="h4" className={classes.title}>
        {item.title}
      </Typography>
    </Box>
  );
};

export default CategoryItem;
