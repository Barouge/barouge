import React from 'react';
import { Typography, Box } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import FilterIcon from '@material-ui/icons/Filter';

const useStyles = makeStyles({
  newsItem: {
    width: 'calc(33.33% - 10px)',
    marginBottom: '20px',
    height: '475px',
  },
  media: {
    height: '200px',
    width: '100%',
  },
  inner: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  empty: {
    height: '200px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
});

const NewsCard = ({ data }) => {
  const classes = useStyles();

  return (
    <Card component={Link} to={`/news/${data.id}`} className={classes.newsItem}>
      <CardActionArea className={classes.inner}>
        {data.image && data.image !== '' ? (
          <CardMedia
            className={classes.media}
            image={data.image}
            title={data.title}
          />
        ) : (
          <Box className={classes.empty}>
            <FilterIcon />
          </Box>
        )}
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {data.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {data.text}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default NewsCard;
