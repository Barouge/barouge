import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { Box } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: 'calc(25% - 20px)',
    marginRight: '20px',
    '&:last-child': {
      marginRight: '0',
    },
  },
  link: {
    width: '100%',
    textDecoration: 'none',
    fontSize: '14px',
    color: theme.color.white,
    textAlign: 'left',
  },
  title: {
    fontSize: '16px',
    color: theme.palette.primary.main,
    marginBottom: '20px',
    paddingBottom: '15px',
    borderBottom: `solid 1px ${theme.palette.primary.main}`,
  },
}));

const FooterMenuSection = ({ data }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Link to={data.href} className={`${classes.link} ${classes.title}`}>
        {data.title}
      </Link>
      {data.items.map((item, i) => {
        return (
          <Link key={i} to={item.href} className={classes.link}>
            {item.name}
          </Link>
        );
      })}
    </Box>
  );
};

export default FooterMenuSection;
