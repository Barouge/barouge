import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Container,
  Typography,
  Grid,
  Hidden,
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { fetchSections } from '../../redux/actions/appActions';
import clsx from 'clsx';
import { useTranslation } from 'react-i18next';

import FooterMenuSection from './FooterMenuSection';
import FooterMenuSectionLast from './FooterMenuSectionLast';
import Subscribe from '../Subscribe';
import PaymentMethods from '../PaymentMethods';

const useStyles = makeStyles((theme) => {
  const root = {
    transition: 'ease .3s all',
    position: 'fixed',
    zIndex: '100',
    bottom: '0',
    width: '100%',
    left: '50%',
    transform: 'translateX(-50%)',
    marginTop: 'auto',
    height: '620px',
    backgroundColor: theme.palette.grey[900],
    '&._subscribed': {
      height: '470px',
    },
    [theme.breakpoints.down('md')]: {
      position: 'static',
      transform: 'none',
      height: 'auto !important',
    },
  };
  return ({
    root: root,
    top: {
      padding: '40px 0',
      height: '430px',
      display: 'flex',
      alignItems: 'center',
      [theme.breakpoints.down('md')]: {
        height: 'auto',
      },
    },
    bottom: {
      height: '40px',
      borderTop: `solid 1px ${theme.palette.grey[500]}`,
    },
    wrapper: {
      height: '100%',
      display: 'flex',
      flexWrap: 'wrap',
      alignItems: 'flex-start',
      justifyContent: 'center',
    },
    copyright: {
      fontSize: '14px',
      color: theme.palette.grey[400],
      lineHeight: '40px',
    },
    subscribe: {
      transition: 'ease .3s all',
      height: '150px',
      backgroundColor: theme.palette.grey[200],
      '&._subscribed': {
        height: '0',
        overflow: 'hidden',
      },
      [theme.breakpoints.down('md')]: {
        height: 'auto',
        padding: '20px 0',
        '&._subscribed': {
          height: '0',
        },
      },
    },
  });
});

const Footer = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const menu = useSelector((state) => state.app.sections);
  const subscribe = useSelector((state) => state.app.subscribe);
  const [subscribed, setSubscribed] = useState(false);
  const { t } = useTranslation();

  useEffect(() => {
    if (menu) return;

    dispatch(fetchSections());
  }, []);

  useEffect(() => {
    if (!subscribe) return;

    setTimeout(() => {
      setSubscribed(subscribe.status);
    }, 3000);
  }, [subscribe]);

  return (
    <footer className={clsx(classes.root, subscribed && '_subscribed')}>
      <Box className={clsx(classes.subscribe, subscribed && '_subscribed')}>
        <Subscribe />
      </Box>
      <Box className={classes.top}>
        {menu && (
          <Container>
            <Grid container spacing={4}>
              {menu.map((item, i) => {
                return (
                  <Grid xs={12} lg={3} item key={i}>
                    <FooterMenuSection data={item} />
                  </Grid>
                );
              })}
              <Grid item xs={12} lg={3}>
                <FooterMenuSectionLast />
              </Grid>
            </Grid>
          </Container>
        )}
      </Box>
      <Box className={classes.bottom}>
        <Container component={Grid} container justify="space-between">
          <Grid item>
            <Typography className={classes.copyright} variant="body2">
              {t('copyright')}
            </Typography>
          </Grid>
          <Hidden mdDown>
            <Grid item>
              <PaymentMethods />
            </Grid>
          </Hidden>
        </Container>
      </Box>
    </footer>
  );
};

export default Footer;
