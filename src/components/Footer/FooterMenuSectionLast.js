import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Grid } from '@material-ui/core';
import clsx from 'clsx';

import VkIcon from '../../icons/VkIcon';
import OdIcon from '../../icons/OdIcon';
import FbIcon from '../../icons/FbIcon';
import InstIcon from '../../icons/InstIcon';
import PhoneIcon from '../../icons/PhoneIcon';
import EmailIcon from '../../icons/EmailIcon';

const useStyles = makeStyles((theme) => ({
  root: {
    borderRight: `solid 1px ${theme.palette.grey[400]}`,
    paddingRight: '40px',
    height: '100%',
  },
  socials: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  icon: {
    width: '42px',
    height: '42px',
    '& svg': {
      transition: 'ease .3s all',
      width: '100%',
      height: '100%',
      color: theme.palette.grey[400],
      '&:hover': {
        transform: 'scale(1.1)',
        color: theme.palette.primary[500],
      },
    },
  },
  link: {
    transition: 'ease .3s all',
    display: 'block',
    width: '100%',
    textDecoration: 'none',
    fontSize: '14px',
    color: theme.palette.grey[400],
    textAlign: 'left',
    padding: '5px 0',
    '&:not(._title)': {
      '&:hover': {
        color: '#fff',
      },
    },
    '&._title': {
      fontSize: '16px',
      color: theme.palette.primary.main,
      marginBottom: '20px',
      paddingBottom: '15px',
      borderBottom: `solid 1px ${theme.palette.primary.main}`,
    },
  },
  phone: {
    display: 'flex',
    alignItems: 'center',
    color: theme.palette.grey[400],
    fontSize: '22px',
    '& svg': {
      transition: 'ease .3s all',
      width: '42px',
      height: '42px',
      marginRight: '10px',
    },
    '&:hover': {
      '& svg': {
        transform: 'scale(1.1)',
      },
    },
  },
}));

const socials = [
  {
    title: 'Вконтакте',
    short: 'vk',
    link: 'http://vk.com/barouge',
    icon: <VkIcon />,
  },
  {
    title: 'Одноклассники',
    short: 'od',
    link: 'http://od.com/barouge',
    icon: <OdIcon />,
  },
  {
    title: 'Фейсбук',
    short: 'fb',
    link: 'http://fb.com/barouge',
    icon: <FbIcon />,
  },
  {
    title: 'Инстаграмм',
    short: 'inst',
    link: 'http://instagram.com/barouge',
    icon: <InstIcon />,
  },
];

const FooterMenuSectionLast = () => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <Box className={clsx(classes.link, '_title')}>Мы в социальных сетях</Box>
      {socials && (
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Box className={classes.socials}>
              {socials.map((item, i) => {
                return (
                  <a
                    key={i}
                    href={item.link}
                    target="_blank"
                    rel="noopener noreferrer"
                    className={classes.icon}
                  >
                    {item.icon}
                  </a>
                );
              })}
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box
              component="a"
              href="tel:+73835151515"
              className={classes.phone}
            >
              <PhoneIcon />
              +7 383 515 15 15
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box
              component="a"
              href="email:email@barouge.su"
              className={classes.phone}
            >
              <EmailIcon />
              email@barouge.su
            </Box>
          </Grid>
        </Grid>
      )}
    </Box>
  );
};

export default FooterMenuSectionLast;
