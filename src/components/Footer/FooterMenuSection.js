import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { Box } from '@material-ui/core';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  root: {
    borderRight: `solid 1px ${theme.palette.grey[400]}`,
    paddingRight: '40px',
    height: '100%',
    [theme.breakpoints.down('md')]: {
      paddingRight: '0',
      border: '0',
    },
  },
  icon: {
    width: '42px',
    height: '42px',
    '& svg': {
      transition: 'ease .3s all',
      width: '100%',
      height: '100%',
      color: theme.palette.grey[400],
      '&:hover': {
        transform: 'scale(1.1)',
        color: theme.palette.primary[500],
      },
    },
  },
  link: {
    transition: 'ease .3s all',
    display: 'block',
    width: '100%',
    textDecoration: 'none',
    fontSize: '14px',
    color: theme.palette.grey[400],
    textAlign: 'left',
    padding: '5px 0',
    '&:not(._title)': {
      '&:hover': {
        color: '#fff',
      },
    },
    '&._title': {
      fontSize: '16px',
      color: theme.palette.primary.main,
      marginBottom: '20px',
      paddingBottom: '15px',
      borderBottom: `solid 1px ${theme.palette.primary.main}`,
    },
  },
}));

const FooterMenuSection = ({ data }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.wrapper}>
        <Box className={clsx(classes.link, '_title')}>{data.name}</Box>
        {data.children.map((item, i) => {
          return (
            <Link key={i} to={item.href} className={classes.link}>
              {item.name}
            </Link>
          );
        })}
      </Box>
    </Box>
  );
};

export default FooterMenuSection;
