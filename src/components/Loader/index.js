import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, CircularProgress } from '@material-ui/core';
import clsx from 'clsx';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
}));

const Loader = (props) => {
  const classes = useStyles();

  return (
    <Box
      align="center"
      className={clsx(classes.root, 'loader', props.className)}
    >
      <CircularProgress />
    </Box>
  );
};

export default Loader;
