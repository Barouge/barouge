import React from 'react';
import HomeIcon from '@material-ui/icons/Home';
import { Box } from '@material-ui/core';
import { Link } from 'react-router-dom';
import clsx from 'clsx';

import { useStyles } from './hooks';

const Crumbs = ({ breadcrumbs }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Link to="/" className={clsx(classes.link, '_home')}>
        <HomeIcon className={classes.icon} />
        <span>Главная</span>
      </Link>

      {breadcrumbs &&
        breadcrumbs.length > 0 &&
        breadcrumbs.map((item, i) => {
          return item.href !== '' ? (
            <Link key={i} to={item.href} className={classes.link}>
              {item.name}
            </Link>
          ) : (
            <Box key={i} className={classes.text}>
              {item.name}
            </Box>
          );
        })}
    </Box>
  );
};

export default Crumbs;
