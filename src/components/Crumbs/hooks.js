import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  link: {
    position: 'relative',
    fontSize: '16px',
    fontWeight: '400',
    lineHeight: '22px',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    color: theme.palette.grey[700],
    paddingRight: theme.spacing(1),
    marginRight: theme.spacing(1),
    '&::before': {
      content: `''`,
      position: 'absolute',
      right: '0',
      top: '7px',
      height: '10px',
      width: '1px',
      backgroundColor: theme.palette.grey[700],
    },
    '&._home': {
      color: theme.palette.grey[700],
      fontWeight: '600',
    },
    '&:last-child': {
      paddingRight: theme.spacing(0),
      marginRight: theme.spacing(0),
      '&::before': {
        display: 'none',
      },
    },
    [theme.breakpoints.down('md')]: {
      whiteSpace: 'nowrap',
    },
  },
  text: {
    fontSize: '15px',
    lineHeight: '20px',
    color: theme.palette.primary[300],
    whiteSpace: 'nowrap',
    maxWidth: '100%',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  icon: {
    marginRight: '5px',
    color: theme.palette.grey[700],
  },
}));