import React, { useState, useEffect,useMemo } from 'react';
import {
  Box,
  Button,
  TextField,
  Typography,
} from '@material-ui/core';
import InputMask from 'react-input-mask';
import { Link } from 'react-router-dom';
import AuthEntity from './AuthEntity';
import { useDispatch, useSelector } from 'react-redux';
import {
  requestPhoneCode,
  checkPhoneCode,
} from '../../redux/actions/userActions';
import { showLoader,hideLoader } from '../../redux/actions/appActions';
import Countdown from 'react-countdown';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TabPanel from './TabPanel';
import { Grow } from '@material-ui/core';
import { useStyles } from './hooks';

const Auth = ({ handleClose }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [phone, setPhone] = useState('+7 (');
  const [access, setAccess] = useState(false);
  const [code, setCode] = useState('');
  const [codeAccess, setCodeAccess] = useState(false);
  const [valueTabs,setValueTabs] = useState(0);
  const user = useSelector((state) => state.user);
  const codeStatus = user.codeStatus;


  const countDigits = (n) => {
    for (var i = 0; n > 1; i++) {
      n /= 10;
    }

    return i;
  };

  const handleChange = (event) => {
    let numbers = parseInt(event.target.value.replace(/\D+/g, ''));
    let numLength = countDigits(numbers);

    if (numLength === 11) {
      setAccess(true);
    } else {
      setAccess(false);
    }

    setPhone(event.target.value);
  };

  const handleChangeCode = (event) => {
    let numbers = parseInt(event.target.value.replace(/\D+/g, ''));
    let numLength = countDigits(numbers);

    if (numLength === 4) {
      setCodeAccess(true);
      dispatch(showLoader());
      dispatch(checkPhoneCode(code));
      setTimeout(() => {
        dispatch(hideLoader());
      }, 500);
    } else {
      setCodeAccess(false);
    }

    setCode(event.target.value);
  };

  const handleCodeActivate = () => {
    dispatch(requestPhoneCode(phone));
  };

  const handleChangeTabs = (_, newValue) => {
    setValueTabs(newValue);
  };

  const restartPhoneCode = (api) => {
    //сначала делаем запрос на новый смс код
    handleCodeActivate();
    //очищаем предыдущий таймер
    api.stop();
    //запускаем заново
    api.start();
    //иначе таймер не начнется заново
  };

  const rendererCount = ({  seconds, completed, api }) => {
    if (completed) {
      return <div className={classes.refreshCode} onClick={() => restartPhoneCode(api)}>Получить код заново</div>
    } else {
      return <span>Запросить повторно код можно будет через {seconds}</span>;
    }
  };

  //таймер нужно закешировать, чтобы он не перерендеривался/не сбрасывался при каждом вводе в инпут
  //перерендерится только при изменении timerStatus
  const rendererTimerСount = useMemo(() => {
    return (
        <Countdown
          renderer={rendererCount}
          //date={Date.now() + user.timer * 1000}
          date={Date.now() + 59000}
          autoStart={true}
        />
    )
  },[user.timerStatus])

  useEffect(() => {
    if (codeStatus === null) return;

    if (codeStatus) {
      setCodeAccess(true);
      //если авторизация проходит успешно, то ребутаем страницу
      setTimeout(() => {
        window.location.reload();
      },1000)
    }

    setCodeAccess(false);
    setCode('');
  }, [codeStatus]);


  if(codeStatus) {
    return (
      <Box>
        <Typography variant="body2">
          Авторизация прошла успешно!
        </Typography>
      </Box>
    )
  }


  if (user.timerStatus) {
    return (
      <Box>
        <Typography variant="body2">
          Мы отправили вам проверочный код по номеру телефона, который вы
          указали.
        </Typography>

        <TextField
          value={code}
          type="tel"
          autoFocus={true}
          onChange={handleChangeCode}
          inputProps={{ maxLength: 4 }}
          disabled={codeAccess}
          variant='outlined'
          size='small'
          className={classes.phoneAccept}
        />


        {codeStatus === false && (
          <Typography className={classes.error} variant="body2">
            Вы неправильно ввели проверочный код
          </Typography>
        )}
        <Box>
          {rendererTimerСount}
        </Box>
      </Box>
    );
  }

  return (
    <Box className={classes.root}>
      <Tabs value={valueTabs} onChange={handleChangeTabs} className={classes.TabsRoot}>
          <Tab label="Физическое лицо" />
          <Tab label="Юридическое лицо" />
      </Tabs>
      <TabPanel value={valueTabs} index={0}>
        <Grow in timeout={200}>
          <Box>
          <Box className={classes.inputWrap}>
            <InputMask
              mask="+7 (999) 999-99-99"
              value={phone}
              onChange={handleChange}
              alwaysShowMask={true}
            >
              {(inputProps) => (
                <TextField {...inputProps} type="tel" autoFocus={true} />
              )}
            </InputMask>
          </Box>
          <Typography variant="body2" className={classes.text}>
            Нажимая кнопку «Далее», вы принимаете условия{' '}
            <Link
              to="/about/legal/politics"
              className={classes.link}
              onClick={handleClose}
            >
              Пользовательского соглашения
            </Link>
          </Typography>
          <Box align="right" className={classes.btnWrap}>
            <Button
              className={classes.button}
              disabled={access ? false : true}
              variant="outlined"
              color="primary"
              onClick={() => handleCodeActivate()}
            >
              Далее
            </Button>
          </Box>
        </Box>
        </Grow>
      </TabPanel>
      <TabPanel value={valueTabs} index={1}>
        <Grow in timeout={600}>
          <Box>
            <AuthEntity />
          </Box>
        </Grow>
      </TabPanel>
      <Box>
        <Box align="right">
          <Link
            to="/legal/partner"
            onClick={handleClose}
            className={classes.entity}
          >
            Стать партнером
          </Link>
        </Box>
      </Box>
    </Box>
  );
};

export default Auth;
