import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    width: '275px',
  },
  refreshCode: {
    textDecoration: 'underline',
    cursor:'pointer',
    color:  theme.palette.primary[500]
  },
  phoneAccept: {
    margin: '10px 0'
  },
  error: {
    color: theme.color.red,
  },
  inputWrap: {
    marginBottom: '20px',
    marginTop: 20
  },
  text: {
    marginBottom: '20px',
    fontSize: '14px',
    lineHeight: '1',
  },
  link: {
    position: 'relative',
    color: theme.palette.primary[500],
    lineHeight: '.9',
    '&::before': {
      content: `''`,
      transition: 'ease .3s all',
      position: 'absolute',
      left: '0',
      top: '100%',
      backgroundColor: theme.palette.primary[500],
      height: '1px',
      width: '0',
    },
    '&:hover': {
      '&::before': {
        width: '100%',
      },
    },
  },
  entity: {
    cursor: 'pointer',
    fontSize: '12px',
    position: 'relative',
    color: theme.color.black,
    '&::before': {
      content: `''`,
      transition: 'ease .3s all',
      position: 'absolute',
      left: '0',
      top: '100%',
      backgroundColor: theme.color.black,
      height: '1px',
      width: '0',
    },
    '&:hover': {
      '&::before': {
        width: '100%',
      },
    },
  },
  btnWrap: {
    //marginBottom: '20px',
  },
  TabsRoot: {
    marginTop: -10,
    '& .MuiTab-root': {
      textTransform: 'none',
      '@media (min-width: 480px)': {
        minWidth: 140
      }
    }
  },
}));