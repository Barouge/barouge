import React, { useEffect } from 'react';
import { Box, TextField, Button,Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import * as yup from 'yup';
import { fetchCheckAuthEntity } from '../../../redux/actions/userActions';

import { useStyles } from './hooks';

const AuthEntity = ({ handleClose }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const authEntity = useSelector(state => state.user.authEntity);

  useEffect(() => {
    if(authEntity && !authEntity.error) {
      setTimeout(() => {
        window.location.reload();
      },1000)
    }
  },[authEntity])

  const formik = useFormik({
    initialValues: {
      login: '',
      password: ''
    },
    validationSchema: yup.object({
      login: yup
        .string()
        .required('Введите логин'),
      password: yup
        .string()
        .required('Введите пароль')
    }),
    onSubmit: values => {
      dispatch(fetchCheckAuthEntity(values));
    },
  });

  if(authEntity && !authEntity.error) {
    return (
      <Box>
        <Typography variant="body2" className={classes.successTitle}>
          Авторизация прошла успешно!
        </Typography>
      </Box>
    )
  }

  return (
    <Box className={classes.root}>
       <form onSubmit={formik.handleSubmit} className={classes.form}>
          <div className={classes.wrapInput}>
            <TextField
              fullWidth
              id="login"
              name="login"
              label="Логин"
              value={formik.values.login}
              onChange={formik.handleChange}
              variant='outlined'
              size='small'
              error={formik.touched.login && Boolean(formik.errors.login)}
            />
            {formik.touched.login && formik.errors.login ? (
              <span className={'error-field'}>{formik.errors.login}</span>
            ) : null}
          </div>

          <div className={classes.wrapInput}>
            <TextField
              fullWidth
              id="password"
              name="password"
              label="Пароль"
              type='password'
              value={formik.values.password}
              onChange={formik.handleChange}
              variant='outlined'
              size='small'
              error={formik.touched.password && Boolean(formik.errors.password)}
            />
            {formik.touched.password && formik.errors.password ? (
              <span className={'error-field'}>{formik.errors.password}</span>
            ) : null}
          </div>

          {authEntity?.error ? (
            <div className={classes.formError}>
              {authEntity.error}
            </div>
          ) : null}

          <Button
            variant="contained"
            color="primary"
            type='submit'
          >
            Отправить
          </Button>
     </form>
    </Box>
  );
};

export default AuthEntity;
