import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(() => ({
  root: {},
  form: {
    marginTop: 30,
    '& .MuiFormControl-root': {
      width: '100%',
      '& .MuiFormLabel-root': {
        fontSize: 18
      }
    }
  },
  wrapInput: {
    width: '100%',
    position: 'relative',
    marginBottom: 20,
    '& .error-field': {
      position: 'absolute',
      bottom: -15,
      left: 0,
    },
  },
  formError: {
    color: 'tomato',
    fontWeight: 'bold',
    marginBottom: 20
  },
  successTitle: {
    margin: '30px 0',
    textAlign:'center'
  }
}));