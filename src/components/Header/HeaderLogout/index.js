import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Dialog,
  DialogTitle,
  IconButton,
  DialogContent,
  Tooltip,
} from '@material-ui/core';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import Auth from '../../Auth';
import { useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {},
  icon: {
    fill: theme.color.white,
  },
  dialogTitle: {
    '& h2': {
      fontWeight: 'bold',
      fontSize: 20
    }
  },
  dialogContent: {
    padding: '10px 24px 30px 24px'
  }
}));

const HeaderLogout = () => {
  const classes = useStyles();
  const user = useSelector((state) => state.user);
  const codeStatus = useSelector((state) => state.user.codeStatus);
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
  };

  return (
    <Box className={classes.root}>
      <Tooltip title="Войти">
        <IconButton aria-label="login" onClick={handleClickOpen}>
          <PersonOutlineIcon className={classes.icon} />
        </IconButton>
      </Tooltip>
      <Dialog
        onClose={handleClose}
        open={open}
        disableBackdropClick={!!user.timerStatus && !codeStatus}
        disableEscapeKeyDown={!!user.timerStatus && !codeStatus}
      >
        {user.timerStatus ? null : <DialogTitle className={classes.dialogTitle}>Вход в аккаунт</DialogTitle>}
        <DialogContent disabletypography="true" className={classes.dialogContent}>
          <Auth handleClose={handleClose} />
        </DialogContent>
      </Dialog>
    </Box>
  );
};

export default HeaderLogout;
