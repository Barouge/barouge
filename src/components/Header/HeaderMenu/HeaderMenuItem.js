import React, { useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import {
  Box,
  MenuList,
  MenuItem,
  ClickAwayListener,
  Paper,
  Popper,
} from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { fetchSections } from '../../../redux/actions/appActions';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  root: {
    marginRight: '10px',
  },
  item: {
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
  },
  title: {
    color: theme.palette.grey[700],
    fontSize: '12px',
    textTransform: 'uppercase',
    [theme.breakpoints.down('md')]: {
      fontSize: '10px',
      whiteSpace: 'nowrap',
    },
  },
  icon: {
    transition: 'ease .3s all',
    color: theme.palette.grey[700],
    '&._opened': {
      transform: 'rotate(180deg)',
    },
  },
  element: {
    fontSize: '14px',
  },
}));

const HeaderMenuItem = ({ item }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const menu = useSelector((state) => state.app.sections);
  const anchorRef = useRef(null);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (menu) return;

    dispatch(fetchSections());
  }, []);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  return (
    <Box className={classes.root}>
      <Box onClick={handleToggle} ref={anchorRef} className={classes.item}>
        <Box component="span" className={classes.title}>
          {item.name}
        </Box>
        <ArrowDropDownIcon className={clsx(classes.icon, open && '_opened')} />
      </Box>
      <Popper disablePortal={true} open={open} anchorEl={anchorRef.current}>
        <Paper>
          <ClickAwayListener onClickAway={handleClose}>
            <MenuList id="split-button-menu">
              {item.children.map((item, index) => {
                return (
                  <MenuItem
                    className={classes.element}
                    key={index}
                    component={Link}
                    to={item.href}
                    onClick={handleClose}
                  >
                    {item.name}
                  </MenuItem>
                );
              })}
            </MenuList>
          </ClickAwayListener>
        </Paper>
      </Popper>
    </Box>
  );
};

export default HeaderMenuItem;
