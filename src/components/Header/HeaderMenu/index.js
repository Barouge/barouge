import React, { useEffect } from 'react';
import {
  Box,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { fetchSections } from '../../../redux/actions/appActions';
import HeaderMenuItem from './HeaderMenuItem';

const useStyles = makeStyles(() => ({
  root: {
    height: '100%',
  },
  menu: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
}));

const HeaderMenu = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const menu = useSelector((state) => state.app.sections);

  useEffect(() => {
    if (menu) return;

    dispatch(fetchSections());
  }, []);

  return (
    <Box className={classes.root}>
      {menu && (
        <Box className={classes.menu}>
          {menu.map((item, i) => {
            return <HeaderMenuItem item={item} key={i} />;
          })}
        </Box>
      )}
    </Box>
  );
};

export default HeaderMenu;
