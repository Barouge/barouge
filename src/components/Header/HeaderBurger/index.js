import React, { useRef, useState, useEffect } from 'react';
import clsx from 'clsx';
import {
  Box,
  MenuList,
  ClickAwayListener,
  Paper,
  Popper,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { useDispatch, useSelector } from 'react-redux';
import {
  fetchCatalogMenu,
  catalogCurrentFiltersReset,
} from '../../../redux/actions/catalogActions';
import { Link } from 'react-router-dom';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';

import { useStyles } from './hooks'

const HeaderBurger = () => {
  const classes = useStyles();
  const anchorRef = useRef(null);
  const [open, setOpen] = useState(false);
  const [curMenu, setCurMenu] = useState(null);
  const dispatch = useDispatch();
  const catalog = useSelector((state) => state.catalog.menu);
  let timerId;

  useEffect(() => {
    if (catalog) return;

    dispatch(fetchCatalogMenu());
  }, []);

  const handleItemHover = (item) => {
    clearTimeout(timerId);

    setCurMenu(item);
  };

  const handleMouseEnter = () => {
    clearTimeout(timerId);

    setOpen(true);
  };

  const handleMouseLeave = () => {
    timerId = setTimeout(() => {
      setOpen(false);
      setCurMenu(null);
    }, 500);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  return (
    <Box className={classes.root}>
      <Box
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        ref={anchorRef}
        className={clsx(classes.button, open && '_open')}
      >
        <MenuIcon className={clsx(classes.icon, open && '_open')} />
        <Box component="span" className={clsx(classes.title, open && '_open')}>
          Каталог
        </Box>
      </Box>
      {catalog && (
        <Popper
          disablePortal={true}
          open={open}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
          placement="bottom-start"
          anchorEl={anchorRef.current}
          className={classes.wrapper}
        >
          <Paper className={classes.paper}>
            <ClickAwayListener onClickAway={handleClose}>
              <Box className={classes.wrap}>
                <MenuList className={classes.menu}>
                  {catalog.map((item, i) => {
                    return (
                      <Box
                        key={item.id}
                        component={Link}
                        onClick={(e) => {
                          handleClose(e);
                          dispatch(catalogCurrentFiltersReset());
                        }}
                        onMouseEnter={() => handleItemHover(item)}
                        to={item.href}
                        className={clsx(
                          classes.item,
                          curMenu && curMenu.id === item.id && '_hover',
                        )}
                      >
                        <Box className={classes.text}>{item.title}</Box>
                        {item.children.length > 0 && (
                          <ArrowRightIcon
                            className={clsx(
                              classes.arr,
                              curMenu && curMenu.id === item.id && '_hover',
                            )}
                          />
                        )}
                      </Box>
                    );
                  })}
                </MenuList>
                {curMenu && curMenu.children.length > 0 && (
                  <MenuList
                    onMouseEnter={() => handleItemHover(curMenu)}
                    className={clsx(classes.menu, '_submenu')}
                  >
                    {curMenu.children.map((item, i) => {
                      return (
                        <Box
                          key={item.id}
                          component={Link}
                          onClick={handleClose}
                          to={item.href}
                          onClick={(e) => {
                            handleClose(e);
                            dispatch(catalogCurrentFiltersReset());
                          }}
                          className={classes.item}
                        >
                          <Box className={classes.text}>{item.title}</Box>
                        </Box>
                      );
                    })}
                  </MenuList>
                )}

                {curMenu && curMenu.preview && (
                  <Box
                    onMouseEnter={() => handleItemHover(curMenu)}
                    className={classes.image}
                  >
                    <img src={curMenu.preview} alt="" />
                  </Box>
                )}
              </Box>
            </ClickAwayListener>
          </Paper>
        </Popper>
      )}
    </Box>
  );
};

export default HeaderBurger;
