import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    height: '100%',
  },
  icon: {
    transition: 'none',
    fill: theme.color.white,
    '&._open': {
      fill: theme.palette.grey[600],
    },
  },
  button: {
    position: 'relative',
    display: 'flex',
    height: '100%',
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: `0 ${theme.spacing(2)}px`,
    borderLeft: `solid 1px #fff`,
    borderRight: `solid 1px #fff`,
    marginLeft: `${theme.spacing(2)}px`,
    cursor: 'pointer',
    '&._open': {
      backgroundColor: '#fff',
      boxShadow: '0px 0 6px 2px rgb(0, 0, 0, .2)',
      '&::before': {
        content: `''`,
        position: 'absolute',
        zIndex: '1000',
        top: 'calc(100% - 2px)',
        left: '0',
        height: '4px',
        backgroundColor: '#fff',
        width: '100%',
      },
    },
  },
  title: {
    fontSize: '16px',
    textTransform: 'uppercase',
    color: '#fff',
    marginLeft: '5px',
    fontWeight: '700',
    '&._open': {
      color: theme.palette.grey[600],
    },
  },
  menu: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    padding: `${theme.spacing(1)}px 0`,
    width: '250px',
    minHeight: '300px',
    '&._submenu': {
      borderLeft: `solid 1px ${theme.palette.grey[300]}`,
      borderRight: `solid 1px ${theme.palette.grey[300]}`,
    },
  },
  item: {
    transition: 'ease .3s all',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '40px',
    padding: `0 ${theme.spacing(2)}px`,
    width: '100%',
    color: theme.color.black,
    '&:hover, &._hover': {
      backgroundColor: theme.palette.primary[50],
    },
  },
  text: {
    color: theme.color.black,
    fontSize: '15px',
  },
  paper: {
    borderRadius: '0 0 5px 5px',
  },
  wrap: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  image: {
    height: '100px',
    marginBottom: '20px',
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    minHeight: '300px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '& img': {
      width: '100%',
      maxWidth: '300px',
      maxHeight: '250px',
      objectFit: 'contain',
    },
  },
}));