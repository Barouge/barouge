import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { Link } from 'react-router-dom';
import { fetchBasket } from '../../../redux/actions/basketActions';
import { fetchUserInfo } from '../../../redux/actions/userActions';
import { Box, IconButton, Badge, Tooltip } from '@material-ui/core';
import HeaderLogin from '../HeaderLogin';
import HeaderLogout from '../HeaderLogout';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    fill: theme.color.white,
  },
  badge: {
    '& .MuiBadge-badge': {
      backgroundColor: theme.color.pink,
      color: theme.color.white,
    },
  },
  wrapper: {
    marginRight: '20px',
  },
}));

const HeaderNav = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const basket = useSelector((state) => state.basket);
  const user = useSelector((state) => state.user);

  useEffect(() => {
    if (basket.data) return;

    dispatch(fetchBasket());
  }, [basket]);

  useEffect(() => {
    if (user.isAuth !== null) return;

    dispatch(fetchUserInfo());
  }, [user]);

  return (
    <Box className={classes.root}>
      {user.isAuth ? <HeaderLogin /> : <HeaderLogout />}
      {basket.data && (
        <Tooltip title="Корзина">
          <IconButton component={Link} to="/basket" aria-label="cart">
            {basket.data.count > 0 ? (
              <Badge badgeContent={basket.data.count} className={classes.badge}>
                <ShoppingCartIcon className={classes.icon} />
              </Badge>
            ) : (
              <ShoppingCartIcon className={classes.icon} />
            )}
          </IconButton>
        </Tooltip>
      )}
    </Box>
  );
};

export default HeaderNav;
