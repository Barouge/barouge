import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import {
  Box,
  Grid,
  LinearProgress,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import HeaderTop from './HeaderTop';
import HeaderBottom from './HeaderBottom';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    zIndex: '1000',
    boxShadow: `0 0 4px rgba(0, 0, 0, .6)`,
  },
  loader: {
    position: 'absolute',
    top: 0,
    width: '100%',
    zIndex: '100',
  },
}));

const Header = () => {
  const classes = useStyles();
  const loading = useSelector((state) => state.app.loading);
  const { t } = useTranslation();

  return (
    <Box component="header" className={classes.root}>
      <Grid container>
        <Grid xs={12} item>
          <HeaderTop />
        </Grid>
        <Grid xs={12} item>
          <HeaderBottom />
        </Grid>
      </Grid>
      {loading && <LinearProgress className={classes.loader} />}
    </Box>
  );
};

export default Header;
