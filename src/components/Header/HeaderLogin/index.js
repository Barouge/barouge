import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, IconButton, Tooltip } from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {},
  icon: {
    fill: theme.color.white,
  },
}));

const HeaderLogin = () => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Tooltip title="Личный кабинет">
        <IconButton component={Link} to="/cabinet/personal" aria-label="login">
          <PersonIcon className={classes.icon} />
        </IconButton>
      </Tooltip>
    </Box>
  );
};

export default HeaderLogin;
