import React, { useState, useRef } from 'react';
import {
  Box,
  MenuList,
  MenuItem,
  ClickAwayListener,
  Paper,
  Popper,
  Button,
} from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { changeLocale, setLocaleFlag } from '../../../redux/actions/appActions';

import FlagRus from '../../../assets/icons/flags/flag-russia-ico.svg';
import FlagEng from '../../../assets/icons/flags/flag-england-ico.svg';
import FlagBel from '../../../assets/icons/flags/flag-belarus-ico.svg';
import FlagKaz from '../../../assets/icons/flags/flag-kaz-ico.svg';

import { useStyles } from './hooks'

const HeaderLanguage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { locale, languages, flag } = useSelector(
    (state) => state.app,
  );
  const { i18n } = useTranslation();
  const [open, setOpen] = useState(false);
  const anchorRef = useRef(null);
  let Flag = FlagRus;

  const handleMenuItemClick = (locale) => {
    i18n.changeLanguage(locale);
    dispatch(changeLocale(locale));
    dispatch(setLocaleFlag(locale));
    setOpen(false);
  };

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  return (
    <Box className={classes.root}>
      <Button
        color="primary"
        size="small"
        onClick={handleToggle}
        ref={anchorRef}
      >
        <img className={classes.flag} src={flag} alt="" />
        <ArrowDropDownIcon className={classes.icon} />
      </Button>
      <Popper disablePortal={true} open={open} anchorEl={anchorRef.current}>
        <Paper>
          <ClickAwayListener onClickAway={handleClose}>
            <MenuList id="split-button-menu">
              {languages.map((item, index) => {
                switch (item) {
                  case 'ru':
                    Flag = FlagRus;
                    break;
                  case 'en':
                    Flag = FlagEng;
                    break;
                  case 'bel':
                    Flag = FlagBel;
                    break;
                  case 'kz':
                    Flag = FlagKaz;
                    break;

                  default:
                    Flag = FlagRus;
                }
                return (
                  <MenuItem
                    key={index}
                    selected={locale === item}
                    onClick={() => handleMenuItemClick(item)}
                  >
                    <img className={classes.flag} src={Flag} alt="" />
                  </MenuItem>
                );
              })}
            </MenuList>
          </ClickAwayListener>
        </Paper>
      </Popper>
    </Box>
  );
};

export default HeaderLanguage;
