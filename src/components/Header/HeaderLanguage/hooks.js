import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {},
  flag: {
    display: 'block',
    width: '100%',
    maxWidth: '25px',
  },
  selectItem: {
    border: 'none',
  },
  icon: {
    color: theme.palette.grey[400],
  },
}));