import React from 'react';
import { Link } from 'react-router-dom';
import { Box, Container, Grid, Hidden } from '@material-ui/core';
import logo from '../../../assets/images/logo-simple.png';
import logoMob from '../../../assets/images/logo-mob.png';
import Search from '../../Search';
import HeaderNav from '../HeaderNav';
import HeaderBurger from '../HeaderBurger';

import { useStyles } from './hooks';

const HeaderBottom = () => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Container className={classes.container}>
        <Grid container alignItems="center" className={classes.wrapper}>
          <Grid item xs={8} lg={4} className={classes.logo}>
            <Link to="/" className={classes.link}>
              <Hidden mdDown>
                <img src={logo} alt="" />
              </Hidden>
              <Hidden mdUp>
                <img src={logoMob} alt="" />
              </Hidden>
            </Link>
            <HeaderBurger />
          </Grid>

          <Hidden mdDown>
            <Grid item lg={6}>
              <Search />
            </Grid>
          </Hidden>

          <Grid item xs={4} lg={2} className={classes.nav}>
            <HeaderNav />
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default HeaderBottom;
