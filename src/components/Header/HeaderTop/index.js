import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  Box,
  Container,
  Typography,
  Grid,
  Tooltip,
  Hidden,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import HeaderLanguage from '../HeaderLanguage';
import HeaderMenu from '../HeaderMenu';

import PhoneIcon from '../../../icons/PhoneIcon';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    zIndex: '100',
    backgroundColor: theme.color.white,
    height: theme.header.row.height,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  nav: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  phone: {
    display: 'block',
    width: '26px',
    height: '26px',
    marginRight: '20px',
    '& svg': {
      transition: 'ease .3s all',
      color: theme.palette.grey[500],
      width: '100%',
      height: '100%',
      '&:hover': {
        transform: 'scale(1.1)',
      },
    },
  },
}));

const HeaderTop = () => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Box className={classes.root}>
      <Container>
        <Grid alignItems="center" container>
          <Grid item xs={10} lg={5}>
            <HeaderMenu />
          </Grid>
          <Grid item xs={2} lg={7}>
            <Grid container alignItems="center">
              <Hidden mdDown>
                <Grid item xs={6}>
                  <Typography align="center" variant="body2">
                    {t('Официальный сайт продукции бренда BAROUGE')}
                  </Typography>
                </Grid>
              </Hidden>
              <Grid item xs={12} lg={6} className={classes.nav}>
                <Tooltip title="+7 (383) 515-15-15">
                  <Box
                    component="a"
                    href="tel:+73835151515"
                    className={classes.phone}
                  >
                    <PhoneIcon />
                  </Box>
                </Tooltip>
                <Hidden mdDown>
                  <HeaderLanguage />
                </Hidden>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default HeaderTop;
