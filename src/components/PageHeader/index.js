import React from 'react';
import { Box, Container, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Crumbs from '../../components/Crumbs';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.grey[50],
    boxShadow: `0 0 5px 1px ${theme.palette.grey[400]}`,
    padding: `${theme.spacing(1)}px 0`,
    marginBottom: theme.spacing(4),
  },
  title: {
    position: 'relative',
    display: 'inline-block',
    padding: '0 3px',
    fontSize: '36px',
    fontStyle: 'italic',
    letterSpacing: '2px',
    fontWeight: '800',
    textTransform: 'uppercase',
    '&::before': {
      content: `''`,
      position: 'absolute',
      left: '0',
      bottom: '3px',
      width: '100%',
      height: '12px',
      backgroundColor: theme.palette.primary[500],
      opacity: '.3',
    },
    [theme.breakpoints.down('md')]: {
      fontSize: '22px',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      maxWidth: '100%',
      textTransform: 'initial',
    },
  },
  inner: {
    height: '100px',
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'space-around',
  },
  row: {
    width: '100%',
  },
}));

const PageHeader = ({ breadcrumbs, title }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Container>
        <Box className={classes.inner}>
          <Box className={classes.row}>
            <Crumbs breadcrumbs={breadcrumbs} />
          </Box>
          <Box className={classes.row}>
            <Typography variant="h1" className={classes.title}>
              {title}
            </Typography>
          </Box>
        </Box>
      </Container>
    </Box>
  );
};

export default PageHeader;
