import React, { useEffect, useRef, useState } from 'react';
import { Box, Container, Typography } from '@material-ui/core';
import clsx from 'clsx';
import { Link } from 'react-router-dom';

import { useStyles } from './hooks';

const ActionsItem = ({ item, home }) => {
  const classes = useStyles();
  const banner = useRef(null);
  const [active, setActive] = useState(false);
  const type = item.type;

  const handleUserScroll = () => {
    if (window.pageYOffset + 300 > banner.current.offsetTop) {
      setActive(true);
    }
  };

  useEffect(() => {
    if (active) {
      window.removeEventListener('scroll', handleUserScroll);
    } else {
      window.addEventListener('scroll', handleUserScroll);
    }

    handleUserScroll();

    return () => {
      window.removeEventListener('scroll', handleUserScroll);
    };
  }, [active]);

  return (
    <Box
      className={clsx(
        classes.root,
        active ? '_active' : '',
        type,
        home && '_home',
      )}
      style={{ backgroundColor: item.bgColor }}
      ref={banner}
    >
      <Container className={classes.container}>
        <Box className={classes.wrapper}>
          {item.images && (
            <Box className={classes.images}>
              {item.images.map((image, i) => {
                if (window.innerWidth < 1280 && i > 0) return;
                if (home && type !== 'ACTIONS_TYPE_3' && i > 0) return null;

                return (
                  <Box
                    key={i}
                    className={clsx(classes.image, `${type}-${i}`, '_image')}
                    style={{ backgroundImage: `url(${image})` }}
                  />
                );
              })}
            </Box>
          )}

          <Box className={clsx(classes.content, type, '_content')}>
            <Typography variant="h3" className={clsx(classes.title, '_title')}>
              {item.subName && (
                <Typography
                  className={clsx(classes.subTitle, '_subTitle')}
                  variant="caption"
                >
                  {item.subName}
                </Typography>
              )}
              {item.name}
            </Typography>
            <Typography
              className={clsx(classes.text, type, '_text')}
              variant="body1"
            >
              {item.text}
            </Typography>
            <Box
              className={clsx(classes.link, type, '_link')}
              component={Link}
              to={item.url}
            >
              {item.buttonTxt.length ? item.buttonTxt : 'Подробнее'}
            </Box>
          </Box>
        </Box>
      </Container>
    </Box>
  );
};

export default ActionsItem;
