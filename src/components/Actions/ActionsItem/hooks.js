import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    height: '500px',
    width: '100%',
    '&.ACTIONS_TYPE_3': {
      overflow: 'hidden',
    },
    '&.ACTIONS_TYPE_4, &.ACTIONS_TYPE_5': {
      width: '50%',
      [theme.breakpoints.down('md')]: {
        width: '100%',
      },
    },
    '&._home': {
      height: '300px',
      marginBottom: '30px',
      '& ._image': {
        backgroundSize: 'contain',
      },
      '&.ACTIONS_TYPE_1': {
        '& ._image': {
          bottom: '0',
          left: '20px',
          height: '330px',
        },
      },
      '&.ACTIONS_TYPE_2': {
        height: '630px',
        '& ._content': {
          top: '40px',
          left: '20px',
          transform: 'none',
        },
        '& ._image': {
          bottom: '0',
          left: 'auto',
          right: '0',
          height: '320px',
        },
      },
      '&.ACTIONS_TYPE_3': {
        '& ._content': {
          overflow: 'visible',
          top: '15px',
        },
        '& ._image': {
          bottom: '0',
          transform: 'none',
          '&.ACTIONS_TYPE_3-0': {
            left: '-40px',
            height: '160px',
          },
          '&.ACTIONS_TYPE_3-1': {
            left: '50%',
            transform: 'translateX(-50%)',
            height: '100px',
          },
          '&.ACTIONS_TYPE_3-2': {
            left: 'auto',
            right: '-40px',
            height: '160px',
          },
        },
      },
    },
    [theme.breakpoints.down('md')]: {
      height: '250px !important',
      '& ._image': {
        position: 'static !important',
        height: '150px !important',
        transform: 'none !important',
      },
      '& ._content': {
        position: 'static !important',
      },
      '& ._title': {
        fontSize: '14px !important',
      },
    },
  },
  container: {
    height: '100%',
  },
  wrapper: {
    position: 'relative',
    height: '100%',
  },
  content: {
    position: 'absolute',
    '&.ACTIONS_TYPE_1': {
      left: '310px',
      maxWidth: '260px',
      top: '50%',
      transform: 'translateY(-50%)',
    },
    '&.ACTIONS_TYPE_2': {
      maxWidth: '360px',
      paddingRight: '40px',
      right: '0',
      top: '50%',
      transform: 'translateY(-50%)',
    },
    '&.ACTIONS_TYPE_3': {
      color: '#fff',
      left: '50%',
      overflow: 'hidden',
      textAlign: 'center',
      top: '55px',
      transform: 'translateX(-50%)',
    },
    '&.ACTIONS_TYPE_4, &.ACTIONS_TYPE_5': {
      left: '40px',
      maxWidth: '260px',
      top: '70px',
    },
  },
  title: {
    fontSize: '60px',
    letterSpacing: '1.5px',
    lineHeight: '1',
    marginBottom: '20px',
    textTransform: 'uppercase',
    fontWeight: '700',
  },
  subTitle: {
    display: 'block',
    position: 'relative',
    top: '5px',
    fontSize: '22px',
    letterSpacing: '5px',
    marginBottom: '5px',
    fontWeight: '400',
  },
  text: {
    fontSize: '16px',
    fontWeight: '400',
    marginBottom: '20px',
    '&.ACTIONS_TYPE_3': {
      display: 'inline-block',
      marginRight: '20px',
    },
  },
  image: {
    position: 'absolute',
    backgroundColor: 'transparent',
    backgroundPosition: '50% 50%',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    '&.ACTIONS_TYPE_1-0': {
      height: '416px',
      left: '75px',
      top: '50%',
      transform: 'translate(0, -50%)',
      width: '182px',
    },
    '&.ACTIONS_TYPE_1-1': {
      height: '500px',
      right: '50px',
      top: '0',
      width: '485px',
    },
    '&.ACTIONS_TYPE_2-0': {
      bottom: '0',
      height: '460px',
      left: '85px',
      right: '0',
      width: '387px',
    },
    '&.ACTIONS_TYPE_2-1': {
      height: '375px',
      left: '50%',
      top: '50%',
      transform: 'translate(-10px, -50%)',
      width: '192px',
    },
    '&.ACTIONS_TYPE_3-0, &.ACTIONS_TYPE_3-1, &.ACTIONS_TYPE_3-2': {
      bottom: '0',
      height: '360px',
      transform: 'translateY(50px)',
      width: '215px',
    },
    '&.ACTIONS_TYPE_3-0': {
      left: '20px',
    },
    '&.ACTIONS_TYPE_3-1': {
      left: '50%',
      transform: 'translate(-50%, 50px)',
    },
    '&.ACTIONS_TYPE_3-2': {
      right: '20px',
    },
    '&.ACTIONS_TYPE_4-0': {
      bottom: '35px',
      height: '452px',
      right: '30px',
      width: '220px',
    },
    '&.ACTIONS_TYPE_5-0': {
      bottom: 0,
      height: '484px',
      right: '0',
      width: '291px',
    },
  },
  link: {
    transition: 'ease .3s all',
    position: 'relative',
    display: 'inline-block',
    fontSize: '18px',
    fontWeight: '700',
    color: theme.palette.grey[700],
    border: '0',
    '&::before, &::after': {
      content: `''`,
      position: 'absolute',
      top: '100%',
      left: '0',
      height: '1px',
    },
    '&::before': {
      width: '100%',
      backgroundColor: theme.palette.grey[400],
    },
    '&::after': {
      transition: 'ease .3s all',
      width: '0',
      backgroundColor: theme.palette.primary[400],
    },
    '&:hover': {
      color: theme.palette.grey[900],
      boxShadow: 'none',
      '&::after': {
        width: '100%',
      },
    },
    '&.ACTIONS_TYPE_3': {
      color: '#fff',
    },
  },
}));