import React from 'react';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import ActionsItem from './ActionsItem';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin: `-${theme.spacing(2)}px 0 -${theme.spacing(2)}px 0`,
  },
}));

const Actions = ({ actions }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      {actions &&
        actions.map((item, i) => {
          return <ActionsItem item={item} key={i} />;
        })}
    </Box>
  );
};

export default Actions;
