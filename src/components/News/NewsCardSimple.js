import React from 'react';
import { Typography, Box, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { Link } from 'react-router-dom';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

const useStyles = makeStyles((theme) => ({
  root: {
    transition: 'ease .3s all',
    display: 'block',
    width: 'calc(100% - 20px)',
    height: '350px',
    border: `solid 1px ${theme.palette.primary[50]}`,
    margin: '0 auto',
    color: '#000',
    '&:hover': {
      '& .news-btn': {
        backgroundColor: theme.palette.primary[50],
        opacity: '1',
      },
      '& ._title': {
        '&::after': {
          width: '100%',
        },
      },
    },
  },
  inner: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    borderRadius: '5px 5px 0 0',
  },
  images: {
    height: '150px',
    width: '100%',
    borderRadius: '5px 5px 0 0',
  },
  image: {
    objectFit: 'cover!important',
  },
  content: {
    position: 'relative',
    padding: '10px',
    width: '100%',
    height: '200px',
    paddingBottom: '48px',
    paddingRight: '48px',
    display: 'block',
    marginTop: 'auto',
  },
  nav: {
    position: 'absolute',
    left: '10px',
    bottom: '10px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 'calc(100% - 20px)',
  },
  iconBtn: {
    transition: 'ease .3s all',
    opacity: '0',
    marginLeft: 'auto',
  },
  title: {
    display: 'inline-block',
    position: 'relative',
    color: theme.color.black,
    marginBottom: '15px',
    fontWeight: '700',
    '&::before, &::after': {
      content: `''`,
      position: 'absolute',
      top: '100%',
      left: '0',
      height: '1px',
    },
    '&::before': {
      width: '100%',
      backgroundColor: theme.palette.grey[100],
    },
    '&::after': {
      transition: 'ease .3s all',
      width: '0',
      backgroundColor: theme.palette.grey[700],
    },
  },
  text: {
    fontSize: '12px',
    color: theme.color.black,
  },
  date: {
    fontSize: '14px',
    fontWeight: '700',
  },
}));

const NewsCardSimple = ({ news }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root} component={Link} to={`/news/${news.id}`}>
      <Box className={classes.inner}>
        <Box className={classes.images}>
          <img className={classes.image} src={news.images[0]} alt="" />
        </Box>

        <Box className={classes.content}>
          <Typography
            className={clsx(classes.title, '_title')}
            gutterBottom
            variant="h5"
            component="h2"
          >
            {news.title}
          </Typography>
          <Typography
            className={clsx(classes.text, '_text')}
            gutterBottom
            variant="body2"
            component="p"
          >
            {news.text}
          </Typography>
          <Box className={classes.nav}>
            {news.date && <Box className={classes.date}>{news.date}</Box>}
            <IconButton
              size="medium"
              className={clsx(classes.iconBtn, 'news-btn')}
            >
              <ArrowForwardIcon color="primary" />
            </IconButton>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default NewsCardSimple;
