import React from 'react';
import { Typography, Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Loader from '../Loader';
import NewsSlider from './NewsSlider';

const useStyles = makeStyles({
  root: {
    width: '100%',
    minHeight: '100%',
  },
  images: {
    height: '415px',
  },
  inner: {
    position: 'relative',
    transition: 'ease .3s all',
    '& .loader': {
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
    },
    '&.loading': {
      opacity: '0.8',
    },
  },
});

const NewsCardFull = ({ news, loading }) => {
  const classes = useStyles();
  const settings = {
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <Box className={clsx(classes.root, 'news_full_show')}>
      <Box className={clsx(classes.inner, loading && 'loading')}>
        {loading && <Loader />}
        <Box className={classes.images}>
          <NewsSlider settings={settings} images={news.images} />
        </Box>
        <Typography variant="body1">{news.text}</Typography>
      </Box>
    </Box>
  );
};

export default NewsCardFull;
