import React from 'react';
import { Typography, Box, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

import NewsSlider from './NewsSlider';

const useStyles = makeStyles((theme) => ({
  root: {
    transition: 'ease .3s all',
    display: 'block',
    width: '100%',
    height: '350px',
    borderRadius: '5px',
    boxShadow: `0 0 2px ${theme.palette.primary[200]}`,
  },
  inner: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    borderRadius: '5px 5px 0 0',
  },
  images: {
    height: '150px',
    width: '100%',
    borderRadius: '5px 5px 0 0',
  },
  content: {
    position: 'relative',
    padding: '10px',
    width: '100%',
    height: '200px',
    paddingBottom: '48px',
    paddingRight: '48px',
    display: 'block',
    marginTop: 'auto',
    '&:hover': {
      '& .news-btn': {
        backgroundColor: theme.palette.primary[50],
        opacity: '1',
      },
    },
  },
  iconBtn: {
    position: 'absolute',
    right: '5px',
    bottom: '5px',
    transition: 'ease .3s all',
    opacity: '0',
  },
  title: {
    color: theme.color.black,
    marginBottom: '15px',
  },
  text: {
    fontSize: '12px',
    color: theme.color.black,
  },
}));

const NewsCard = ({ data, setCurrentNews }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const settings = {
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <Box className={clsx(classes.root, 'lazy-show')}>
      <Box className={classes.inner}>
        <Box className={classes.images}>
          <NewsSlider settings={settings} images={data.images} />
        </Box>

        <Box
          className={classes.content}
          component={Link}
          to={`/news/${data.id}`}
          onClick={() => {
            dispatch(setCurrentNews(data.id));
          }}
        >
          <Typography
            className={classes.title}
            gutterBottom
            variant="h5"
            component="h2"
          >
            {data.title}
          </Typography>
          <Typography
            className={classes.text}
            gutterBottom
            variant="body2"
            component="p"
          >
            {data.text}
          </Typography>
          <IconButton
            size="medium"
            className={clsx(classes.iconBtn, 'news-btn')}
          >
            <ArrowForwardIcon color="primary" />
          </IconButton>
        </Box>
      </Box>
    </Box>
  );
};

export default NewsCard;
