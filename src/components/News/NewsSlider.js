import React from 'react';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MonochromePhotosIcon from '@material-ui/icons/MonochromePhotos';
import Carousel from '../Carousel';

const useStyles = makeStyles({
  root: {
    width: '100%',
    height: '100%',
    borderRadius: '5px 5px 0 0',
    '& img': {
      borderRadius: '5px 5px 0 0',
    },
  },
  noImageIco: {
    width: '100px',
    height: '100px',
  },
  empty: {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  image: {
    display: 'block',
    width: '100%',
    maxHeight: '100%',
    objectFit: 'cover',
    borderRadius: '5px 5px 0 0',
  },
});

const NewsSlider = ({ images, settings }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      {images.length === 0 ? (
        <Box className={classes.empty}>
          <MonochromePhotosIcon
            className={classes.noImageIco}
            color="primary"
          />
        </Box>
      ) : images.length > 1 ? (
        <Carousel settings={settings} items={images} />
      ) : (
        <img className={classes.image} src={images[0]} alt="" />
      )}
    </Box>
  );
};

export default NewsSlider;
