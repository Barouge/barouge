import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

const useStyles = makeStyles(() => ({
  arrow: {
    position: 'absolute',
    top: '50%',
    transform: 'translateY(-50%)',
    cursor: 'pointer',
    zIndex: '1010',
    width: '60px',
    height: '60px',
    transition: 'ease .3s all',
    '&:hover': {
      transform: 'scale(.9) translateY(-50%)',
    },
  },
  prev: {
    left: '50px',
  },
}));

const ArrowPrev = ({ className, onClick }) => {
  const classes = useStyles();

  return (
    <ArrowBackIosIcon
      className={`${className} ${classes.arrow} ${classes.prev}`}
      onClick={onClick}
      color="primary"
    />
  );
};

export default ArrowPrev;
