import React from 'react';
import { Box, Container, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import clsx from 'clsx';

const useStyles = makeStyles(() => ({
  root: {
    position: 'relative',
    height: '500px',
  },
  image: {
    position: 'absolute',
    display: 'block',
    background: 'transparent 50% 50% no-repeat',
    backgroundSize: 'cover',
    '&._image_0': {
      bottom: '0',
      height: '460px',
      left: '85px',
      right: '0',
      width: '387px',
    },
    '&._image_1': {
      height: '375px',
      left: '50%',
      top: '50%',
      transform: 'translate(-10px, -50%)',
      width: '192px',
    },
  },
  panel: {
    position: 'absolute',
    maxWidth: '360px',
    paddingTight: '40px',
    right: '0',
    top: '50%',
    transform: 'translateY(-50%)',
  },
  title: {
    fontSize: '50px',
    textTransform: 'uppercase',
    fontWeight: 'bold',
    lineHeight: '1.2',
    marginBottom: '20px',
  },
  subTitle: {
    fontSize: '22px',
    textTransform: 'uppercase',
    lineHeight: '1',
  },
  text: {
    marginBottom: '20px',
  },
}));

const Variant2 = ({ item: { images, title, subTitle, link, text } }) => {
  const classes = useStyles();

  return (
    <Box>
      <Container className={classes.root}>
        {images && (
          <Box className={classes.images}>
            {images.map((item, i) => {

              return (
                <Box
                  key={i}
                  className={clsx(classes.image, `_image_${i}`)}
                  style={{ backgroundImage: `url(${item})` }}
                />
              );
            })}
          </Box>
        )}
        <Box className={clsx(classes.panel, `_animate_banner_1_text`)}>
          {title && (
            <Box className={classes.title}>
              {subTitle && (
                <Typography variant="body2" className={classes.subTitle}>
                  {subTitle}
                </Typography>
              )}
              {title}
            </Box>
          )}

          {text && (
            <Typography variant="body2" className={classes.text}>
              {text}
            </Typography>
          )}

          {link && (
            <Box>
              <Button
                variant="outlined"
                color="primary"
                component={Link}
                to={link.href}
              >
                {link.name}
              </Button>
            </Box>
          )}
        </Box>
      </Container>
    </Box>
  );
};

export default Variant2;
