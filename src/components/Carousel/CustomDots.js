import React from 'react';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'absolute',
    listStyle: 'none',
    padding: '0',
    margin: '0',
    left: '50%',
    transform: 'translateX(-50%)',
    bottom: '40px',
    display: 'flex',
    alignItems: 'center',
    '& .slick-active .MuiButton-root': {
      backgroundColor: theme.palette.primary[50],
    },
  },
}));

const CustomDots = ({ dots }) => {
  const classes = useStyles();

  return (
    <Box component="ul" className={clsx(classes.root, 'slider-dots')}>
      {dots}
    </Box>
  );
};

export default CustomDots;
