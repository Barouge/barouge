import React from 'react';
import Slider from 'react-slick';
import Variant1 from './Variant1.js';
import Variant2 from './Variant2.js';
import Variant3 from './Variant3.js';
import ArrowNext from './ArrowNext.js';
import ArrowPrev from './ArrowPrev.js';
import CustomDots from './CustomDots.js';
import CustomDotsItem from './CustomDotsItem.js';

const Carousel = ({ settings, items }) => {
  if (!items || items.length < 1) return null;

  settings.nextArrow = <ArrowNext />;
  settings.prevArrow = <ArrowPrev />;
  settings.appendDots = (dots) => <CustomDots dots={dots} />;
  settings.customPaging = (i) => <CustomDotsItem index={i} />;

  return (
    <Slider {...settings}>
      {items.map((item, i) => {
        switch (item.type) {
          case 'variant1':
            return <Variant1 item={item} key={i} />;

            break;
          case 'variant2':
            return <Variant2 item={item} key={i} />;

            break;
          case 'variant3':
            return <Variant3 item={item} key={i} />;

            break;
          default:
            return (
              <img
                src={typeof item === 'string' ? item : item.image}
                alt=""
                key={i}
              />
            );
        }
      })}
    </Slider>
  );
};

export default Carousel;
