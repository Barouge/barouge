import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

const useStyles = makeStyles(() => ({
  arrow: {
    position: 'absolute',
    top: '50%',
    transform: 'translateY(-50%)',
    cursor: 'pointer',
    width: '60px',
    height: '60px',
    transition: 'ease .3s all',
    '&:hover': {
      transform: 'scale(.9) translateY(-50%)',
    },
  },
  next: {
    right: '50px',
  },
}));

const ArrowNext = ({ className, style, onClick }) => {
  const classes = useStyles();

  return (
    <ArrowForwardIosIcon
      style={style}
      className={`${className} ${classes.arrow} ${classes.next}`}
      onClick={onClick}
      color="primary"
    />
  );
};

export default ArrowNext;
