import React from 'react';
import { Box, Container, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import clsx from 'clsx';

const useStyles = makeStyles(() => ({
  root: {
    position: 'relative',
    height: '500px',
    backgroundColor: '#000',
  },
  image: {
    position: 'absolute',
    display: 'block',
    background: 'transparent 50% 50% no-repeat',
    backgroundSize: 'cover',
    height: '360px',
    width: '215px',
    bottom: '0',
    transform: 'translateY(50px)',
    '&._image_0': {
      left: '20px',
    },
    '&._image_1': {
      left: '50%',
      transform: 'translate(-50%, 50px)',
    },
    '&._image_2': {
      right: '20px',
    },
  },
  panel: {
    position: 'absolute',
    color: '#fff',
    left: '50%',
    overflow: 'hidden',
    textAlign: 'center',
    top: '55px',
    transform: 'translateX(-50%)',
  },
  title: {
    fontSize: '50px',
    textTransform: 'uppercase',
    fontWeight: 'bold',
    lineHeight: '1.2',
    marginBottom: '20px',
  },
  subTitle: {
    fontSize: '22px',
    textTransform: 'uppercase',
    lineHeight: '1',
  },
  text: {
    marginRight: '10px',
  },
  link: {
    color: '#fff',
    borderColor: '#fff',
  },
  wrapper: {
    display: 'flex',
    alignItems: 'center',
  },
}));

const Variant3 = ({ item: { images, title, subTitle, link, text } }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Container>
        {images && (
          <Box className={classes.images}>
            {images.map((item, i) => {

              return (
                <Box
                  key={i}
                  className={clsx(classes.image, `_image_${i}`)}
                  style={{ backgroundImage: `url(${item})` }}
                />
              );
            })}
          </Box>
        )}
        <Box className={classes.panel}>
          {title && (
            <Box className={classes.title}>
              {subTitle && (
                <Typography variant="body2" className={classes.subTitle}>
                  {subTitle}
                </Typography>
              )}
              {title}
            </Box>
          )}

          {(text || link) && (
            <Box className={classes.wrapper}>
              {text && (
                <Typography variant="body2" className={classes.text}>
                  {text}
                </Typography>
              )}

              {link && (
                <Button
                  variant="outlined"
                  color="primary"
                  component={Link}
                  to={link.href}
                  className={classes.link}
                >
                  {link.name}
                </Button>
              )}
            </Box>
          )}
        </Box>
      </Container>
    </Box>
  );
};

export default Variant3;
