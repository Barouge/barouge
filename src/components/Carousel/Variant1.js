import React, { useEffect, useState } from 'react';
import { Box, Container, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import clsx from 'clsx';

const useStyles = makeStyles(() => ({
  root: {
    position: 'relative',
    height: '500px',
  },
  image: {
    position: 'absolute',
    display: 'block',
    background: 'transparent 50% 50% no-repeat',
    backgroundSize: 'cover',
    '&._animate_banner_1_image_0': {
      height: '416px',
      width: '182px',
      left: '75px',
      top: '50%',
      transform: 'translateY(-50%)',
    },
    '&._animate_banner_1_image_1': {
      height: '500px',
      width: '485px',
      right: '50px',
      top: '0px',
    },
  },
  panel: {
    position: 'absolute',
    top: '50%',
    left: '25%',
    maxWidth: '260px',
    transform: 'translateY(-50%)',
  },
  title: {
    fontSize: '50px',
    textTransform: 'uppercase',
    fontWeight: 'bold',
    lineHeight: '1.2',
    marginBottom: '20px',
  },
  subTitle: {
    fontSize: '22px',
    textTransform: 'uppercase',
    lineHeight: '1',
  },
  text: {
    marginBottom: '20px',
  },
}));

const Variant1 = ({ item: { images, title, subTitle, link, text } }) => {
  const classes = useStyles();
  const [load, setLoad] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoad(false);
    }, 300);
  }, []);

  return (
    <Box>
      <Container className={classes.root}>
        {images && (
          <Box className={classes.images}>
            {images.map((item, i) => {
              return (
                <Box
                  key={i}
                  className={clsx(
                    classes.image,
                    !load && `_animate_banner_1_image_${i}`,
                  )}
                  style={{ backgroundImage: `url(${item})` }}
                />
              );
            })}
          </Box>
        )}
        <Box className={clsx(classes.panel, `_animate_banner_1_text`)}>
          {title && (
            <Box className={classes.title}>
              {subTitle && (
                <Typography variant="body2" className={classes.subTitle}>
                  {subTitle}
                </Typography>
              )}
              {title}
            </Box>
          )}

          {text && (
            <Typography variant="body2" className={classes.text}>
              {text}
            </Typography>
          )}

          {link && (
            <Box>
              <Button
                variant="outlined"
                color="primary"
                component={Link}
                to={link.href}
              >
                {link.name}
              </Button>
            </Box>
          )}
        </Box>
      </Container>
    </Box>
  );
};

export default Variant1;
