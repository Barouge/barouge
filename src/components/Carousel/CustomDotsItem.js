import React from 'react';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'block',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
    minWidth: '0',
    padding: '0',
    marginRight: '10px',
  },
}));

const CustomDotsItem = ({ onClick }) => {
  const classes = useStyles();

  return (
    <Button
      className={clsx(classes.root, 'slider-dots__item')}
      variant="contained"
      color="primary"
      onClick={onClick}
    />
  );
};

export default CustomDotsItem;
