import React from 'react';
import Slider from 'react-slick';
import ArrowNext from './ArrowNext.js';
import ArrowPrev from './ArrowPrev.js';
import CustomDots from './CustomDots.js';
import CustomDotsItem from './CustomDotsItem.js';
import ProductCardSimple from '../Product/ProductCard/ProductCardSimple';

const settings = {
  dots: false,
  touchMove: false,
  arrows: true,
  infinite: true,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 1,
  nextArrow: <ArrowNext />,
  prevArrow: <ArrowPrev />,
  appendDots: (dots) => <CustomDots dots={dots} />,
  customPaging: (i) => <CustomDotsItem index={i} />,
};

const CarouselProducts = ({ products }) => {
  if (!products || products.length < 1) return null;

  return (
    <Slider className="home-slider" {...settings}>
      {products.map((item, i) => {
        return <ProductCardSimple key={i} product={item} />;
      })}
    </Slider>
  );
};

export default CarouselProducts;
