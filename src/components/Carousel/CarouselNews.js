import React from 'react';
import Slider from 'react-slick';
import ArrowNext from './ArrowNext.js';
import ArrowPrev from './ArrowPrev.js';
import CustomDots from './CustomDots.js';
import CustomDotsItem from './CustomDotsItem.js';
import NewsCardSimple from '../News/NewsCardSimple';

const settings = {
  dots: false,
  touchMove: false,
  arrows: true,
  infinite: true,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 1,
  nextArrow: <ArrowNext />,
  prevArrow: <ArrowPrev />,
  appendDots: (dots) => <CustomDots dots={dots} />,
  customPaging: (i) => <CustomDotsItem index={i} />,
  responsive: [
    {
      breakpoint: 1280,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false,
        arrows: false,
        touchMove: true,
      },
    },
  ],
};

const CarouselNews = ({ news }) => {
  if (!news || news.length < 1) return null;

  return (
    <Slider className={'home-slider'} {...settings}>
      {news.map((item, i) => {
        return <NewsCardSimple key={i} news={item} />;
      })}
    </Slider>
  );
};

export default CarouselNews;
