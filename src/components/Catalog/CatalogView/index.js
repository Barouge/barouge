import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Box, IconButton } from '@material-ui/core';

import { useDispatch, useSelector } from 'react-redux';
import { changeCatalogView } from '../../../redux/actions/catalogActions';

import ViewListIcon from '@material-ui/icons/ViewList';
import ViewComfyIcon from '@material-ui/icons/ViewComfy';
import ViewHeadlineIcon from '@material-ui/icons/ViewHeadline';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
}));

const CatalogView = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const view = useSelector((state) => state.catalog.view);

  const handleChangeView = (view) => {
    setTimeout(() => {
      dispatch(changeCatalogView(view));
    }, 100);
  };

  return (
    <Box className={classes.root}>
      <IconButton
        onClick={() => handleChangeView('block')}
        className={classes.item}
      >
        <ViewComfyIcon color={view === 'block' ? 'primary' : 'disabled'} />
      </IconButton>
      <IconButton
        onClick={() => handleChangeView('list')}
        className={classes.item}
      >
        <ViewListIcon color={view === 'list' ? 'primary' : 'disabled'} />
      </IconButton>
      <IconButton
        onClick={() => handleChangeView('table')}
        className={classes.item}
      >
        <ViewHeadlineIcon color={view === 'table' ? 'primary' : 'disabled'} />
      </IconButton>
    </Box>
  );
};

export default CatalogView;
