import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography, Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {},
  wrapper: {
    display: 'flex',
  },
  container: {
    marginRight: '10px',
  },
  color: {
    width: '40px',
    height: '40px',
    padding: '0',
    minWidth: '0',
    border: `solid 1px ${theme.color.border}`,
    backgroundSize: 'cover',
  },
}));

const CatalogFiltersSection = ({ filters }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Typography variant="body2">{filters.title}</Typography>
      <Box className={classes.wrapper}>
        {filters.type === 'color' &&
          filters.items.map((item, i) => {
            const type = item.hex ? 'color' : 'image';

            return (
              <Box key={i} className={classes.container}>
                {type === 'color' ? (
                  <Button
                    style={{ backgroundColor: item.hex }}
                    className={classes.color}
                  />
                ) : (
                  <Button
                    style={{ backgroundImage: `url(${item.image})` }}
                    className={classes.color}
                  />
                )}
              </Box>
            );
          })}

        {filters.type === 'series' &&
          filters.items.map((item, i) => {
            return (
              <Box key={i} className={classes.container}>
                <Button
                  style={{ backgroundImage: `url(${item.image})` }}
                  className={classes.color}
                />
              </Box>
            );
          })}

        {filters.type === 'material' &&
          filters.items.map((item, i) => {
            return (
              <Box key={i} className={classes.container}>
                <Button
                  style={{ backgroundImage: `url(${item.image})` }}
                  className={classes.textFilter}
                  size="small"
                  variant="outlined"
                  color="primary"
                >
                  {item.name}
                </Button>
              </Box>
            );
          })}
      </Box>
    </Box>
  );
};

export default CatalogFiltersSection;
