import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Grid,
} from '@material-ui/core';
import clsx from 'clsx';

import { useSelector } from 'react-redux';

import Loader from '../../Loader';
import CatalogFiltersItem from './CatalogFiltersItem';

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: '150px',
    position: 'relative',
    '&._loading': {
      opacity: '.6',
    },
  },
  loader: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
  green: {
    color: theme.palette.green[400],
    '&$checked': {
      color: theme.palette.green[600],
    },
  },
  section: {
    marginBottom: '15px',
  },
  formControl: {
    margin: '0',
    padding: '0',
    width: '100%',
    justifyContent: 'space-between',
    fontSize: '16px',
    '& span': {
      fontSize: '16px',
    },
  },
}));

const CatalogFilters = () => {
  const classes = useStyles();
  const { filters, loading } = useSelector((state) => state.catalog);

  return (
    <Box className={clsx(classes.root, loading && '_loading')}>
      <Grid container spacing={4}>
        {loading && <Loader className={classes.loader} />}
        {filters && (
          <Grid item xs={12}>
            <Grid container spacing={2} alignContent="flex-start">
              {filters.map((item, i) => {
                return <CatalogFiltersItem key={i} filter={item} />;
              })}
            </Grid>
          </Grid>
        )}
      </Grid>
    </Box>
  );
};

export default CatalogFilters;
