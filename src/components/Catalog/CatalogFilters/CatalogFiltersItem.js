import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import {
  Box,
  Checkbox,
  Switch,
  Grid,
} from '@material-ui/core';
import clsx from 'clsx';

import { useDispatch, useSelector } from 'react-redux';
import {
  catalogCurrentFiltersUpdate,
} from '../../../redux/actions/catalogActions';

import CatalogFiltersPrice from './CatalogFiltersPrice';

const useStyles = makeStyles((theme) => ({
  root: {},
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    cursor: 'pointer',
  },
  title: {
    fontWeight: '800',
    fontSize: '14px',
    textTransform: 'uppercase',
    marginBottom: '10px',
  },
  select: {
    padding: '5px',
    border: `solid 1px ${theme.palette.grey[100]}`,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    maxHeight: '150px',
    overflowY: 'auto',
  },
  color: {
    width: '20px',
    height: '20px',
    borderRadius: '50%',
    border: `solid 1px ${theme.palette.grey[100]}`,
    marginRight: '5px',
  },
  label: {
    display: 'flex',
    alignItems: 'center',
    lineHeight: '20px',
    fontSize: '12px',
    fontWeight: '700',
    cursor: 'pointer',
  },
  checkbox: {
    padding: '5px',
  },
}));

const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 42,
    height: 26,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
    '&$checked': {
      transform: 'translateX(16px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: theme.palette.primary[500],
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: theme.palette.primary[500],
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 24,
    height: 24,
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});

const CatalogFiltersItem = ({ filter }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {
    location: { search },
  } = useHistory();
  const { curFilters } = useSelector((state) => state.catalog);
  const [values, setValue] = useState([]);

  const handleChange = (name, event) => {
    const value = event.target ? event.target.checked : event;

    dispatch(catalogCurrentFiltersUpdate(name, value));
  };

  if (filter.type === 'price') return null;

  return (
    <Grid item xs={12} className={classes.section}>
      {(() => {
        switch (filter.type) {
          case 'select':
            return (
              <Box className={classes.wrap}>
                <Box className={classes.title}>{filter.title}</Box>
                <Box className={classes.select}>
                  {filter.items.map((item, i) => {
                    let style;
                    let isImage;
                    if (filter.name === 'colors') {
                      isImage = item.hex.search('#') === -1;
                      style = isImage
                        ? { backgroundImage: `url(${item.hex})` }
                        : { backgroundColor: item.hex };
                    }
                    const index = curFilters[filter.name].indexOf(item.name);

                    return (
                      <Box
                        key={i}
                        component="label"
                        className={clsx(classes.label, '_color')}
                      >
                        <Checkbox
                          color="primary"
                          className={classes.checkbox}
                          checked={index > -1}
                          onChange={(e) => {
                            handleChange(filter.name, item.name);
                          }}
                          name={item.name}
                        />
                        {filter.name === 'colors' && (
                          <Box className={classes.color} style={style} />
                        )}
                        {item.title}
                      </Box>
                    );
                  })}
                </Box>
              </Box>
            );

            break;

          case 'checkbox':
            return (
              <Box component="label" className={classes.wrapper}>
                <Box className={classes.title}>{filter.title}</Box>
                <IOSSwitch
                  checked={curFilters[filter.name]}
                  color="primary"
                  name={filter.name}
                  className={classes.switch}
                  onChange={(e) => handleChange(filter.name, e)}
                />
              </Box>
            );

          case 'price':
            return <CatalogFiltersPrice price={filter.price} />;

            break;
          default:
        }
      })()}
    </Grid>
  );
};

export default CatalogFiltersItem;
