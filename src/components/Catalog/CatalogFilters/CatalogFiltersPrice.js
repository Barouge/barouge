import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import {
  Box,
  Slider,
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {},
}));

const PrettoSlider = withStyles({
  root: {
    color: '#52af77',
    height: 8,
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(Slider);

const CatalogFiltersPrice = ({ price }) => {
  const classes = useStyles();

  console.log(price);

  return (
    <Box className={classes.root}>
      <PrettoSlider
        min={price.price[0]}
        max={price.price[1]}
        value={price.price}
      />
    </Box>
  );
};

export default CatalogFiltersPrice;
