import React, { useState, useEffect } from 'react';
import { Box, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { getAsideCatalog } from '../../../actions/catalog';
import CategoryItem from '../../CategoryItem';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.palette.primary[50],
    padding: '64px 0 40px',
    overflowY: 'auto',
  },
  inner: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: '100%',
  },
}));

const CatalogMenu = () => {
  const classes = useStyles();
  const [catalog, setCatalog] = useState([]);

  useEffect(() => {
    getAsideCatalog().then((res) => {
      setCatalog(res.data);
    });
  }, []);

  return (
    <Box className={classes.root}>
      <Box className={classes.inner}>
        {catalog.length ? (
          catalog.map((item, i) => {
            return <CategoryItem key={i} item={item} />;
          })
        ) : (
          <CircularProgress />
        )}
      </Box>
    </Box>
  );
};

export default CatalogMenu;
