import React from 'react';
import { Grid } from '@material-ui/core';

import ProductCardBlock from '../../Product/ProductCard/ProductCardBlock';
import ProductCardTable from '../../Product/ProductCard/ProductCardTable';
import ProductCardList from '../../Product/ProductCard/ProductCardList';

const CatalogProductsInner = ({ product, view }) => {
  return (
    <Grid
      item
      xs={12}
      md={view === 'block' ? 6 : 12}
      lg={view === 'block' ? 3 : 12}
    >
      {view === 'block' && <ProductCardBlock product={product} view={view} />}
      {view === 'table' && <ProductCardTable product={product} view={view} />}
      {view === 'list' && <ProductCardList product={product} view={view} />}
    </Grid>
  );
};

export default CatalogProductsInner;
