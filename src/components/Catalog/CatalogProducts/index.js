import React from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Grid, Typography, Button } from '@material-ui/core';
import clsx from 'clsx';
import { useSelector, useDispatch } from 'react-redux';
import {
  fetchCatalogProductsMore,
} from '../../../redux/actions/catalogActions';
import { paramsToObject } from '../../../libs';
import CatalogProductsInner from './CatalogProductsInner';
import Loader from '../../Loader';
import _ from 'lodash';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    transition: 'ease .3s all',
    paddingBottom: '80px',
    '&._loading': {
      opacity: '.6',
    },
  },
  loadmore: {
    width: '100%',
    marginTop: '20px',
  },
  button: {
    width: '100%',
    textAlign: 'center',
    height: '60px',
    '&._loading': {
      backgroundColor: theme.palette.primary[100],
    },
  },
  loader: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 'auto',
    height: 'auto',
    zIndex: '100',
  },
}));

const CatalogProducts = ({ products, loading }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const search = history.location.search;
  const urlParams = new URLSearchParams(search);
  const entries = urlParams.entries();
  const params = paramsToObject(entries);
  const { view, onPage, curFilters } = useSelector(
    (state) => state.catalog,
  );

  delete params.page;

  return (
    <Box className={clsx(classes.root, `type-${view}`, loading && '_loading')}>
      {loading && <Loader className={classes.loader} />}
      <Grid container spacing={view === 'table' ? 0 : 1}>
        {products.length > 0 ? (
          products.map((product, i) => {
            const visible = Object.keys(params).every((key) => {
              const prodFilter = product.filters.find(
                (f) => f[key] !== undefined,
              );

              if (prodFilter === undefined) return false;

              let isBoolean = _.isBoolean(prodFilter[key]);
              let isArray = _.isArray(prodFilter[key]);

              if (isBoolean) {
                return params[key];
              }

              if (isArray) {
                return params[key].some((j) => prodFilter[key].includes(j));
              }
            });

            return (
              visible && (
                <CatalogProductsInner key={i} product={product} view={view} />
              )
            );
          })
        ) : (
          <Typography varinat="body1">Ничего не найдено.</Typography>
        )}

        {products.length > onPage * Number(curFilters.page) && (
          <Box className={classes.loadmore}>
            <Button
              className={clsx(classes.button, loading && '_loading')}
              variant="contained"
              color="primary"
              onClick={() => dispatch(fetchCatalogProductsMore())}
              disabled={loading && true}
            >
              {loading ? <Loader /> : 'Подгрузить еще'}
            </Button>
          </Box>
        )}
      </Grid>
    </Box>
  );
};

export default CatalogProducts;
