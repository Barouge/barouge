import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

import CatalogView from '../CatalogView';
import CatalogSort from '../CatalogSort';

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: '30px',
  },
  filters: {
    borderBottom: `solid 1px ${theme.palette.grey[100]}`,
  },
  bottom: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
    borderBottom: `solid 1px ${theme.palette.grey[100]}`,
  },
}));

const CatalogHeader = () => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.bottom}>
        <CatalogSort />
        <CatalogView />
      </Box>
    </Box>
  );
};

export default CatalogHeader;
