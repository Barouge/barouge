import React, { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { catalogPriceSort } from '../../../redux/actions/catalogActions';
import clsx from 'clsx';
import _ from 'lodash';

import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  item: {
    height: '48px',
    lineHeight: '48px',
    fontSize: '14px',
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    marginRight: '15px',
  },
  name: {
    display: 'block',
    marginRight: '5px',
  },
  icon: {
    transition: 'ease .3s all',
    opacity: '0',
    '&.sort-asc': {
      transform: 'rotate(-90deg)',
      opacity: '1',
    },
    '&.sort-desc': {
      transform: 'rotate(90deg)',
      opacity: '1',
    },
  },
}));

const CatalogSort = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [priceSort, setPriceSort] = useState('asc');
  const [popSort, setPopSort] = useState('');
  const products = useSelector((state) => state.catalog.products);

  const sortPriceChange = () => {
    setPopSort('');
    setPriceSort(priceSort === 'desc' ? 'asc' : 'desc');

    if (priceSort === 'desc') {
      dispatch(catalogPriceSort(_.sortBy(products, ['price'])));
    } else {
      dispatch(catalogPriceSort(_.sortBy(products, ['price']).reverse()));
    }
  };

  const sortPopChange = () => {
    setPriceSort('');
    setPopSort(popSort === 'desc' ? 'asc' : 'desc');
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.item} onClick={() => sortPriceChange()}>
        <Box component="span" className={classes.name}>
          По цене
        </Box>
        <ArrowRightAltIcon
          className={clsx(classes.icon, `sort-${priceSort}`)}
        />
      </Box>

      <Box className={classes.item} onClick={() => sortPopChange()}>
        <Box component="span" className={classes.name}>
          По популярности
        </Box>
        <ArrowRightAltIcon className={clsx(classes.icon, `sort-${popSort}`)} />
      </Box>
    </Box>
  );
};

export default CatalogSort;
