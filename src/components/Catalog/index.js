import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';

import { useDispatch, useSelector } from 'react-redux';
import {
  fetchCatalogProducts,
  catalogCurrentFiltersAdd,
  fetchCatalogFilters,
} from '../../redux/actions/catalogActions';
import { paramsToObject } from '../../libs';

import CatalogProducts from './CatalogProducts';
import CatalogAside from './CatalogAside';
import CatalogHeader from './CatalogHeader';

const useStyles = makeStyles(() => ({
  root: {},
}));

const Catalog = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const search = history.location.search;
  const {
    filters,
    products,
    curFilters,
    loading,
  } = useSelector((state) => state.catalog);
  const urlParams = new URLSearchParams(search);
  const entries = urlParams.entries();
  const params = paramsToObject(entries);

  useEffect(() => {
    if (filters) return;

    dispatch(fetchCatalogFilters());
  }, []);

  useEffect(() => {
    if (search) {
      dispatch(catalogCurrentFiltersAdd(params));
    }
  }, [search]);

  useEffect(() => {
    let path = '';

    Object.keys(curFilters).find((k, i, arr) => {
      if (!curFilters[k] || curFilters[k].length < 1) return;

      path = path.concat(
        `${encodeURIComponent(k)}=${encodeURIComponent(curFilters[k])}`,
        '&',
      );
    });

    history.push({
      search: path.slice(0, -1),
    });
  }, [curFilters]);

  useEffect(() => {
    dispatch(fetchCatalogProducts(history.location));
  }, [history.location]);

  return (
    <Grid className={classes.root} container spacing={3}>
      <Grid item xs={12} md={3}>
        <CatalogAside />
      </Grid>
      <Grid item xs={12} md={9}>
        <CatalogHeader />
        {products && <CatalogProducts loading={loading} products={products} />}
      </Grid>
    </Grid>
  );
};

export default Catalog;
