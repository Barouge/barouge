import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  fetchCatalogMenu,
  catalogCurrentFiltersReset,
} from '../../../redux/actions/catalogActions';
import CatalogFilters from '../CatalogFilters';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'sticky',
    top: '110px',
    paddingBottom: '40px',
    paddingRight: '40px',
    marginBottom: '80px',
    maxHeight: 'calc(100vh - 120px)',
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  inner: {
    height: 'auto',
  },
  menu: {
    marginBottom: '20px',
  },
  item: {
    transition: 'ease .3s all',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    padding: '5px 0',
    backgroundColor: theme.color.white,
    '&:last-child': {
      marginBottom: '0',
    },
  },
  name: {
    transition: 'ease .3s all',
    position: 'relative',
    textTransform: 'uppercase',
    display: 'block',
    fontSize: '20px',
    fontWeight: '800',
    fontStyle: 'italic',
    lineHeight: '36px',
    color: '#000',
    '&:hover': {
      color: theme.palette.red[300],
    },
    '&::before': {
      content: `''`,
      position: 'absolute',
      left: '0',
      bottom: '6px',
      width: '100%',
      height: '10px',
      backgroundColor: theme.palette.primary[500],
      opacity: '.3',
    },
  },
  subMenu: {
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: '10px',
  },
  subItem: {
    position: 'relative',
    display: 'inline-block',
    color: '#000',
    fontWeight: '600',
    lineHeight: '18px',
    padding: '5px 0',
    '&::before': {
      transition: 'ease .3s all',
      content: `''`,
      position: 'absolute',
      bottom: '0',
      left: '0',
      width: '100%',
      transform: 'scale(0)',
      height: '1px',
      backgroundColor: '#000',
    },
    '&:hover, &._active': {
      '&::before': {
        transform: 'scale(1)',
      },
    },
  },
}));

const CatalogAside = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const menu = useSelector((state) => state.catalog.menu);

  useEffect(() => {
    if (menu) return;

    dispatch(fetchCatalogMenu());
  }, [menu]);

  return (
    <Box component="aside" className={classes.root}>
      <Box className={classes.inner}>
        {menu && (
          <Box className={classes.menu} component="nav">
            {menu.map((item, i) => {
              return (
                <Box className={classes.item} key={i}>
                  <Link
                    onClick={() => {
                      dispatch(catalogCurrentFiltersReset());
                    }}
                    to={item.href}
                    className={classes.name}
                  >
                    {item.title}
                  </Link>

                  {item.children && item.children.length > 0 && (
                    <Box className={classes.subMenu}>
                      {item.children.map((sub) => {
                        return (
                          <Link
                            to={sub.href}
                            key={sub.id}
                            className={classes.subItem}
                            onClick={() => {
                              dispatch(catalogCurrentFiltersReset());
                            }}
                          >
                            {sub.title}
                          </Link>
                        );
                      })}
                    </Box>
                  )}
                </Box>
              );
            })}
          </Box>
        )}

        <CatalogFilters />
      </Box>
    </Box>
  );
};

export default CatalogAside;
