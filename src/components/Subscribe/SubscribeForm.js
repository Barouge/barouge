import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Button,
} from '@material-ui/core';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { updateSubscribe } from '../../redux/actions/appActions';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  root: {},
  form: {
    display: 'flex',
    width: '100%',
    [theme.breakpoints.down('md')]: {
      flexWrap: 'wrap',
    },
  },
  input: {
    transition: 'ease .3s all',
    padding: `${theme.spacing(2)}px`,
    height: '60px',
    border: `solid 1px ${theme.palette.grey[400]}`,
    width: '100%',
    borderRight: 'none',
    '&._error': {
      borderColor: theme.palette.red[500],
    },
    [theme.breakpoints.down('md')]: {
      height: '50px',
    },
  },
  field: {
    position: 'relative',
    width: 'calc(100% - 150px)',
    [theme.breakpoints.down('md')]: {
      width: 'calc(100% - 100px)',
    },
  },
  submit: {
    boxShadow: 'none',
    height: '60px',
    borderRadius: '0',
    width: '150px',
    backgroundColor: theme.palette.grey[900],
    '&:hover': {
      backgroundColor: theme.palette.grey[900],
    },
    border: '0',
    [theme.breakpoints.down('md')]: {
      width: '100px',
      height: '50px',
      fontSize: '12px',
    },
  },
  error: {
    position: 'absolute',
    bottom: 'calc(100% + 5px)',
    right: '5px',
    color: theme.palette.red[500],
    fontSize: '12px',
  },
}));

const validationSchema = yup.object({
  email: yup
    .string()
    .email('Укажите почту формата: name@domain.ru')
    .required('Укажите вашу почту'),
});

const SubscribeForm = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const subscribe = useSelector((state) => state.app.subscribe);

  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      dispatch(updateSubscribe(values.email));
    },
  });

  return (
    <Box className={classes.root}>
      <form className={classes.form} onSubmit={formik.handleSubmit}>
        <Box className={classes.field}>
          {formik.values.email !== '' &&
            (formik.errors || !subscribe.status) && (
              <Box className={classes.error}>
                {formik.errors.email || subscribe.error}
              </Box>
            )}
          <Box
            className={clsx(
              classes.input,
              formik.touched.email && Boolean(formik.errors.email) && '_error',
            )}
            value={formik.values.email}
            onChange={formik.handleChange}
            component="input"
            type="email"
            name="email"
            placeholder="E-mail"
          />
        </Box>
        <Button
          className={classes.submit}
          type="submit"
          variant="contained"
          color="primary"
        >
          Подписаться
        </Button>
      </form>
    </Box>
  );
};

export default SubscribeForm;
