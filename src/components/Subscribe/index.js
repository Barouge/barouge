import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Container,
  Typography,
  Grid,
} from '@material-ui/core';
import { useSelector } from 'react-redux';
import AirplaneIcon from '../../icons/AirplaneIcon';
import SubscribeForm from './SubscribeForm';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
  },
  container: {
    height: '100%',
  },
  wrapper: {
    position: 'relative',
    paddingLeft: '170px',
    height: '100%',
    [theme.breakpoints.down('md')]: {
      paddingLeft: '0',
    },
  },
  icon: {
    position: 'absolute',
    top: '50%',
    transform: 'translateY(-50%)',
    left: '0',
    height: '130px',
    width: '130px',
    color: theme.palette.grey[400],
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
  subscribe: {
    height: '100%',
  },
  title: {
    fontSize: '20px',
    fontWeight: 'bold',
    marginRight: '20px',
    color: theme.palette.grey[700],
    [theme.breakpoints.down('md')]: {
      fontSize: '14px',
      marginRight: '0',
      marginBottom: '20px',
    },
  },
}));

const Subscribe = () => {
  const classes = useStyles();
  const subscribe = useSelector((state) => state.app.subscribe);

  return (
    <Box className={classes.root}>
      <Container className={classes.container}>
        <Box className={classes.wrapper}>
          <AirplaneIcon
            className={clsx(classes.icon, subscribe.status && '_subscribed')}
          />
          <Grid container className={classes.subscribe} alignItems="center">
            <Grid item xs={12} lg={subscribe.status ? 12 : 4}>
              <Typography className={classes.title}>
                {subscribe.status
                  ? 'Вы успешно подписались на рассылку! Пройдите по ссылке в письме, чтобы получить 10% скидку.'
                  : 'Подпишись и получи персональную скидку в 10%!'}
              </Typography>
            </Grid>
            {!subscribe.status && (
              <Grid item xs={12} lg={8}>
                <Box className={classes.form}>
                  <SubscribeForm />
                </Box>
              </Grid>
            )}
          </Grid>
        </Box>
      </Container>
    </Box>
  );
};

export default Subscribe;
