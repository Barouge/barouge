import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Header from '../Header';
import Footer from '../Footer';
import { useDispatch, useSelector } from 'react-redux';
import clsx from 'clsx';
import { Helmet } from 'react-helmet';
import { fetchSeo } from '../../redux/actions/appActions';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
  },
  main: {
    transition: 'ease .3s all',
    position: 'relative',
    zIndex: '110',
    marginTop: '100px',
    minHeight: 'calc(100vh - 100px)',
    marginBottom: '620px',
    backgroundColor: '#fff',
    boxShadow: `0 0 4px rgba(0, 0, 0, .6)`,
    '&._subscribed': {
      marginBottom: '470px',
    },
    [theme.breakpoints.down('md')]: {
      marginBottom: '0 !important',
    },
  },
}));

const Layout = ({ children, ...props }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {
    location: { pathname },
  } = useHistory();
  const { seo, subscribe } = useSelector((state) => state.app);
  const [subscribed, setSubscribed] = useState(false);

  useEffect(() => {
    if (!subscribe) return;

    setTimeout(() => {
      setSubscribed(subscribe.status);
    }, 3000);
  }, [subscribe]);

  useEffect(() => {
    dispatch(fetchSeo(pathname));
  }, []);

  console.log(seo);

  return (
    <div {...props} className={clsx(classes.root, subscribed && '_subscribed')}>
      {seo && (
        <Helmet titleTemplate="Barouge. %s">
          {seo.title && <title>{seo.title}</title>}
          {seo.description && (
            <meta name="description" content={seo.description} />
          )}
        </Helmet>
      )}
      <Header />
      <main className={clsx(classes.main, subscribed && '_subscribed')}>
        {children}
      </main>
      <Footer />
    </div>
  );
};

export default Layout;
