import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Button, Typography } from '@material-ui/core';
import BasketProduct from '../BasketProduct';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {},
  empty: {},
  text: {
    fontSize: '20px',
    fontWeight: '700',
    marginBottom: '40px',
  },
  btn: {
    color: theme.palette.primary[300],
  },
}));

const BasketList = ({ products }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      {products.length > 0 ? (
        products.map((item, i) => {
          return <BasketProduct key={i} product={item} />;
        })
      ) : (
        <Box className={classes.empty}>
          <Typography variant="body2" className={classes.text}>
            Ваша корзина пуста
          </Typography>
          <Button
            className={classes.btn}
            component={Link}
            to={'/catalog'}
            color="primary"
            variant="outlined"
          >
            Перейти в каталог
          </Button>
        </Box>
      )}
    </Box>
  );
};

export default BasketList;
