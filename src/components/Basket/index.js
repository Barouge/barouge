import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Grid } from '@material-ui/core';
import BasketDetails from './BasketDetails';
import BasketList from './BasketList';

const useStyles = makeStyles((theme) => ({
  root: {},
  aside: {
    position: 'sticky',
    top: '90px',
  },
}));

const Basket = ({ basket }) => {
  const classes = useStyles();
  const empty = basket.products && basket.products.length === 0;

  return (
    <Grid
      container
      alignItems="flex-start"
      spacing={4}
      className={classes.root}
    >
      <Grid item xs={12} lg={empty ? 12 : 9} className={classes.cart}>
        <Box className={classes.list}>
          {basket && <BasketList products={basket.products} />}
        </Box>
      </Grid>
      {!empty && (
        <Grid item xs={12} lg={3} className={classes.aside}>
          <BasketDetails details={basket} />
        </Grid>
      )}
    </Grid>
  );
};

export default Basket;
