import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography, Tooltip, Grid } from '@material-ui/core';
import { Link } from 'react-router-dom';
import CloseIcon from '@material-ui/icons/Close';
import { cutText } from '../../../libs';
import ProductCount from '../../Product/ProductCount';
import { useStyles } from './hooks';

const BasketProduct = ({ product }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.remove}>
        <Tooltip title="Удалить">
          <CloseIcon />
        </Tooltip>
      </Box>
      <Grid container spacing={2} className={classes.grid}>
        <Grid item lg={3} md={6} xs={12}>
          <Link to={product.href} className={classes.image}>
            <img src={product.preview} alt="" />
          </Link>
        </Grid>
        <Grid item lg={6} md={6} xs={12}>
          <Box className={classes.info}>
            <Box className={classes.title}>
              <Tooltip title={product.title}>
                <Link className={classes.link} to={product.href}>
                  {product.title}
                </Link>
              </Tooltip>
            </Box>
            {product.description && (
              <Typography className={classes.description} variant="body2">
                {cutText(product.description, 80)}
              </Typography>
            )}
            <Box className={classes.table}>
              {product.vendor && (
                <Box className={classes.row}>
                  <Box className={classes.key}>Код продукта:&nbsp;</Box>
                  <Box className={classes.val}>{product.vendor}</Box>
                </Box>
              )}
              {product.volume && (
                <Box className={classes.row}>
                  <Box className={classes.key}>Объем:&nbsp;</Box>
                  <Box className={classes.val}>{product.volume}</Box>
                </Box>
              )}
              {product.color && (
                <Box className={classes.row}>
                  <Box className={classes.key}>Цвет:&nbsp;</Box>
                  <Box className={classes.val}>{product.color.name}</Box>
                </Box>
              )}
            </Box>
          </Box>
        </Grid>
        {product.price && (
          <Grid item lg={3} md={12} xs={12}>
            <Box className={classes.price}>
              <ProductCount
                count={product.count}
                totalCount={product.totalCount}
                price={product.price}
                discount={product.discount}
              />
            </Box>
          </Grid>
        )}
      </Grid>
    </Box>
  );
};

export default BasketProduct;
