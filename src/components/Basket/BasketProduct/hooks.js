import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    transition: 'ease .3s all',
    position: 'relative',
    border: `solid 1px ${theme.palette.grey[300]}`,
    borderRadius: '5px',
    height: '200px',
    display: 'flex',
    marginBottom: '20px',
    '&:hover': {
      boxShadow: `0 0 3px ${theme.palette.grey[300]}`,
    },
  },
  remove: {
    position: 'absolute',
    transition: 'ease .3s all',
    top: '10px',
    right: '10px',
    width: '24px',
    height: '24px',
    cursor: 'pointer',
    '&:hover': {
      transform: 'rotate(180deg)',
    },
  },
  image: {
    display: 'block',
    height: '100%',
    padding: '10px 0',
    '& img': {
      maxWidth: '100%',
      display: 'block',
      objectFit: 'cover',
      maxHeight: '100%',
      margin: '0 auto',
    },
  },
  color: {
    display: 'flex',
    alignItems: 'center',

    '& span': {
      display: 'block',
      marginLeft: '5px',
    },
  },
  info: {
    padding: '10px',
  },
  title: {
    fontSize: '20px',
    marginBottom: '10px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  description: {
    fontSize: '16px',
    marginBottom: '10px',
  },
  link: {
    color: theme.color.black,
  },
  table: {
    fontSize: '14px',
  },
  row: {
    padding: '3px 5px',
    display: 'flex',
    '&:nth-child(2n - 1)': {
      backgroundColor: theme.palette.grey[50],
    },
  },
  val: {
    fontWeight: '600',
  },
  grid: {
    '& .MuiGrid-item': {
      height: '100%',
    },
  },
  price: {
    width: '100%',
    paddingTop: '50px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    padding: '20px',
  },
}));