import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { declension } from '../../../libs';

const useStyles = makeStyles((theme) => ({
  root: {},
  item: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    borderBottom: `dashed 1px ${theme.color.border}`,
  },
  val: {
    paddingLeft: '3px',
    fontWeight: '600',
    position: 'relative',
    top: '3px',
    display: 'block',
    fontSize: '14px',
    lineHeight: '20px',
    backgroundColor: theme.color.white,
  },
  key: {
    paddingRight: '3px',
    position: 'relative',
    top: '3px',
    display: 'block',
    fontSize: '14px',
    lineHeight: '20px',
    backgroundColor: theme.color.white,
  },
  discount: {
    color: theme.color.pink,
  },
  listItem: {
    '&:last-child': {
      '& span': {
        fontSize: '18px',
        fontWeight: '600',
      },
    },
  },
  result: {
    padding: '10px 0',
  },
}));

const BasketDetails = ({ details: { data } }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.result}>
        Итого:{' '}
        <strong>
          {data.count +
            ' ' +
            declension(['товар', 'товара', 'товаров'], data.count)}
        </strong>{' '}
        на сумму <strong>{data.summ.toLocaleString('Ru-ru')} ₽</strong>
      </Box>
      <Button component={Link} to="/order" variant="contained" color="primary">
        Перейти к оформлению
      </Button>
    </Box>
  );
};

export default BasketDetails;
