import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Grid,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import ProductPreview from '../../ProductPreview';
import ProductContent from '../../ProductContent';
import ProductInfoTable from '../../ProductInfoTable';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    paddingTop: '40px',
  },
  wrapper: {
    position: 'sticky',
    top: '150px',
  },
  title: {
    fontSize: '42px',
    fontWeight: '700',
    marginBottom: '10px',
  },
  description: {
    fontSize: '24px',
  },
  row: {
    marginBottom: '40px',
    '&:last-child': {
      marginBottom: '0',
    },
  },
}));

const ProductCardPage = ({ product, handleProductUpdate, page, data }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Grid container spacing={2}>
        {product && (
          <Grid item xs={12} className={classes.row}>
            <Grid container>
              <Grid item xs={12} lg={6}>
                <Box className={classes.wrapper}>
                  {product.images && (
                    <ProductPreview
                      color={data.color}
                      images={product.images}
                    />
                  )}
                </Box>
              </Grid>
              <Grid item xs={12} lg={6}>
                <ProductContent
                  handleProductUpdate={handleProductUpdate}
                  product={product}
                  page={page}
                  data={data}
                />
              </Grid>
            </Grid>
          </Grid>
        )}
        {product && product.description && (
          <Grid item xs={12} className={classes.row}>
            <Typography varinat="h2" className={classes.title}>
              Описание
            </Typography>

            <Box className={classes.description}>{product.description}</Box>
          </Grid>
        )}
        {product &&
          (product.information.description || product.information.common) && (
            <Grid item xs={12} className={classes.row}>
              {product.information.common && (
                <ProductInfoTable data={product.information.common} />
              )}
              {product.information.description &&
                product.information.description.map((item, i) => {
                  return (
                    <Accordion square={true} key={i}>
                      <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        className={classes.link}
                      >
                        {item.title}
                      </AccordionSummary>
                      <AccordionDetails>{item.content}</AccordionDetails>
                    </Accordion>
                  );
                })}
            </Grid>
          )}
      </Grid>
    </Box>
  );
};

export default ProductCardPage;
