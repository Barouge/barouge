import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Avatar,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  Tooltip,
  Grid,
} from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '385px',
    width: 'calc(100% - 10px)',
    margin: '0 10px 10px 0',
    '&:nth-child(3n)': {
      marginRight: '0',
    },
  },
  media: {},
}));

const ProductCard = ({ product }) => {
  const classes = useStyles();

  return (
    <Grid item xs={12} md={6} lg={4}>
      <Card component={Link} to={product.href} className={classes.root}>
        <CardHeader
          avatar={
            product.stickers &&
            product.stickers.map((item, i) => {
              return (
                <Tooltip title={item.title} arrow key={i}>
                  <Avatar>{item.tag}</Avatar>
                </Tooltip>
              );
            })
          }
          title={product.fullName}
          subheader={product.vendor}
        />
        <CardMedia
          className={classes.media}
          image={product.image}
          title={product.fullName}
        />
        <CardContent>Контент</CardContent>
        <CardActions disableSpacing>подвал</CardActions>
      </Card>
    </Grid>
  );
};

export default ProductCard;
