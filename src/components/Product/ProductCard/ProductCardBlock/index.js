import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import ProductPrice from '../../ProductPrice';
import ProductCardSlider from '../../ProductCardSlider';
import ProductAddBasket from '../../ProductAddBasket';
import ProductVolumes from '../../ProductVolumes';

const useStyles = makeStyles((theme) => ({
  root: {
    transition: 'ease .3s all',
    position: 'relative',
    height: '380px',
    width: '100%',
    margin: '0',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    border: `solid 1px ${theme.color.greyLight}`,
    backgroundColor: theme.color.white,
    overflow: 'hidden',
    '&:hover, &._active': {
      overflow: 'visible',
      zIndex: '100',
      borderColor: theme.palette.primary[500],
      '& .product-card__basket': {
        borderColor: theme.palette.primary[500],
        opacity: 1,
        height: '69px',
      },
    },
    '&:hover': {
      zIndex: '200',
    },
    '&.type-list, &.type-table': {
      flexDirection: 'row',
    },
    '&.type-list': {
      height: '250px',
    },
    '&.type-table': {
      height: '80px',
      marginTop: '-1px',
      overflow: 'visible',
    },
  },
  header: {
    position: 'relative',
    width: '100%',
    height: '200px',
    overflow: 'hidden',
    padding: '10px',
    '&.type-list': {
      width: '250px',
      height: '250px',
      padding: '20px',
      position: 'static',
    },
    '&.type-table': {
      width: '80px',
      height: '80px',
      padding: '5px',
      overflow: 'visible',
    },
  },
  footer: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: '15px',
    width: '100%',
    height: '180px',
    '&.type-list': {
      width: 'calc(100% - 250px)',
      height: '100%',
      padding: '20px',
      flexDirection: 'row',
    },
    '&.type-table': {
      width: 'calc(100% - 80px)',
      height: '100%',
      padding: '5px',
      flexDirection: 'row',
      alignItems: 'center',
    },
  },
  stickers: {
    position: 'absolute',
    zIndex: '10',
    top: '0',
    right: '0',
    width: '100%',
    padding: '5px',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    '&.type-list': {
      justifyContent: 'flex-start',
      padding: '10px',
      right: 'auto',
      left: '0',
    },
  },
  imageWrap: {
    '&.type-table': {
      position: 'relative',
      height: '100%',
      display: 'block',
      transition: 'ease .3s all',
      backgroundColor: theme.color.white,
      '&:hover': {
        transform: 'scale(2.5)',
        boxShadow: '0px 0px 6px rgb(0, 0, 0, .3)',
        zIndex: '1000',
      },
    },
  },
  image: {
    display: 'block',
    width: '100%',
    objectFit: 'contain',
    maxHeight: '100%',
  },
  basket: {
    opacity: '0',
    height: '0',
    transition: 'ease .3s all',
    position: 'absolute',
    top: '100%',
    left: '-1px',
    right: '-1px',
    border: `solid 1px ${theme.color.greyLight}`,
    borderTop: '0',
    padding: '5px 15px 15px',
    backgroundColor: theme.color.white,
  },
  title: {
    display: 'block',
    transition: 'ease .3s all',
    position: 'relative',
    fontSize: '20px',
    lineHeight: '1.2',
    marginBottom: '10px',
    color: theme.color.black,
    '&:hover': {
      color: theme.color.red,
    },
    '&.type-table': {
      marginBottom: '0',
    },
  },
  button: {
    marginTop: 'auto',
    fontSize: '12px',
    whiteSpace: 'nowrap',
  },
  priceWrap: {
    marginTop: 'auto',
  },
  lastchild: {
    display: 'flex',
    width: '200px',
    justifyContent: 'flex-end',
    flexDirection: 'column',
    marginLeft: 'auto',
    height: '100%',
    '& .product-price': {
      marginBottom: '20px',
    },
    '&.type-table': {
      flex: 1,
      width: 'auto',
      margin: '0',
      flexDirection: 'row',
      alignItems: 'center',
      '& .product-price': {
        marginBottom: '0',
        marginRight: '20px',
      },
    },
  },
  rating: {
    position: 'absolute',
    zIndex: '100',
    top: '0',
    left: '0',
    padding: '5px',
    '&.type-list': {
      padding: '20px',
      left: 'auto',
      right: '0',
    },
  },
  descr: {
    fontSize: '12px',
    marginBottom: '10px',
  },
  content: {
    '&.type-list': {
      width: 'calc(100% - 200px)',
    },
  },
  itemsWrap: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
}));

const ProductCardBlock = ({ product, view }) => {
  const classes = useStyles();

  return (
    <Box className={clsx(classes.root, 'type-' + view)}>
      <Box
        className={clsx(classes.header, 'product-card__header', 'type-' + view)}
      >
        <Link
          to={product.href}
          className={clsx(classes.imageWrap, 'type-' + view)}
        >
          {product.images.length > 1 ? (
            <ProductCardSlider images={product.images} />
          ) : (
            <img
              src={'http://barouge2.ck31528.tmweb.ru/' + product.images[0]}
              alt=""
              className={clsx(classes.image, 'type-' + view)}
            />
          )}
        </Link>
      </Box>
      <Box
        className={clsx(classes.footer, 'product-card__footer', 'type-' + view)}
      >
        <Box className={clsx(classes.content, 'type-' + view)}>
          <Link
            to={product.href}
            className={clsx(classes.title, 'type-' + view)}
          >
            {product.shortName + ' ' + product.series}
          </Link>
        </Box>

        <Box className={classes.priceWrap}>
          {product.volumes && <ProductVolumes volumes={product.volumes} />}
          <ProductPrice price={product.price} discount={product.discount} />
        </Box>

        <Box className={clsx(classes.basket, 'product-card__basket')}>
          <ProductAddBasket product={product} />
        </Box>
      </Box>
    </Box>
  );
};

export default ProductCardBlock;
