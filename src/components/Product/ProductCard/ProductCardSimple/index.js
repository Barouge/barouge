import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  IconButton,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

import ProductSticker from '../../ProductSticker';
import ProductPrice from '../../ProductPrice';

const useStyles = makeStyles((theme) => ({
  root: {
    transition: 'ease .3s all',
    position: 'relative',
    height: '280px',
    width: 'calc(100% - 20px)',
    margin: '10px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    border: `solid 1px ${theme.color.greyLight}`,
    backgroundColor: theme.color.white,
    overflow: 'hidden',
    '&:hover': {
      borderColor: theme.palette.primary[100],
      '& .product-btn': {
        backgroundColor: theme.palette.primary[50],
        opacity: '1',
      },
      '& ._image': {
        transform: 'scale(1.1)',
      },
      '& ._title': {
        color: theme.palette.red[900],
      },
    },
  },
  header: {
    position: 'relative',
    width: '100%',
    height: '140px',
    overflow: 'hidden',
    paddingTop: '10px',
  },
  content: {
    position: 'relative',
    height: '140px',
    paddingBottom: '60px',
    padding: '15px 15px',
  },
  footer: {
    position: 'absolute',
    bottom: '15px',
    left: '15px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 'calc(100% - 30px)',
    height: '60px',
  },
  stickers: {
    position: 'absolute',
    zIndex: '10',
    top: '5px',
    right: '5px',
    padding: '5px',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: 'auto',
  },
  imageWrap: {
    height: '100%',
  },
  image: {
    transition: 'ease .3s all',
    display: 'block',
    width: '100%',
    objectFit: 'contain',
    maxHeight: '100%',
  },
  title: {
    display: 'block',
    transition: 'ease .3s all',
    position: 'relative',
    fontSize: '16px',
    margin: '10px 0',
    fontWeight: '600',
    lineHeight: '1.2',
    color: theme.color.black,
  },
  iconBtn: {
    transition: 'ease .3s all',
    opacity: '0',
    marginLeft: 'auto',
  },
}));

const ProductCardSimple = ({ product }) => {
  const classes = useStyles();

  return (
    <Box
      component={Link}
      to={product.href}
      className={clsx(classes.root, 'product-card')}
    >
      <Box className={classes.header}>
        {product.stickers && (
          <Box className={clsx(classes.stickers, 'product-card__stickers')}>
            {product.stickers.map((item, i) => {
              return <ProductSticker key={i} sticker={item} />;
            })}
          </Box>
        )}

        <Box to={product.href} className={classes.imageWrap}>
          <img
            src={product.images[0]}
            alt=""
            className={clsx(classes.image, '_image')}
          />
        </Box>
      </Box>
      <Box className={classes.content}>
        <Box className={clsx(classes.title, '_title')}>
          {product.shortName + ' ' + product.series}
        </Box>
        <Box className={classes.footer}>
          <ProductPrice price={product.price} discount={product.discount} />
          <IconButton
            size="medium"
            className={clsx(classes.iconBtn, 'product-btn')}
          >
            <ArrowForwardIcon color="primary" />
          </IconButton>
        </Box>
      </Box>
    </Box>
  );
};

export default ProductCardSimple;
