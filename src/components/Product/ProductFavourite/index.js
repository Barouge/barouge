import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, IconButton } from '@material-ui/core';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';

const useStyles = makeStyles(() => ({
  root: {},
}));

const ProductFavourite = () => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <IconButton color="primary">
        <FavoriteBorderIcon />
      </IconButton>
    </Box>
  );
};

export default ProductFavourite;
