import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {},
}));

const Product = ({ product }) => {
  const classes = useStyles();

  console.log(product);

  return <Box className={classes.root}>{product.fullName}</Box>;
};

export default Product;
