import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import MonochromePhotosIcon from '@material-ui/icons/MonochromePhotos';
import Carousel from '../../Carousel';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
  },
  noImage: {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: '200px',
    height: '200px',
    color: theme.color.black,
  },
}));

const ProductSlider = ({ images }) => {
  const classes = useStyles();
  const settings = {
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <Box className={classes.root}>
      {!!images.length ? (
        <Box className={classes.slider}>
          <Carousel settings={settings} items={images} />
        </Box>
      ) : (
        <Box className={classes.noImage}>
          <MonochromePhotosIcon color="primary" className={classes.icon} />
        </Box>
      )}
    </Box>
  );
};

export default ProductSlider;
