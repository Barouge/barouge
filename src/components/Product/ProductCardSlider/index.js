import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import MonochromePhotosIcon from '@material-ui/icons/MonochromePhotos';
import Carousel from '../../Carousel';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
  },
  slider: {
    height: '100%',
    '& .slider-dots': {
      transition: 'ease .3s all',
      opacity: 0,
      bottom: '0',
      '&__item': {
        width: '10px',
        height: '10px',
        marginRight: '5px',
      },
    },
    '&:hover': {
      '& .slider-dots': {
        opacity: 1,
      },
    },
  },
  noImage: {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: '200px',
    height: '200px',
    color: theme.color.black,
  },
}));

const ProductCardSlider = ({ images }) => {
  const classes = useStyles();
  const settings = {
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <Box className={classes.root}>
      {!!images.length ? (
        <Box className={classes.slider}>
          <Carousel settings={settings} items={images} />
        </Box>
      ) : (
        <Box className={classes.noImage}>
          <MonochromePhotosIcon color="primary" className={classes.icon} />
        </Box>
      )}
    </Box>
  );
};

export default ProductCardSlider;
