import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Avatar, Tooltip } from '@material-ui/core';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '28px',
    height: '28px',
    marginRight: '5px',
    '&:last-child': {
      marginRight: '0',
    },
  },
  sticker: {
    fontWeight: '600',
    fontSize: '12px',
    width: '100%',
    height: '100%',
    '&.hit': {
      backgroundColor: theme.palette.primary.main,
    },
    '&.discount': {
      backgroundColor: theme.palette.secondary.main,
    },
    '&.new': {
      backgroundColor: theme.palette.success.dark,
    },
  },
}));

const ProductSticker = ({ sticker }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Tooltip title={sticker.title}>
        <Avatar size="small" className={clsx(classes.sticker, sticker.class)}>
          {sticker.tag}
        </Avatar>
      </Tooltip>
    </Box>
  );
};

export default ProductSticker;
