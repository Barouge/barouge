import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import AnimatedNumber from 'animated-number-react';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '&._large': {
      '& ._price, & ._newPrice': {
        fontSize: '60px',
      },
      '& ._newPrice': {
        '&::before': {
          height: '10px',
          bottom: '15px',
        },
      },
      '& ._discount': {
        display: 'none',
      },
      '& ._oldPrice': {
        fontSize: '26px',
      },
    },
  },
  wrapper: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  price: {
    position: 'relative',
    color: theme.palette.grey[900],
    fontWeight: '900',
    fontSize: '22px',
    fontStyle: 'italic',
    '&::before': {
      content: `''`,
      position: 'absolute',
      left: '0',
      bottom: '5px',
      width: '100%',
      height: '10px',
      backgroundColor: theme.palette.primary[500],
      opacity: '.3',
    },
  },
  newPrice: {
    position: 'relative',
    color: theme.palette.grey[900],
    fontWeight: '900',
    fontSize: '22px',
    whiteSpace: 'nowrap',
    fontStyle: 'italic',
    '& span:last-child': {
      fontWeight: '600',
      fontSize: '20px',
    },
    '&::before': {
      content: `''`,
      position: 'absolute',
      left: '0',
      bottom: '5px',
      width: '100%',
      height: '10px',
      backgroundColor: theme.palette.primary[500],
      opacity: '.3',
    },
  },
  oldPrice: {
    position: 'absolute',
    textDecoration: 'line-through',
    top: '-2px',
    left: 'calc(100% + 5px)',
    fontSize: '14px',
    fontWeight: '500',
    color: theme.palette.grey[700],
    padding: '0 2px',
    whiteSpace: 'nowrap',
  },
  discount: {
    position: 'absolute',
    bottom: '-2px',
    left: 'calc(100% + 5px)',
    whiteSpace: 'nowrap',
    backgroundColor: theme.palette.primary[100],
    borderRadius: '2px',
    padding: '0 3px',
    fontSize: '11px',
    lineHeight: '14px',
    height: '14px',
    color: theme.palette.red[300],
  },
  rouble: {
    fontWeight: 'normal',
  },
}));

const ProductPrice = ({ price, discount, large }) => {
  const classes = useStyles();
  const formatValue = (value) => value.toFixed(0);

  return (
    <Box className={clsx(classes.root, 'product-price', large && '_large')}>
      {discount && discount > 0 ? (
        <Box className={classes.wrapper}>
          <Box className={clsx(classes.newPrice, '_newPrice')}>
            <AnimatedNumber
              value={price - (price * discount) / 100}
              formatValue={formatValue}
              duration={300}
            />{' '}
            <Box component="span" className={classes.rouble}>
              ₽
            </Box>
          </Box>
          <Box variant="body2" className={clsx(classes.oldPrice, '_oldPrice')}>
            <AnimatedNumber
              value={price}
              formatValue={formatValue}
              duration={300}
            />{' '}
            <Box component="span" className={classes.rouble}>
              ₽
            </Box>
          </Box>
          <Box className={clsx(classes.discount, '_discount')}>
            - {discount} %
          </Box>
        </Box>
      ) : (
        <Box className={clsx(classes.price, '_price')}>
          <AnimatedNumber
            value={price}
            formatValue={formatValue}
            duration={300}
          />{' '}
          <Box component="span" className={classes.rouble}>
            ₽
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default ProductPrice;
