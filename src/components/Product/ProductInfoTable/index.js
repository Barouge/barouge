import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
  root: {},
  accordion: {
    flexWrap: 'wrap',
  },
  row: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottom: `dashed 1px ${theme.color.border}`,
    height: '30px',
    marginBottom: '5px',
    '&:last-child': {
      marginBottom: '0',
    },
  },
}));

const ProductInfoTable = ({ data }) => {
  const classes = useStyles();

  return (
    <Accordion defaultExpanded={true} square={true} className={classes.root}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        className={classes.link}
      >
        Информация о товаре
      </AccordionSummary>
      <AccordionDetails className={classes.accordion}>
        {data.map((item, i) => {
          return (
            <Box key={i} className={classes.row}>
              <Typography variant="body2">{item.key}</Typography>
              <Typography variant="body2">{item.val}</Typography>
            </Box>
          );
        })}
      </AccordionDetails>
    </Accordion>
  );
};

export default ProductInfoTable;
