import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Button } from '@material-ui/core';
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import { updateCurrentProductData } from '../../../redux/actions/catalogActions';

const useStyles = makeStyles((theme) => ({
  root: {},
  volume: {
    position: 'relative',
    boxShadow: 'none',
    minWidth: '80px',
    padding: '0 20px',
    height: '50px',
    color: theme.palette.grey[900],
    border: `solid 1px ${theme.palette.grey[100]}`,
    marginRight: '10px',
    textTransform: 'lowercase',
    fontSize: '16px',
    fontWeight: '700',
    '&:hover': {
      borderColor: theme.palette.grey[100],
    },
    '&::before': {
      transition: 'ease .3s all',
      content: `''`,
      position: 'absolute',
      bottom: '0',
      left: '0',
      width: '100%',
      height: '5px',
      opacity: '.3',
      backgroundColor: theme.palette.primary[500],
      transform: 'scale(0)',
      borderRadius: '0 0 5px 5px',
    },
    '&._active': {
      '&::before': {
        transform: 'scale(1)',
      },
    },
  },
  expansive: {
    marginLeft: '3px',
    fontSize: '12px',
    color: theme.palette.grey[400],
    fontWeight: '500',
  },
}));

const ProductVolumes = ({ volumes, volume, price, discount }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const curVol = parseInt(volume);
  const { curProduct } = useSelector((state) => state.catalog);

  const handleClick = (item) => {
    dispatch(
      updateCurrentProductData({
        ...curProduct.data,
        volume: item.volume,
        price: item.price,
        vendor: item.vendor,
        discount: item.discount,
        title: item.title,
        colors: item.colors,
      }),
    );
  };

  return (
    <Box className={classes.root}>
      {volumes.map((item, i) => {
        const vol = parseInt(item.volume);
        const totItemPrice = parseInt(
          item.price - (item.price * item.discount) / 100,
        );
        const totPrice = parseInt(price - (price * discount) / 100);
        const diff = totItemPrice - totPrice;

        return (
          <Button
            variant={'outlined'}
            className={clsx(classes.volume, curVol === vol && '_active')}
            key={i}
            color="primary"
            onClick={() => handleClick(item)}
          >
            {item.volume}
            {diff > 0 && <Box className={classes.expansive}>(+ {diff} ₽)</Box>}
          </Button>
        );
      })}
    </Box>
  );
};

export default ProductVolumes;
