import React, { useState, useEffect } from 'react';
import { GlassMagnifier } from 'react-image-magnifiers';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import clsx from 'clsx';
import Slider from 'react-slick';
import ArrowNext from '../../Carousel/ArrowNext.js';
import ArrowPrev from '../../Carousel/ArrowPrev.js';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '500px',
    display: 'flex',
    justifyContent: 'flex-start',
    '& .slick-slide': {
      width: '80px',
      height: '100px',
    },
    '& .slick-prev': {
      top: '-50px',
      left: '20px',
      transform: 'rotate(90deg)',
      width: '40px',
      height: '60px',
      '&:hover': {
        transform: 'rotate(90deg) scale(0.9)',
      },
    },
    '& .slick-next': {
      bottom: '-60px',
      top: 'auto',
      left: '20px',
      transform: 'rotate(90deg)',
      width: '40px',
      height: '60px',
      '&:hover': {
        transform: 'rotate(90deg) scale(0.9)',
      },
    },
  },
  thumb: {
    height: '100px',
    width: '80px',
    border: `solid 1px ${theme.palette.primary[50]}`,
    cursor: 'pointer',
    '&._active': {
      borderColor: theme.palette.primary[300],
    },
  },
  aside: {
    position: 'relative',
    width: '80px',
    marginRight: '20px',
  },
  image: {
    width: 'calc(100% - 100px)',
    height: '100%',
    '& >div': {
      height: '100%',
      '& >img': {
        width: '100%',
        maxHeight: '500px',
        objectFit: 'contain',
      },
    },
  },
  prev: {
    top: '0',
    transform: 'rotate(90deg)',
  },
}));

const ProductPreview = ({ images, color }) => {
  const classes = useStyles();
  const [curImage, setCurImage] = useState(null);
  const [firstLoad, setFirstLoad] = useState(true);

  useEffect(() => {
    if (firstLoad) return;

    const img = images.find((i) => {
      if (i.color === color.color) {
        return i.image;
      }
    });

    setCurImage(img && img.image);
  }, [color]);

  useEffect(() => {
    setCurImage(images[0].image);
    setFirstLoad(false);
  }, [images]);

  const settings = {
    arrows: true,
    dots: false,
    infinite: false,
    slidesToShow: 5,
    slidesToScroll: 1,
    vertical: true,
    verticalSwiping: true,
    nextArrow: <ArrowNext />,
    prevArrow: <ArrowPrev />,
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.aside}>
        <Slider {...settings}>
          {images.map((item, i) => {
            return (
              <img
                key={i}
                className={clsx(
                  classes.thumb,
                  curImage === item.image && '_active',
                )}
                src={item.image}
                onMouseEnter={() => setCurImage(item.image)}
              />
            );
          })}
        </Slider>
      </Box>
      <Box className={classes.image}>
        {curImage && (
          <GlassMagnifier
            magnifierSize={'50%'}
            allowOverflow={true}
            magnifierBorderSize={1}
            magnifierBorderColor={'#e3f2fd'}
            square={true}
            imageSrc={curImage}
            imageAlt=""
          />
        )}
      </Box>
    </Box>
  );
};

export default ProductPreview;
