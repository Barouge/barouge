import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Tooltip, Button } from '@material-ui/core';
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import { updateCurrentProductData } from '../../../redux/actions/catalogActions';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  button: {
    transition: 'ease .3s all',
    display: 'block',
    minWidth: '0',
    marginRight: '10px',
    padding: '0',
    width: '50px',
    height: '50px',
    border: `solid 3px ${theme.palette.grey[400]}`,
    '&._disabled': {
      opacity: '0.2',
      cursor: 'default',
    },
    '&._active': {
      borderColor: theme.palette.primary[500],
      transform: 'scale(1.2)',
    },
  },
}));

const ProductColors = ({ colors, color, avaiableColors }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {
    curProduct: { data },
  } = useSelector((state) => state.catalog);

  const handleClick = (item) => {
    dispatch(
      updateCurrentProductData({
        ...data,
        color: item,
      }),
    );
  };

  return (
    <Box className={classes.root}>
      {colors.map((item, i) => {
        const isImage = item.hex.search('#') === -1;
        const disabled = avaiableColors.indexOf(item.color) === -1;
        const style = isImage
          ? { backgroundImage: `url(${item.hex})` }
          : { backgroundColor: item.hex };
        const active = color.color === item.color;

        return (
          <Box key={i} className={classes.color}>
            <Tooltip title={disabled ? 'Цвет недоступен' : item.name}>
              <Button
                variant="outlined"
                className={clsx(
                  classes.button,
                  'product-color',
                  disabled && '_disabled',
                  active && '_active',
                )}
                onClick={() => !disabled && handleClick(item)}
                style={style}
              />
            </Tooltip>
          </Box>
        );
      })}
    </Box>
  );
};

export default ProductColors;
