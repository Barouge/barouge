import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '30px',
    height: '30px',
    borderRadius: '4px',
    border: `solid 1px ${theme.color.border}`,
  },
}));

const ProductColor = ({ color }) => {
  const classes = useStyles();
  const isImage = color.search('#') === -1;

  return isImage ? (
    <Box
      className={classes.root}
      style={{ backgroundImage: `url(${color})` }}
    />
  ) : (
    <Box className={classes.root} style={{ backgroundColor: color }} />
  );
};

export default ProductColor;
