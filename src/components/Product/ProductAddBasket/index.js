import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Button,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import {
  addBasketProduct,
} from '../../../redux/actions/basketActions';
import { updateCurrentProductData } from '../../../redux/actions/catalogActions';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  wrapper: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    transition: 'ease .3s all',
    width: '38px',
    height: '38px',
    backgroundColor: theme.palette.primary[50],
  },
  input: {
    width: '38px',
    height: '38px',
    border: `solid 1px ${theme.palette.primary[500]}`,
    fontSize: '18px',
    textAlign: 'center',
    borderRadius: '5px',
    margin: '0 10px',
    '&::before': {
      display: 'none',
    },
    '& input': {
      textAlign: 'center',
      color: theme.palette.primary[500],
    },
  },
  button: {
    width: '250px',
    boxShadow: 'none',
    fontSize: '22px',
    whiteSpace: 'nowrap',
  },
}));

const ProductAddBasket = ({ product, data }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const handleClick = () => {
    if (data && data.inCart) {
      history.push('/basket');
    }

    dispatch(
      updateCurrentProductData({
        ...data,
        id: product.id,
        inCart: true,
      }),
    );
    dispatch(addBasketProduct(product));
  };

  return (
    <Box className={classes.root}>
      <Button
        variant="contained"
        color="primary"
        className={classes.button}
        onClick={handleClick}
      >
        {data && data.inCart ? 'Перейти в корзину' : 'Добавить в корзину'}
      </Button>
    </Box>
  );
};

export default ProductAddBasket;
