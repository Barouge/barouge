import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';

import ProductPrice from '../ProductPrice';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    marginTop: '20px',
  },
  control: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '40px',
    height: '40px',
    cursor: 'pointer',
    border: `solid 1px ${theme.palette.grey[100]}`,
    '&:nth-child(2)': {
      margin: '0 -1px',
    },
  },
  descr: {
    position: 'absolute',
    top: '100%',
    right: '0',
    marginTop: '5px',
    fontSize: '12px',
    fontStyle: 'italic',
  },
}));

const ProductCount = ({ count, totalCount, price, discount }) => {
  const classes = useStyles();
  const [curCount, setCurCount] = useState(count);

  const handleClick = (direction) => {
    switch (direction) {
      case 'plus':
        if (curCount === totalCount) {
          break;
        }

        setCurCount(curCount + 1);

        break;
      case 'minus':
        if (curCount === 1) {
          break;
        }

        setCurCount(curCount - 1);

        break;
      default:
        break;
    }
  };

  return (
    <Box className={classes.root}>
      <ProductPrice price={price * curCount} discount={discount} />
      <Box className={classes.wrapper}>
        <Box onClick={() => handleClick('minus')} className={classes.control}>
          <RemoveIcon />
        </Box>
        <Box className={classes.control}>{curCount}</Box>
        <Box onClick={() => handleClick('plus')} className={classes.control}>
          <AddIcon />
        </Box>
      </Box>
      {curCount > 1 && <Box className={classes.descr}>{price} ₽/шт.</Box>}
    </Box>
  );
};

export default ProductCount;
