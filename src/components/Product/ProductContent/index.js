import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import { Link } from 'react-router-dom';

import clsx from 'clsx';

import ProductAddBasket from '../ProductAddBasket';

import ProductPrice from '../ProductPrice';
import ProductColors from '../ProductColors';

import ProductVolumes from '../ProductVolumes';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: '-20px',
    display: 'flex',
    height: '100%',
    flexDirection: 'column',
  },
  header: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: '30px',
  },
  col: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    marginRight: '20px',
    '&:last-child': {
      marginRight: '0',
    },
  },
  key: {
    fontSize: '14px',
    fontWeight: '400',
    color: theme.palette.grey[600],
    marginRight: '5px',
  },
  val: {
    fontSize: '14px',
    fontWeight: '600',
    color: '#000',
  },
  connected: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    marginBottom: '50px',
  },
  conItem: {
    display: 'block',
    marginRight: '10px',
    '&:last-child': {
      marginRight: '0',
    },
  },
  thumb: {
    width: '80px',
    height: '80px',
    border: `solid 1px transparent`,
    objectFit: 'contain',
    '&._active': {
      borderColor: theme.palette.primary[300],
    },
  },
  title: {
    fontSize: '20px',
    fontWeight: '700',
    marginBottom: '15px',
  },
  row: {
    marginBottom: '30px',
  },
}));

const ProductContent = ({ product, handleProductUpdate, page, data }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      {data.price && (
        <Box className={classes.price}>
          <ProductPrice
            large={true}
            price={data.price}
            discount={data.discount}
          />
        </Box>
      )}
      <Box className={classes.header}>
        {data.vendor && (
          <Box className={classes.col}>
            <Box className={classes.key}>Артикул:</Box>
            <Box className={classes.val}>{data.vendor}</Box>
          </Box>
        )}
        {product.liked && (
          <Box className={classes.col}>
            <Box className={classes.key}>Понравилось:</Box>
            <Box className={classes.val}>{`${product.liked} людям`}</Box>
          </Box>
        )}
        {data.color && (
          <Box className={classes.col}>
            <Box className={classes.key}>Цвет:</Box>
            <Box className={classes.val}>{data.color.name}</Box>
          </Box>
        )}
        {product.material && (
          <Box className={classes.col}>
            <Box className={classes.key}>Материал:</Box>
            <Box className={classes.val}>{product.material}</Box>
          </Box>
        )}
      </Box>
      <Box className={classes.content}>
        {product.volumes && product.volumes.length > 0 && (
          <Box className={classes.row}>
            <Box className={classes.title}>Выберите объем:</Box>
            <ProductVolumes
              price={product.price}
              discount={product.discount}
              volume={data.volume}
              volumes={product.volumes}
            />
          </Box>
        )}
        {product.colors && product.colors.length > 0 && (
          <Box className={classes.row}>
            <Box className={classes.title}>Выберите цвет:</Box>
            <ProductColors
              color={data.color}
              colors={product.colors}
              avaiableColors={data.colors}
            />
          </Box>
        )}
        {product.connected && product.connected.length > 0 && (
          <Box className={classes.connected}>
            {product.connected.map((item) => {
              return (
                <Box
                  key={item.id}
                  component={Link}
                  to={item.href}
                  className={classes.conItem}
                  onClick={() => handleProductUpdate(item.name)}
                >
                  <img
                    className={clsx(
                      classes.thumb,
                      item.name === page && '_active',
                    )}
                    src={item.preview}
                    alt=""
                  />
                </Box>
              );
            })}
          </Box>
        )}
      </Box>
      <Box className={classes.footer}>
        <ProductAddBasket data={data} product={product} />
      </Box>
    </Box>
  );
};

export default ProductContent;
