import React from 'react';
import { Box, Grid, Container, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CarouselProducts from '../../Carousel/CarouselProducts';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: `${theme.spacing(2)}px 0`,
  },
}));

const ProductVisitedSlider = ({ products }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Container>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Typography variant="h2">Вы недавно просматривали</Typography>
          </Grid>
          {products && (
            <Grid xs={12} item>
              <CarouselProducts products={products} />
            </Grid>
          )}
        </Grid>
      </Container>
    </Box>
  );
};

export default ProductVisitedSlider;
