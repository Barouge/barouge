import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

import { useDispatch, useSelector } from 'react-redux';
import { fetchTopMenu } from '../../redux/actions/appActions';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: 0,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  element: {
    color: theme.color.white,
    textDecoration: 'none',
    textTransform: 'uppercase',
    lineHeight: '40px',
    height: '40px',
    padding: '0 40px',
    fontWeight: '700',
    whiteSpace: 'nowrap',
    '&:first-child': {
      paddingLeft: 0,
    },
    '&:last-child': {
      paddingRight: 0,
    },
  },
}));

const TopMenu = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const menu = useSelector((state) => state.content.menu);
  const loading = useSelector((state) => state.app.loading);

  useEffect(() => {
    dispatch(fetchTopMenu());
  }, []);

  return (
    <menu className={classes.root}>
      {menu.map((item, i) => {
        return (
          <Link key={i} to={item.href} className={classes.element}>
            {item.name}
          </Link>
        );
      })}
    </menu>
  );
};

export default TopMenu;
