import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, CircularProgress } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {
    minHeight: 'calc(100vh - 160px)',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
  },
}));

const PageLoader = () => {
  const classes = useStyles();

  return (
    <Box align="center" className={classes.root}>
      <CircularProgress />
    </Box>
  );
};

export default PageLoader;
