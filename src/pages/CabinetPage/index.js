import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Container } from '@material-ui/core';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { fetchUserInfo } from '../../redux/actions/userActions';

import Cabinet from '../../components/Cabinet';
import CabinetMenu from '../../components/Cabinet/CabinetMenu';
import PageHeader from '../../components/PageHeader';
import Loader from '../../components/Loader';

const useStyles = makeStyles((theme) => ({
  root: {},
  container: {
    display: 'flex',
    alignItems: 'flex-start',
    [theme.breakpoints.down('md')]: {
      flexWrap: 'wrap',
    },
  },
  menu: {
    width: '280px',
    boxShadow: `0 0 3px 1px ${theme.palette.grey[300]}`,
    padding: `10px 0`,
    marginRight: `60px`,
    [theme.breakpoints.down('md')]: {
      width: '100%',
      marginRight: '0',
      marginBottom: '20px',
    },
  },
  content: {
    width: `calc(100% - 340px)`,
    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
}));

const breadcrumbs = [
  {
    name: 'Личный кабинет',
    href: '',
  },
];

const seo = [
  {
    name: 'Личный кабинет',
    href: 'Barouge. Личный кабинет',
  },
];

const CabinetPage = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { isAuth, user } = useSelector((state) => state.user);
  const [type, setType] = useState('personal');

  useEffect(() => {
    if (history.location.pathname === '/cabinet') {
      history.push('/cabinet/personal');
    }

    switch (history.location.pathname) {
      case '/cabinet/personal':
        setType('personal');

        break;
      case '/cabinet/orders':
        setType('orders');

        break;
      case '/cabinet/favorites':
        setType('favorites');

        break;
      default:
        setType('personal');
    }
  }, []);

  useEffect(() => {
    if (isAuth === false) {
      history.push('/catalog');
    }

    if (isAuth) return;

    if (isAuth === null) {
      dispatch(fetchUserInfo());
    }
  }, [isAuth]);

  return (
    <Box className={classes.root}>
      <PageHeader
        breadcrumbs={breadcrumbs}
        seo={seo}
        title={'Личный кабинет'}
      />
      <Container className={classes.container}>
        {isAuth && (
          <Box component="nav" className={classes.menu}>
            <CabinetMenu type={type} setType={setType} />
          </Box>
        )}
        {isAuth ? (
          <Box className={classes.content}>
            <Cabinet user={user} type={type} />
          </Box>
        ) : (
          <Loader />
        )}
      </Container>
    </Box>
  );
};

export default CabinetPage;
