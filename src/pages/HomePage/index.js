import React from 'react';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Home from '../../components/Home';

const useStyles = makeStyles(() => ({
  root: {},
}));

const HomePage = () => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Home />
    </Box>
  );
};

export default HomePage;
