import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Box,
  Container,
  Grid,
  Button,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { Link } from 'react-router-dom';
import clsx from 'clsx';

import NewsCard from '../../components/News/NewsCard';
import NewsCardFull from '../../components/News/NewsCardFull';
import ScrollToTop from '../../components/ScrollToTop';
import Loader from '../../components/Loader';
import PageHeader from '../../components/PageHeader';

import { useDispatch, useSelector } from 'react-redux';
import {
  fetchNews,
  setCurrentNews,
  clearCurNews,
  showMoreNews,
} from '../../redux/actions/newsActions';
import { showLoader, hideLoader } from '../../redux/actions/appActions';

const useStyles = makeStyles({
  root: {
    paddingBottom: '40px',
  },
  title: {
    marginBottom: '10px',
  },
  nav: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  navBtn: {},
  next: {
    marginLeft: 'auto',
  },
  showMore: {
    width: '100%',
    height: '48px',
  },
});

const initialBreadcrumbs = [
  {
    name: 'Новости',
    href: '/news',
  },
];

const initialSeo = {
  title: 'Новости',
  description: 'Barouge. Новости',
};

const NewsPage = ({
  match: {
    params: { id },
  },
}) => {
  const classes = useStyles();
  const [seo, setSeo] = useState(initialSeo);
  const [prevNews, setPrevNews] = useState(null);
  const [nextNews, setNextNews] = useState(null);
  const [breadcrumbs, setBreadcrumbs] = useState(initialBreadcrumbs);
  const dispatch = useDispatch();
  const news = useSelector((state) => state.news);
  const loading = useSelector((state) => state.app.loading);
  const { t } = useTranslation();

  useEffect(() => {
    if (news.allNews) return;

    dispatch(showLoader());
    setTimeout(() => {
      dispatch(fetchNews());
      dispatch(hideLoader());
    }, 500);

    if (id === undefined) {
      setBreadcrumbs(initialBreadcrumbs);
      setSeo(initialSeo);
      dispatch(clearCurNews());

      return;
    }

    dispatch(setCurrentNews(id));
  }, []);

  useEffect(() => {
    if (id === undefined) {
      setBreadcrumbs(initialBreadcrumbs);
      setSeo(initialSeo);
      dispatch(clearCurNews());

      return;
    }
    dispatch(setCurrentNews(id));
  }, [news.news]);

  useEffect(() => {
    if (!news.curNews || id === undefined) return;

    news.allNews.map((item, i, arr) => {
      let cur = item.id === id;

      if (cur) {
        setNextNews(arr[i + 1]);
        setPrevNews(arr[i - 1]);
      }
    });

    setSeo({
      title: `Новости. ${news.curNews.title}`,
      description: `Barouge. Новости. ${news.curNews.title}`,
    });

    setBreadcrumbs([
      ...initialBreadcrumbs,
      {
        name: news.curNews.title,
        href: '',
      },
    ]);
  }, [news.curNews]);

  return (
    <Box className={classes.root}>
      <ScrollToTop />
      <PageHeader
        breadcrumbs={breadcrumbs}
        seo={seo}
        title={news.curNews ? news.curNews.title : t('News title')}
      />

      <Container>
        <Grid container spacing={4}>
          {news.curNews && (
            <Grid item xs={12} sm={3}>
              <Button
                component={Link}
                to={'/news'}
                variant="text"
                color="primary"
              >
                Назад к новостям
              </Button>
            </Grid>
          )}
          {!news.curNews ? (
            news.news &&
            news.news.map((item, i) => {
              return (
                <Grid item key={i} xs={12} md={4} lg={3}>
                  <NewsCard
                    key={i}
                    data={item}
                    setCurrentNews={setCurrentNews}
                  />
                </Grid>
              );
            })
          ) : (
            <Grid item xs={12}>
              <NewsCardFull loading={loading} news={news.curNews} />
            </Grid>
          )}
          {!news.curNews && news.news && news.news.length < news.totalNews && (
            <Grid item xs={12}>
              <Button
                className={classes.showMore}
                variant="contained"
                color="primary"
                size="large"
                onClick={() => {
                  dispatch(showLoader());

                  setTimeout(() => {
                    dispatch(showMoreNews());
                    dispatch(hideLoader());
                  }, 500);
                }}
              >
                {loading ? <Loader /> : 'Показать еще новости'}
              </Button>
            </Grid>
          )}
          {news.curNews && (
            <Box className={classes.nav}>
              {prevNews && (
                <Button
                  component={Link}
                  to={`/news/${prevNews.id}`}
                  variant="text"
                  color="primary"
                  className={clsx(classes.navBtn, classes.prev)}
                  onClick={() => {
                    dispatch(showLoader());
                    setTimeout(() => {
                      dispatch(setCurrentNews(prevNews.id));
                      dispatch(hideLoader());
                    }, 300);
                  }}
                >
                  Предыдущая новость
                </Button>
              )}
              {nextNews && (
                <Button
                  component={Link}
                  to={`/news/${nextNews.id}`}
                  variant="text"
                  color="primary"
                  className={clsx(classes.navBtn, classes.next)}
                  onClick={() => {
                    dispatch(showLoader());
                    setTimeout(() => {
                      dispatch(setCurrentNews(nextNews.id));
                      dispatch(hideLoader());
                    }, 300);
                  }}
                >
                  Следующая новость
                </Button>
              )}
            </Box>
          )}
        </Grid>
      </Container>
    </Box>
  );
};

export default NewsPage;
