import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Container } from '@material-ui/core';
import { fetchBasket } from '../../redux/actions/basketActions';
import { useDispatch, useSelector } from 'react-redux';

import Loader from '../../components/Loader';
import Basket from '../../components/Basket';
import PageHeader from '../../components/PageHeader';

const useStyles = makeStyles(() => ({
  root: {},
}));

const breadcrumbs = [
  {
    name: 'Корзина',
    href: '',
  },
];

const seo = [
  {
    title: 'Корзина',
    description: 'Barouge. Корзина',
  },
];

const BasketPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const basket = useSelector((state) => state.basket);

  useEffect(() => {
    if (basket) return;

    dispatch(fetchBasket());
  }, [basket]);

  return (
    <Box className={classes.root}>
      <PageHeader breadcrumbs={breadcrumbs} seo={seo} title={'Корзина'} />
      <Container>
        {basket.products ? <Basket basket={basket} /> : <Loader />}
      </Container>
    </Box>
  );
};

export default BasketPage;
