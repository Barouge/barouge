import React from 'react';
import { Box, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import Order from '../../components/Order';
import PageHeader from '../../components/PageHeader';

const useStyles = makeStyles((theme) => ({
  root: {},
  title: {
    marginBottom: '40px',
  },
}));

const OrderPage = () => {
  const classes = useStyles();
  const breadcrumbs = [
    {
      name: 'Оформление заказа',
      href: '',
    },
  ];
  const seo = [
    {
      title: 'Barouge. Оформление заказа',
      description: 'Barouge. Оформление заказа',
    },
  ];

  return (
    <Box className={classes.root}>
      <PageHeader
        breadcrumbs={breadcrumbs}
        seo={seo}
        title={'Оформление заказа'}
      />
      <Container>
        <Order />
      </Container>
    </Box>
  );
};

export default OrderPage;
