import React, { useEffect } from 'react';
import { Box, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { fetchFaq } from '../../redux/actions/appActions';
import { useDispatch, useSelector } from 'react-redux';

import PageHeader from '../../components/PageHeader';
import FaqList from '../../components/FaqList';
import ScrollToTop from '../../components/ScrollToTop';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingBottom: theme.spacing(10),
  },
}));

const breadcrumbs = [
  {
    name: 'Вопрос-Ответ',
    href: '/faq',
  },
];

const seo = {
  title: 'Новости',
  description: 'Barouge. Новости',
};

const FaqPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const faq = useSelector((state) => state.app.faq);

  useEffect(() => {
    if (faq) return;

    dispatch(fetchFaq());
  }, [faq]);

  return (
    <Box className={classes.root}>
      <ScrollToTop />
      <PageHeader breadcrumbs={breadcrumbs} seo={seo} title={'Вопрос-Ответ'} />
      <Container>{faq && <FaqList faq={faq} />}</Container>
    </Box>
  );
};

export default FaqPage;
