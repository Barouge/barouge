import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Container, Hidden } from '@material-ui/core';
import SideMenu from '../../components/SideMenu';
import Content from '../../components/Content';
import Loader from '../../components/Loader';
import ScrollToTop from '../../components/ScrollToTop';
import {
  fetchSections,
} from '../../redux/actions/appActions';
import { useDispatch, useSelector } from 'react-redux';
import PageHeader from '../../components/PageHeader';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingBottom: theme.spacing(10),
  },
  wrapper: {
    display: 'flex',
  },
  aside: {
    width: '350px',
    marginRight: '40px',
  },
  content: {
    width: 'calc(100% - 390px)',
    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
}));

const InnerPage = ({
  match: {
    params: { page },
  },
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [curPage, setCurPage] = useState(page);
  const [content, setContent] = useState(null);
  const [title, setTitle] = useState('');
  const [breadcrumbs, setBreadcrumbs] = useState(null);
  const [seo, setSeo] = useState(null);
  const sections = useSelector((state) => state.app.sections);
  const loading = useSelector((state) => state.app.loading);

  const loadContent = () => {
    sections.map((section) => {
      section.children.find((item) => {
        if (item.page === page) {
          if (item.seo) {
            setSeo(item.seo);
          }
          setContent(item.html);
          setTitle(item.name);
          setBreadcrumbs([
            {
              name: item.name,
              href: '',
            },
          ]);
        }
      });
    });
  };

  useEffect(() => {
    dispatch(fetchSections());
  }, []);

  useEffect(() => {
    if (!sections) return;

    loadContent();
  }, [sections]);

  useEffect(() => {
    if (!sections) return;

    loadContent();
  }, [curPage]);

  return (
    <Box className={classes.root}>
      <ScrollToTop />
      {curPage && <PageHeader title={title} />}
      <Container>
        {sections && (
          <Box className={classes.wrapper}>
            <Hidden mdDown>
              <Box component="aside" className={classes.aside}>
                <SideMenu
                  menu={sections}
                  setCurPage={setCurPage}
                  curPage={curPage}
                />
              </Box>
            </Hidden>
            <Box className={classes.content}>
              {loading ? <Loader /> : <Content content={content} />}
            </Box>
          </Box>
        )}
      </Container>
    </Box>
  );
};

export default InnerPage;
