import React, { useState, useEffect } from 'react';
import { getNewsItem } from '../../actions/news';
import {
  Box,
  Container,
  Typography,
  CircularProgress,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {},
  title: {
    marginBottom: '40px',
  },
  image: {
    display: 'block',
    maxWidth: '100%',
    marginBottom: '40px',
  },
});

const NewsItemPage = ({
  match: {
    params: { id },
  },
}) => {
  const classes = useStyles();
  const [news, setNews] = useState([]);

  useEffect(() => {
    getNewsItem().then((res) => {
      setNews(res.data);
    });
  }, []);

  return (
    <Box>
      {news.length !== undefined ? (
        <Box align="center" p={20}>
          <CircularProgress color="primary" />
        </Box>
      ) : (
        <Box className={classes.root}>
          <Container>
            <Typography variant="h1" className={classes.title}>
              {news.title}
            </Typography>
            <img src={news.image} alt="" className={classes.image} />
            <Typography>{news.text}</Typography>
          </Container>
        </Box>
      )}
    </Box>
  );
};

export default NewsItemPage;
