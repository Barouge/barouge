import React from 'react';
import { Box, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  root: {},
}));

const SearchPage = ({ history }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Container>Поиск</Container>
    </Box>
  );
};

export default SearchPage;
