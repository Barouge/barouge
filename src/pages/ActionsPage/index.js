import React, { useEffect } from 'react';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { fetchActions } from '../../redux/actions/newsActions';
import { useDispatch, useSelector } from 'react-redux';
import { showLoader, hideLoader } from '../../redux/actions/appActions';

import Actions from '../../components/Actions';

const useStyles = makeStyles(() => ({
  root: {},
}));

const ActionsPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const actions = useSelector((state) => state.news.actions);
  const loading = useSelector((state) => state.app.loading);

  useEffect(() => {
    if (actions) return;

    dispatch(showLoader());
    setTimeout(() => {
      dispatch(fetchActions());
      dispatch(hideLoader());
    }, 500);
  }, []);

  return (
    <Box className={classes.root}>
      {loading ? null : <Actions actions={actions} />}
    </Box>
  );
};

export default ActionsPage;
