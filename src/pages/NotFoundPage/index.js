import React from 'react';
import { Box, Container, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    width: '100%',
    minHeight: 'calc(100vh - 172px)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  title: {
    fontSize: '140px',
    marginBottom: '30px',
  },
});

const NotFoundPage = (props) => {
  const classes = useStyles();

  return (
    <Box>
      <Container className={classes.root}>
        <Typography color="primary" variant="h1" className={classes.title}>
          404
        </Typography>
        <Box>
          <Button component={Link} to="/" variant="outlined">
            Вернуться на главную
          </Button>
        </Box>
      </Container>
    </Box>
  );
};

export default NotFoundPage;
