import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Container } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import {
  fetchProduct,
  clearCurrentProduct,
} from '../../redux/actions/catalogActions';
import { showLoader, hideLoader } from '../../redux/actions/appActions';
import { useHistory } from 'react-router-dom';
import clsx from 'clsx';

import PageHeader from '../../components/PageHeader';

import ProductCardPage from '../../components/Product/ProductCard/ProductCardPage';
import HomeRecommendedSlider from '../../components/Home/HomeRecommendedSlider';
import ProductBuyWithSlider from '../../components/Product/ProductBuyWithSlider';
import ProductVisitedSlider from '../../components/Product/ProductVisitedSlider';

const useStyles = makeStyles((theme) => ({
  root: {
    transition: 'ease .3s all',
    padding: `0 0 ${theme.spacing(10)}px`,
    opacity: '1',
    '&._loading': {
      height: 'calc(100vh - 100px)',
      opacity: '0.5',
    },
  },
  loader: {
    minHeight: 'calc(100vh - 160px)',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
  },
}));

const ProductPage = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.app.loading);
  const { buyWith, curProduct, visited, recommended } = useSelector(
    (state) => state.catalog,
  );
  const [breadcrumbs, setBreadcrumbs] = useState(null);
  const page = history && history.location.pathname.split('/products/')[1];

  const handleProductUpdate = (page) => {
    dispatch(showLoader());
    setTimeout(() => {
      dispatch(fetchProduct(page));
      dispatch(hideLoader());
    }, 300);
  };

  useEffect(() => {
    if (page === undefined || page === '') {
      history.push('/catalog');
    }

    handleProductUpdate(page);

    return () => {
      dispatch(clearCurrentProduct());
    };
  }, []);

  useEffect(() => {
    if (!curProduct) return;

    setBreadcrumbs([
      {
        name: curProduct.product && curProduct.product.parent.name,
        href: curProduct.product && curProduct.product.parent.href,
      },
      {
        name: curProduct.product && curProduct.product.title,
        href: '',
      },
    ]);
  }, [curProduct]);

  return (
    <Box className={clsx(classes.root, loading && '_loading')}>
      {curProduct && (
        <PageHeader breadcrumbs={breadcrumbs} title={curProduct.data.title} />
      )}
      <Container>
        {curProduct && (
          <Box className={classes.wrapper}>
            <ProductCardPage
              handleProductUpdate={handleProductUpdate}
              product={curProduct.product}
              data={curProduct.data}
              page={page}
            />
            {buyWith && buyWith.length > 0 && (
              <ProductBuyWithSlider products={buyWith} />
            )}
            {recommended && recommended.length > 0 && (
              <HomeRecommendedSlider products={recommended} />
            )}
          </Box>
        )}
        {visited && visited.length > 1 && (
          <ProductVisitedSlider products={visited} />
        )}
      </Container>
    </Box>
  );
};

export default ProductPage;
