import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Box, Container } from '@material-ui/core';

import Catalog from '../../components/Catalog';
import PageHeader from '../../components/PageHeader';

const useStyles = makeStyles((theme) => ({
  root: {},
  aside: {
    position: 'sticky',
    top: `calc(140px + ${theme.spacing(2)}px)`,
    width: '100%',
  },
  catalog: {
    width: 'calc(100% - 370px)',
    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
  loader: {
    minHeight: '500px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

const breadcrumbs = [
  {
    name: 'Каталог',
    href: '',
  },
];

const seo = {
  title: 'Каталог',
  description: 'Barouge. Каталог',
};

const CatalogPage = () => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <PageHeader breadcrumbs={breadcrumbs} seo={seo} title={'Каталог'} />
      <Container>
        <Catalog />
      </Container>
    </Box>
  );
};

export default CatalogPage;
