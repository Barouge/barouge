import HomePage from './pages/HomePage';
import ContactsPage from './pages/ContactsPage';
import NewsPage from './pages/NewsPage';
import NotFoundPage from './pages/NotFoundPage';
import BasketPage from './pages/BasketPage';
import CatalogPage from './pages/CatalogPage';
import CabinetPage from './pages/CabinetPage';
import InnerPage from './pages/InnerPage';
import ProductPage from './pages/ProductPage';
import OrderPage from './pages/OrderPage';
import ActionsPage from './pages/ActionsPage';
import FaqPage from './pages/FaqPage';

const routes = [
  {
    path: '/',
    exact: true,
    component: HomePage,
  },
  {
    path: '/:language?/contacts',
    component: ContactsPage,
  },
  {
    path: '/:language?/news',
    component: NewsPage,
    exact: true,
  },
  {
    path: '/:language?/news/:id',
    component: NewsPage,
  },
  {
    path: '/:language?/basket',
    component: BasketPage,
  },
  {
    path: '/:language?/catalog',
    component: CatalogPage,
  },
  {
    path: '/:language?/catalog/:id',
    component: CatalogPage,
  },
  {
    path: '/:language?/cabinet',
    component: CabinetPage,
  },
  {
    path: '/:language?/cabinet/personal',
    component: CabinetPage,
  },
  {
    path: '/:language?/cabinet/orders',
    component: CabinetPage,
  },
  {
    path: '/:language?/cabinet/favorites',
    component: CabinetPage,
  },
  {
    path: '/:language?/sections/:section/:page',
    component: InnerPage,
  },
  {
    path: '/:language?/products',
    component: ProductPage,
  },
  {
    path: '/:language?/products/:id',
    component: ProductPage,
  },
  {
    path: '/:language?/order',
    component: OrderPage,
  },
  {
    path: '/:language?/actions',
    component: ActionsPage,
  },
  {
    path: '/:language?/faq',
    component: FaqPage,
  },
  {
    path: '**',
    component: NotFoundPage,
  },
];

export default routes;
