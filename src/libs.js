export const hasRole = (userRoles, roles) => {
  return roles.some((role) => {
    return userRoles.indexOf(role) > -1;
  });
};

export const cutText = (text, limit) => {
  return text.length >= limit ? `${text.slice(0, limit)}...` : text;
};

export const groupParamsByKey = (params) =>
  [...params.entries()].reduce((acc, tuple) => {
    const [key, val] = tuple;

    if (acc.hasOwnProperty(key)) {
      if (Array.isArray(acc[key])) {
        acc[key] = [...acc[key], val];
      } else {
        acc[key] = [acc[key], val];
      }
    } else {
      acc[key] = [val];
    }

    return acc;
  }, {});

export const addOrRemove = (array, value) => {
  const index = array.indexOf(value);

  if (index === -1) {
    return [...array, value];
  } else {
    array.splice(index, 1);

    return array;
  }
};

export const serialize = (obj, prefix) => {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + '[' + p + ']' : p,
        v = obj[p];
      str.push(
        v !== null && typeof v === 'object'
          ? serialize(v, k)
          : encodeURIComponent(k) + '=' + encodeURIComponent(v),
      );
    }
  }
  return str.join('&');
};

export const paramsToObject = (entries) => {
  const result = {};
  for (const [key, value] of entries) {
    result[key] =
      value === 'true' || value === 'false' ? Boolean(value) : value.split(',');
  }
  return result;
};

export const declension = (forms, val) => {
  const cases = [2, 0, 1, 1, 1, 2];

  return forms[
    val % 100 > 4 && val % 100 < 20 ? 2 : cases[val % 10 < 5 ? val % 10 : 5]
  ];
};
